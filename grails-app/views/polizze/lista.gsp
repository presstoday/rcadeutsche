<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="navigazione"/>
    <title>Elenco</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>

<body>
<div class="col-md-12 col-md-push-11"><asset:image src="/loghi/mansutti.png" height="30px" style="margin-bottom: 25px;" /></div>
<div class="row">
    <div class="col-md-12 col-xs-10">
        <g:if test="${flash.error}"><div class="labelErrore marginesotto">${flash.error.toString().replaceAll("\\<.*?>","")} </div></g:if>
        <g:if test="${flash.errorDealerPiva}">
            <div class="labelErrore marginesotto">
                <p>DEALER NON CARICATI PERCHE' GIA' PRESENTI:</p>
                <g:each var="errore" in="${flash.errorDealerPiva}">
                        <p>${errore}</p>
                </g:each>
            </div>
        </g:if>
        <g:if test="${flash.errorDealer}">
            <div class="labelErrore marginesotto">
                <p>DEALER GIA' PRESENTI, MA CON P.IVA DIVERSA:</p>
                <g:each var="errore" in="${flash.errorDealer}">
                    <p>${errore}</p>
                </g:each>
            </div>
        </g:if>
        <g:if test="${flash.errorePratica}">
            <div class="labelErrore marginesotto">
                <p>Il caricamento delle pratiche ha riportato i seguenti errori:</p>
                <g:each var="errore" in="${flash.errorePratica}">
                    <p>${errore.toString().replaceAll("\\<.*?>","")}</p>
                </g:each>
            </div>
        </g:if>
        <g:if test="${flash.message=="POLIZZA"}"><script type="text/javascript">swal({
            title: "Richiesta inviata!",
            type: "success",
            timer: "1800",
            showConfirmButton: false
        });</script></g:if>
        <g:elseif test="${flash.message=="Dealer aggiornato"}" ><script type="text/javascript">swal({
            title: "${flash.message}",
            type: "success",
            timer: "1800",
            showConfirmButton: false
        });</script></g:elseif>
        <g:elseif test="${flash.message=="DEALER AGGIORNATO"}">
            <script type="text/javascript">swal({
                title: "Il dealer è stato aggiornato!",
                type: "success",
                timer: "1800",
                showConfirmButton: false
            });</script>
        </g:elseif>
        <g:elseif test="${flash.messageDealer}">
            <div class="labelErrore marginesotto">
                <p>NUOVI DEALER CARICATI CORRETTAMENTE:</p>
                <g:each var="messageD" in="${flash.messageDealer}">
                    <p>${messageD}</p>
                </g:each>
            </div>
        </g:elseif>
        <g:elseif test="${flash.praticheNuove}">
            <div class="labelErrore marginesotto">
                <p>PRATICHE CORRETTAMENTE CARICATE:</p>
                <g:each var="messageP" in="${flash.praticheNuove}">
                    <p>${messageP}</p>
                </g:each>
            </div>
        </g:elseif>
        <div class="col-md-12 col-xs-12 col-xs-offset-1">
            <f:form  id="listform" method="get">
                <div class="col-md-3 col-xs-6 col-xs-pull-1  area-ricerca">
                    <div class="input-group input-ricerca">
                        <input type="text" class="form-control" placeholder="ricerca pratiche" name="search" value="${params.search}">
                        <span class="input-group-btn">
                            <button type="submit" id="button_search" class="btn btn-secondary btn-ricerca" onchange="window.location.href = window.location.href.substr(0,window.location.href.lastIndexOf('?'));"><i class="glyphicon glyphicon-search"></i></button>
                        </span>
                    </div>
                </div>
                <sec:ifHasRole role="ADMIN">
                <div class="col-md-6 col-xs-12" style="margin-bottom: 4px;">
                    <div class="col-md-3 col-xs-3 col-md-pull-2">
                        <a href="#" id="new-pratica" class="btn  btn-small" title="Nuova pratica"><b>Caricare pratica</b> <i class="fa fa-upload"></i></a>
                    </div>
                    <div class="col-md-3 col-xs-3 col-md-pull-1" >
                        <a href="#" id="new-dealer" class="btn  btn-small" title="Nuovo dealer"><b>Caricare dealer</b> <i class="fa fa-upload"></i></a>
                    </div>
                    <div class="col-md-3 col-xs-3 col-md-push-1">
                        <a href="${createLink(controller: "polizze", action: "generaFilePolizzeattivate")}" target="_blank" class="btn  btn-small" title="Estrazione polizze"><b>Estrarre polizze </b> <i class="fa fa-download"></i></a>
                    </div>
                    <div class="col-md-3 col-xs-3 col-md-push-2" >
                        <a href="${createLink(controller: "polizze", action: "scaricaFascicoloInformativo")}" target="_blank" class="btn  btn-small" title="Scaricare fascicolo informativo"><b>Fascicolo informativo</b> <i class="fa fa-download"></i></a>
                    </div>
                </div>
                </sec:ifHasRole>
                <sec:ifHasNotRole role="ADMIN">
                <div class="col-md-3 col-xs-3 col-md-push-6 col-xs-pull-1" style="margin-bottom: 4px;">
                    <a href="${createLink(controller: "polizze", action: "scaricaFascicoloInformativo")}" target="_blank" class="btn  btn-small" title="Scaricare fascicolo informativo"><b>Fascicolo informativo</b> <i class="fa fa-download"></i></a>
                </div>
                </sec:ifHasNotRole>
            </f:form>
        </div>
        <div class="table-responsive col-md-12 col-xs-12">
            <table class="table table-condensed" id="tabellaPolizza">
                <thead>
                <sec:ifHasNotRole role="DEALER">
                    <th class="text-left vcenter-column">DEALER</th>
                </sec:ifHasNotRole>
                <th class="text-justify"></th>
                <th class="text-left">STATO</th>
                <th class="text-left">NO. PRATICA</th>
                <th class="text-left">INTESTATARIO</th>
                <th class="text-left">P.IVA.<br>INTESTARIO</th>
                <th class="text-left">GARANTE</th>
                <th class="text-left">P.IVA.<br>GARANTE</th>
                <th class="text-center">MODULO<br>ADESIONE</th>
                <th class="text-left">CERTIFICATO<br>DI POLIZZA</th>
                <th class="text-center">DOCUMENTO<br>ACQUISIZIONE</th>
                </tr>
                </thead>
                <tbody>
                <g:each var="polizza" in="${polizze}">
                    <input type="hidden" name="idPolizzasel" id="idPolizzasel" value="${polizza.id}"/>
                    <tr class="polizza">
                        <sec:ifHasNotRole role="DEALER">
                            <td class="text-left vcenter-column dealerNome" data-id="${polizza.dealer.id}"  id="dealerNome" name="dealerNome" value="${polizza.dealer.id}"><b>${polizza.dealer.ragioneSociale}</b><br>${polizza.dealer.indirizzo}, ${polizza.dealer.provincia}</td>
                            <td class="text-left vcenter-column"><i class="fa fa-pencil" style="cursor: pointer; font-size: 1.25em;" onclick="return modalPolizzaAdmin(${polizza.id});"></i></td>
                        </sec:ifHasNotRole>
                        <sec:ifHasRole role="DEALER">
                            <td class="text-left vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()=="PREVENTIVO"}"><i class="fa fa-pencil" style="cursor: pointer; font-size: 1.25em;" onclick="return modalPolizza(${polizza.id});"></i></g:if></td>
                        </sec:ifHasRole>
                        <td class="text-left vcenter-column" id="statoPolizza"><g:if test="${polizza.stato.toString().toUpperCase()=="POLIZZA POSTICIPATA"}">POSTICIPATA</g:if><g:elseif test="${polizza.stato.toString().toUpperCase()=="POLIZZA_IN_ATTESA_ATTIVAZIONE"}">IN ATTESA DI ATTIVAZIONE</g:elseif><g:else>${polizza.stato.toString().toUpperCase()}</g:else> </td>
                        <td class="text-left vcenter-column">${polizza.noPratica}</td>
                        <td class="text-left vcenter-column">${polizza.nomeInt.toString().toUpperCase()} ${polizza.cognomeInt.toString().toUpperCase()}</td>
                        <td class="text-left vcenter-column">${polizza.partitaIvaInt.toString().toUpperCase()}</td>
                        <td class="text-left vcenter-column">${polizza.nomeGarante.toString().toUpperCase()} ${polizza.cognomeGarante.toString().toUpperCase()}</td>
                        <td class="text-left vcenter-column">${polizza.partitaIvaGarante.toString().toUpperCase()}</td>
                        <td class="text-center vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()!="PREVENTIVO" && polizza.adesione ==true }"><g:link action="scaricaModulo" id="${polizza.id}" class="bottoneDoc" data-toggle="tooltip" data-container="body" data-placement="bottom" title="Modulo adesione ${polizza.noPolizza}"><i class="fa fa-file-text" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></g:link></g:if></td>
                        <td class="text-center vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()=="POLIZZA"}"><a class="bottoneDoc" href="${createLink(controller: "polizze", action: "scaricaCertificato", id: polizza.id)}" target="_blank"  title="Certificato polizza ${polizza.noPolizza}"><i class="fa fa-file-pdf-o" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></a></g:if></td>
                        <td class="text-center vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()!="PREVENTIVO" && polizza.tipoAcquisizione.toString().toUpperCase()!= "NUOVA POLIZZA" }"><g:link action="scaricaDocumento" id="${polizza.id}" class="bottoneDoc" data-toggle="tooltip" data-container="body" data-placement="bottom" title="documento acquisizione"><i class="fa fa-file-text" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></g:link></g:if><g:elseif test="${polizza.stato.toString().toUpperCase()!="PREVENTIVO" && polizza.tipoAcquisizione.toString().toUpperCase()== "NUOVA POLIZZA" }">NUOVA POLIZZA</g:elseif></td>
                    </tr>
                </g:each>
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="18" class="text-center"><b>Sono state trovate ${polizze?.totalCount ?: 0} polizze</b></th>
                </tr>
                </tfoot>
            </table>
        </div>
        <nav aria-label="Page navigation">
            <ul class="pagination pull-right">
                <g:if test="${totalPages>1}">
                    <li>
                        <link:list page="1" controller="${controllerName}">
                            <span aria-hidden="true">&laquo;</span>
                        </link:list>
                    </li>
                </g:if>
                <g:each var="p" in="${pages}">
                    <g:if test="${p == page}"><li class="active"></g:if><g:else><li></g:else>
                    <g:if test="${params.search}"><link:list page="${p}" controller="${controllerName}" search="${params.search}">${p}</link:list></g:if>
                    <g:else><link:list page="${p}" controller="${controllerName}">${p}</link:list></g:else>
                    </li>
                </g:each>
                <g:if test="${totalPages>1}">
                    <li>
                        <link:list page="${totalPages}" controller="${controllerName}">
                            <span aria-hidden="true">&raquo;</span>
                        </link:list>
                    </li>
                </g:if>
            </ul>
        </nav>

    </div>
    <div class="modal modal-wide fade windowPolizza" tabindex="-1" id="modalPolizza" role="dialog" aria-labelledby="myLargeModalLabel" style="overflow-y: scroll;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header insPolizzaHeader">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h3 class="text-center testo-navBarTitolo"><fa:icon name="newspaper-o"/> Inserimento Polizza</h3>
                </div>
                <f:form action="${createLink(action: "aggiornaPolizza")}" id="polizzaForm"  method="post" class="form-horizontal" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="datiPoli"></div>
                        <div class="bs-example">
                            <ul class="nav nav-tabs">
                                <sec:ifHasRole role="ADMIN">
                                    <li class="active"><a  href="#sectionA" class="legendaTabs" data-toggle="tab"><span class="fa fa-user"></span> DATI DEL CLIENTE</a></li>
                                    <li><a  href="#sectionB" class="legendaTabs" data-toggle="tab"><span class="fa fa-files-o"></span> ACQ. POSIZIONE ASSICURATIVA</a></li>
                                    <li><a  href="#sectionC" class="legendaTabs" data-toggle="tab"><span class="fa fa-users"></span> DATI DEL CONCESSIONARIO</a></li>
                                </sec:ifHasRole>
                                <sec:ifHasNotRole role="ADMIN">
                                    <li class="active"><a  href="#sectionA" class="legendaTabs"><span class="fa fa-user"></span> DATI DEL CLIENTE</a></li>
                                    <li><a  href="#sectionB" class="legendaTabs"><span class="fa fa-files-o"></span> ACQ. POSIZIONE ASSICURATIVA</a></li>
                                    <li><a  href="#sectionC" class="legendaTabs"><span class="fa fa-users"></span> DATI DEL CONCESSIONARIO</a></li>
                                </sec:ifHasNotRole>

                            </ul>
                            <div class="tab-content">
                                <div id="sectionA" class="tab-pane fade in active">
                                    <input type="hidden" name="idPolizza" id="idPolizza" value=""/>
                                    <div class="col-md-12" style="margin-top: 10px;">
                                        <label class="testoParagraph control-label ">SELEZIONARE IL CONTRAENTE EFFETTIVO:</label>
                                    </div>
                                    <div class="col-md-12 col-xs-10">
                                        <div class="col-md-3 col-xs-3"><f:radiobutton  class="centered" name="tipoContraente"  value="INTESTATARIO" id="cliente" checked="true" text="INTESTATARIO"/></div>
                                        <div class="col-md-3 col-xs-3"><f:radiobutton  class="centered" name="tipoContraente"  value="GARANTE" id="garante"  text="GARANTE"/></div>
                                    </div>
                                    <div class="col-xs-6 col-md-4 marginesopra">
                                        <label class="testoParagraph control-label ">COGNOME</label>
                                        <input class="form-control testoParagraph" type="text" name="cognomeInt" id="cognomeInt" value="" readonly="true"/>
                                    </div>
                                    <div class="col-xs-6 col-md-4 marginesopra">
                                        <label class="testoParagraph control-label ">NOME</label>
                                        <input class="form-control testoParagraph" type="text" name="nomeInt" id="nomeInt" value="" readonly="true"/>
                                    </div>
                                    <div class="col-xs-6 col-md-4 marginesopra">
                                        <label class="testoParagraph control-label">C.FISCALE / P.IVA</label>
                                        <input class="form-control testoParagraph" type="text" name="partitaIvaInt"  id="partitaIvaInt" value="" readonly="true"/>
                                    </div>
                                    <div class="col-xs-6 col-md-4">
                                        <label class="testoParagraph control-label">TELEFONO</label>
                                        <input class="form-control testoParagraph" type="text" name="telefonoDef"  id="telefonoDef" value=""/>
                                    </div>
                                    <div class="col-xs-6 col-md-4">
                                        <label class="testoParagraph control-label">EMAIL</label>
                                        <input class="form-control testoParagraph" type="text" name="emailDef"  id="emailDef" value="" />
                                    </div>
                                    <div class="col-xs-6 col-md-2">
                                        <label class="testoParagraph control-label">SESSO</label>
                                        <input class="form-control testoParagraph" type="text" name="tipoClienteInt" id="tipoClienteInt" value="" readonly="true"/>
                                    </div>
                                    <div class="col-xs-6 col-md-2">
                                        <label class="testoParagraph control-label">PROVINCIA</label>
                                        <f:input control-class="form" type="text" name="provinciaInt" class="maiuscola testoParagraph" id="provinciaInt" readonly="true" value="" />
                                    </div>
                                    <div class="col-xs-6 col-md-5 marginesotto">
                                        <label class="testoParagraph control-label">INDIRIZZO</label>
                                        <f:input control-class="form" type="text" name="indirizzoInt" class="maiuscola testoParagraph"  id="indirizzoInt" readonly="true" value="" />
                                    </div>
                                    <div class="col-xs-6 col-md-5 marginesotto">
                                        <label class="testoParagraph control-label">LOCALITA'</label>
                                        <f:input control-class="form" type="text" name="localitaInt" class="maiuscola testoParagraph" id="localitaInt" readonly="true" value="" />
                                    </div>
                                    <div class="col-xs-6 col-md-2 marginesotto">
                                        <label class="testoParagraph control-label">CAP</label>
                                        <f:input control-class="form" type="text" name="capInt" class="maiuscola testoParagraph" id="capInt" readonly="true" value="" />
                                    </div>
                                    <legend class="legenda"><span class="fa fa-car"></span> DATI DEL AUTOVEICOLO</legend>
                                    <div class="col-xs-6 col-md-3" >
                                        <label class="testoParagraph control-label">NO. PRATICA</label>
                                        <input class="form-control testoParagraph" type="text" name="noPratica"  id="noPratica" value="" readonly="true"/>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <label class="testoParagraph control-label">MARCA</label>
                                        <f:input control-class="form" type="text" name="marca"  class="maiuscola testoParagraph" id="marca" value="" />
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <label class="testoParagraph control-label">MODELLO</label>
                                        <f:input control-class="form" type="text" name="modello" id="modello" class="maiuscola testoParagraph" value="" />
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <label class="testoParagraph control-label">TARGA</label>
                                        <f:input control-class="form"  type="text" name="targa" class="maiuscola testoParagraph"  id="targa" value="" />
                                    </div>
                                    <div class="col-xs-10 col-md-3" style="margin-top: 25px;">
                                        <select class="form-control testoParagraph" name="autoveicolo" id="autoveicolo">
                                            <option id="autoV" value="true" selected>AUTOVETTURA</option>
                                            <option id="autoC" value="false">AUTOCARRO</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <label class="testoParagraph control-label">VALORE ASSIC. (&euro;)</label>
                                        <f:input control-class="form" type="number" name="valoreAssicurato"  required="true" id="valoreAssicurato" value="" />
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <label class="testoParagraph control-label">DATA IMMATR.</label>
                                        <f:input control-class="form" type="text" name="dataImmatricolazione"  id="dataImmatricolazione" value="" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true" data-date-end-date="0d"
                                                 data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph"
                                                 data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false" required="true"/>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <label class="testoParagraph control-label">DATA DECORRENZA</label>
                                        <f:input control-class="form" type="text" name="dataDecorrenza"  id="dataDecorrenza" value="" readonly="true" required="false"/>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <label class="testoParagraph control-label">TELAIO</label>
                                        <f:input control-class="form" type="text" name="telaio" class="maiuscola testoParagraph"  id="telaio" value="" />
                                    </div>
                                    <div class="col-xs-3 col-md-6">
                                        <label class="testoParagraph control-label">MODULO DI ADESIONE FIRMATO*</label>
                                        <input id="input-idmodulo" type="file"  class="file moduloAdesione" multiple data-preview-file-type="text" name="fileContentModAdesione" data-upload-async="false" data-language="it" data-max-file-count="1" data-show-preview="false" data-show-upload="false">
                                    </div>
                                    <div class="col-xs-6 col-md-12" style="margin-top:20px;">
                                        <div class="col-xs-6 col-md-6">
                                        </div>
                                        <div class="col-xs-6 col-md-6">
                                            <button href="#sectionB" data-toggle="tab" id="avanti1" class="btn  pull-right" onclick="attivaTab('sectionB')">Avanti&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div id="sectionB" class="tab-pane fade">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="col-xs-6 col-md-12">
                                            <div class="col-xs-3 col-md-12" style="margin-top: 25px;">
                                                <f:radiobutton  class="centered " name="tipoAcquisizione"  value="NUOVA_POLIZZA" id="acquisizioneNuovaPolizza" checked="true" text="Nuova Polizza" />
                                            </div>
                                            <div class="col-xs-3 col-md-12" style="margin-top: 25px;">
                                                <f:radiobutton  class="centered " name="tipoAcquisizione"  value="PASSAGGIO" id="acquisizionePassaggio" text="Passaggio da altro veicolo: targa del vecchio veicolo e documento che attesti la perdita di possesso" />
                                            </div>
                                            <div class="col-xs-3 col-md-12" style="margin-top: 25px;">
                                                <f:radiobutton  class="centered " name="tipoAcquisizione"  value="BERSANI" id="acquisizioneBersani"  text="Bersani: targa del veicolo da cui ereditare la classe e autocertificazione stato di famiglia che attesta la convivenza con il contraente" />
                                            </div>
                                            <div class="col-xs-3 col-md-12" style="margin-top: 25px;">
                                                <div class="col-xs-3 col-md-3"><label class="testoParagraph control-label">TARGA </label><f:input control-class="form"  type="text"  name="targaAcquisizione" class="maiuscola testoParagraph"  id="targaAcquisizione" value="" /></div>
                                                <div class="col-xs-3 col-md-9">
                                                    <label class="testoParagraph control-label">DOCUMENTO</label>
                                                    <input id="input-idAcquisizione" type="file"  class="file allegatoPassaggio" multiple data-preview-file-type="text" name="fileContentAcquisizione" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-12" style="margin-top:20px;">
                                        <div class="col-xs-6 col-md-6">
                                            <a href="#sectionA" data-toggle="tab" id="indietro" class="btn pull-left button-ladda" onclick="attivaTab('sectionA')"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Indietro</a>
                                        </div>
                                        <div class="col-xs-6 col-md-6">
                                            <button href="#sectionC" data-toggle="tab" id="avanti2" class="btn  pull-right" onclick="attivaTab('sectionC')">Avanti&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div id="sectionC" class="tab-pane fade">
                                    <div class="col-xs-6 col-md-5 marginesopra">
                                        <label class="testoParagraph control-label">RAGIONE SOCIALE</label>
                                        <f:input control-class="form" type="text" name="ragionesociale" class="maiuscol, testoParagraph" id="ragionesociale" readonly="true" value="" />
                                    </div>
                                    <div class="col-xs-6 col-md-4 marginesopra">
                                        <label class="testoParagraph control-label">INDIRIZZO</label>
                                        <f:input control-class="form" type="text" name="indirizzo" class="maiuscola testoParagraph"  id="indirizzo" readonly="true" value="" />
                                    </div>
                                    <div class="col-xs-6 col-md-3 marginesopra">
                                        <label class="testoParagraph control-label">PARTITA IVA</label>
                                        <f:input control-class="form" type="text" name="piva" class="maiuscola testoParagraph"  id="piva" readonly="true" value="" />
                                    </div>
                                    <div class="col-xs-6 col-md-5">
                                        <label class="testoParagraph control-label">COMUNE</label>
                                        <f:input control-class="form" type="text" name="comune" class="maiuscola testoParagraph" id="comune" readonly="true" value="" />
                                    </div>
                                    <div class="col-xs-6 col-md-2">
                                        <label class="testoParagraph control-label">CAP</label>
                                        <f:input control-class="form" type="text" name="cap" class="maiuscola testoParagraph" id="cap" readonly="true" value="" />
                                    </div>
                                    <div class="col-xs-6 col-md-2">
                                        <label class="testoParagraph control-label">PROV.</label>
                                        <f:input control-class="form" type="text" name="provincia" class="maiuscola testoParagraph" id="provincia" readonly="true" value="" />
                                    </div>
                                    <div class="col-xs-6 col-md-5">
                                        <label class="testoParagraph control-label">MAIL</label>
                                        <f:input control-class="form" type="text" name="email"  id="email" class="testoParagraph" value="" />
                                    </div>
                                    <div class="col-xs-6 col-md-4">
                                        <label class="testoParagraph control-label">TELEFONO</label>
                                        <f:input control-class="form" type="text" name="telefono" class="maiuscola testoParagraph" id="telefono"  value="" />
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <label class="testoParagraph control-label">FAX</label>
                                        <f:input control-class="form" type="text" name="fax"  id="fax" class="testoParagraph"  value="" />
                                    </div>
                                    <div class="col-xs-6 col-md-12" style="margin-top:20px;">
                                        <div class="col-xs-6 col-md-6">
                                            <a href="#sectionB" data-toggle="tab" id="indietro1" class="btn pull-left button-ladda" onclick="attivaTab('sectionB')"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Indietro</a>
                                        </div>
                                        <div class="col-xs-6 col-md-6">
                                            <button type="submit" id="bottonSalva" class="btn pull-right">Salva</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </f:form>
            </div>
        </div>
    </div>
    <div class="modal" id="form-dealer">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center testo-navBarTitolo"> Caricamento Dealer</h3>
                    </div>
                    <div class="modal-body" style="background: lightgrey">
                        <form action="${createLink(action: "caricaDealer", params:[_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input id="input-idExcelDealer" type="file" name="excelDealer" class="file" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                            %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" style="margin-top: 10px;"  class="btn pull-right" data-uploading-text="${'<i class="fa fa-spinner fa-spin fa-lg"></i>'}">Carica</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>
    <div class="modal" id="form-pratica">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center testo-navBarTitolo"> Caricamento Pratica</h3>
                    </div>
                    <div class="modal-body" style="background: lightgrey">
                        <form action="${createLink(action: "caricaPratica", params:[_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input id="input-idExcelPratica" type="file" name="excelPratica" class="file" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                            %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" style="margin-top: 10px;"  class="btn pull-right" data-uploading-text="${'<i class="fa fa-spinner fa-spin fa-lg"></i>'}">Carica</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#new-dealer").click(function() { $("#form-dealer").modal("show"); });
    $("#new-pratica").click(function() { $("#form-pratica").modal("show"); });
    $(document).ready(function() {
        window.setTimeout(function() {
            $(".alert").fadeTo(1500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 5000);
    });
    var txtemailCliente="";
    var txttargaCliente="";
    var txttargaAcquisizione="";
    var txtdataImmatricolazione="";

    function attivaTab(tab){
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    };
    function modalPolizzaAdmin(x){
        $.post("${createLink(controller: "polizze", action: 'getPolizzaAdmin')}",{ idPolizza: x,  _csrf: "${request._csrf.token}"}, function(response) {

            if(response.risposta==true){
                $( ".alert" ).remove();
                $("#cognomeInt").val(response.cognomeInt);
                $("#nomeInt").val(response.nomeInt);
                $("#tipoClienteInt").val(response.tipoClienteInt);
                $("#noPratica").val(response.noPratica);
                $("#partitaIvaInt").val(response.partitaIvaInt);
                $("#telefonoDef").val(response.telefonoInt);
                $("#indirizzoInt").val(response.indirizzoInt);
                $("#capInt").val(response.capInt);
                $("#localitaInt").val(response.localitaInt);
                $("#provinciaInt").val(response.provinciaInt);
                $("#emailDef").val(response.emailInt);
                $("#targa").val(response.targa).prop("disabled",false);
                $("#telaio").val(response.telaio).prop("disabled",false);
                $("#valoreAssicurato").val(response.valoreAssicurato).prop("disabled",false);
                $("#marca").val(response.marca);
                $("#dataImmatricolazione").datepicker('setDate',response.dataImmatricolazione).prop("disabled",false);
                $("#dataDecorrenza").val(response.dataDecorrenza);
                $("#modello").val(response.modello);
                var autoveicolo=response.autoveicolo;
                if(autoveicolo=="true"){
                    document.getElementById("autoveicolo").selectedIndex = 0;
                    $("#autoveicolo").value = "true";
                }else{
                    document.getElementById("autoveicolo").selectedIndex = 1;
                    $("#autoveicolo").value = "false";
                }
                var tipoContraente=response.tipoContraente;
                if(tipoContraente=='garante'){
                    $("#garante").prop("checked",true);
                }else{
                    $("#cliente").prop("checked",true);
                }
                var tipoAcquisizione=response.tipoAcquisizione;
                if(tipoAcquisizione=='nuova polizza'){
                    $("#acquisizioneNuovaPolizza").prop("checked",true);
                    $('#input-idAcquisizione').fileinput('disable');
                    $("#targaAcquisizione").prop('disabled', true);
                }else if(tipoAcquisizione=='Bersani'){
                    $("#acquisizioneBersani").prop("checked",true);
                    $('#input-idAcquisizione').fileinput('enable');
                    $("#targaAcquisizione").val(response.targaAcquisizione).prop('disabled', false);
                }else if(tipoAcquisizione=='passaggio'){
                    $("#acquisizionePassaggio").prop("checked",true);
                    $('#input-idAcquisizione').fileinput('enable');
                    $("#targaAcquisizione").val(response.targaAcquisizione).prop('disabled', false);
                }
                $("#ragionesociale").val(response.dealer);
                $("#piva").val(response.piva);
                $("#indirizzo").val(response.indirizzo);
                $("#cap").val(response.cap);
                $("#provincia").val(response.provincia);
                $("#comune").val(response.comune);
                $("#telefono").val(response.telefono);
                var fax;
                if(response.fax=="null"){
                    fax='';
                }else{
                    fax=response.fax;
                }
                $("#fax").val(fax);
                $("#email").val(response.email);
                $("#idPolizza").val(x);

                $("#bottonSalva").prop('disabled', false);
                $("#avanti1").prop('disabled', true);
                $("#modalPolizza").modal({
                    keyboard: true
                });
            }
        }, "json");
    }
    function modalPolizza(x){
        $.post("${createLink(controller: "polizze", action: 'getPolizza')}",{ idPolizza: x,  _csrf: "${request._csrf.token}"}, function(response) {

            if(response.risposta==true && response.stato=="PREVENTIVO"){
                $( ".alert" ).remove();
                $("#cognomeInt").val(response.cognomeInt);
                $("#nomeInt").val(response.nomeInt);
                $("#tipoClienteInt").val(response.tipoClienteInt);
                $("#noPratica").val(response.noPratica);
                $("#partitaIvaInt").val(response.partitaIvaInt);
                $("#telefonoDef").val(response.telefonoInt);
                $("#indirizzoInt").val(response.indirizzoInt);
                $("#capInt").val(response.capInt);
                $("#localitaInt").val(response.localitaInt);
                $("#provinciaInt").val(response.provinciaInt);
                $("#emailDef").val(response.emailInt);
                $("#targa").prop("disabled",false);
                $("#telaio").prop("disabled",false);
                $("#marca").val(response.marca);
                $("#dataImmatricolazione").prop("disabled",false);
                $("#dataDecorrenza").val(response.dataDecorrenza);
                $("#modello").val(response.modello);
                var autoveicolo=response.autoveicolo;
                if(autoveicolo=="true"){
                    document.getElementById("autoveicolo").selectedIndex = 0;
                    $("#autoveicolo").value = "true";
                }else{
                    document.getElementById("autoveicolo").selectedIndex = 1;
                    $("#autoveicolo").value = "false";
                }
                $("#ragionesociale").val(response.dealer);
                $("#piva").val(response.piva);
                $("#indirizzo").val(response.indirizzo);
                $("#cap").val(response.cap);
                $("#provincia").val(response.provincia);
                $("#comune").val(response.comune);
                $("#telefono").val(response.telefono);
                var fax;
                if(response.fax=="null"){
                    fax='';
                }else{
                    fax=response.fax;
                }
                $("#fax").val(fax);
                $("#email").val(response.email);
                $("#idPolizza").val(x);
                $('#input-idAcquisizione').fileinput('disable');
                $("#targaAcquisizione").prop('disabled', true);
                $("#bottonSalva").prop('disabled', false);
                $("#avanti1").prop('disabled', true);
                $("#modalPolizza").modal({
                    keyboard: true
                });
            }
        }, "json");
    }
    function createCell(cell, text, style) {
        var txt = document.createTextNode(text); // create text node
        cell.appendChild(txt);                   // append DIV to the table cell
    }
    function aggiornaPolizza(){
        var idPolizza=$("#idPolizza").val();
        var targa=$("#targa").val();
        var telaio=$("#telaio").val();
        var dataImmatricolazione=$("#dataImmatricolazione").val();
        var marchio=$("#marca").val();
        var versione=$("#modello").val();
        var dataDecorrenza=$("#dataDecorrenza").val();
        var telefonoInt=$("#telefonoInt").val();
        var email=$("#email").val();
        var valoreAssicurato=$("#valoreAssicurato").val();
        $.post("${createLink(controller: "polizze", action: 'aggiornaPolizza')}",{ idPolizza: idPolizza, targa:targa, telaio:telaio,dataImmatricolazione:dataImmatricolazione, marchio:marchio, versione:versione, motoveicolo:motoveicolo,dataDecorrenza:dataDecorrenza, dataPosticipata:dataPosticipata,cv:cv,valoreAssicurato:valoreAssicurato,cellulare:cellulare,email:email, _csrf: "${request._csrf.token}"}, function(response) {
            if(response.risposta==true){
                $('#tabellaPolizza').load(window.location.href + ' #tabellaPolizza');
                swal({
                    title: "Polizza salvata!",
                    type: "success",
                    timer: "1800",
                    showConfirmButton: false
                });
            }else{
                swal({
                    title: "Errore salvataggio!",
                    text: ""+ response.errore+"",
                    type: "error",
                    showConfirmButton: true
                });
            }
        }, "json");
    }
    function annullaPolizza(){
        var idPolizza=$("#idPolizza").val();
        $.post("${createLink(controller: "polizze", action: 'annullaPolizza')}",{ idPolizza: idPolizza, _csrf: "${request._csrf.token}"}, function(response) {
            if(response.risposta==true){
                $('#tabellaPolizza').load(window.location.href + ' #tabellaPolizza');
                swal({
                    title: "Polizza aggiornata!",
                    type: "success",
                    timer: "1800",
                    showConfirmButton: false
                });
            }else{
                swal({
                    title: "Errore aggiornamento preventivo!",
                    text: ""+ response.errore+"",
                    type: "error",
                    showConfirmButton: true
                });
            }
        }, "json");
    }
    $(".windowPolizza").on("hidden.bs.modal", function(){
        $( ".alert" ).remove();
        $(this).find('form')[0].reset();
        if($("#dataImmatricolazione").val()!= ''){
            $("#dataImmatricolazione").data('datepicker').setDate(null);
        }
    });

    function verificaTarga() {
        var valore= $("#targa").val();
        var regtarga=/^[a-zA-Z]{2}[0-9]{3,4}[a-zA-Z]{2}$/;
        if(valore!='' && regtarga.test(valore)){ return true; }
        else  {
            if(!regtarga.test(valore) && valore!=''){
                txttargaCliente="il formato della targa è sbagliato";
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txttargaCliente);
            }
            else {
                txttargaCliente="compilare il campo targa";
            }
            return false;
        }
    }
    function verificaEmailContr() {
        var valore= $("#emailDef").val();
        var regemailDef=/^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$/;
        if(valore!='' && regemailDef.test(valore)){
            $( ".alert" ).remove();
            return true;
        }
        else{
            if(!regemailDef.test(valore) && valore!=''){ txtemailCliente="verificare il formato del campo email";}
            else {txttargaCliente="compilare il campo email";}
            return false;
        }
    }
    function monthDiff(d2) {
        var d1=new Date();
        var day1= d1,day2= d2;
        if(day1<day2){
            d1= day2;
            d2= day1;
        }
        var months= (d1.getFullYear()-d2.getFullYear())*12+(d1.getMonth()-d2.getMonth());
        if(d1.getDate()<d2.getDate()) --months;
        return months <= 0 ? 0 : months;
    }
    function verificaDataImmatricolazione(){
        var data1 = $("#dataImmatricolazione").val();
        var format1=new Date($("#dataImmatricolazione").datepicker("getDate"));
        var strDateTime =  format1.getDate() + "-" + (format1.getMonth()+1) + "-" + format1.getFullYear();
        if (data1 !=''){
            d1 = strDateTime;
            d2 = strDateTime;
            var mesi=monthDiff( format1);
            if(mesi>36){
                txtdataImmatricolazione="la data immatricolazione deve essere inferiore a 3 anni";
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtdataImmatricolazione);
                return false;
            }else{
                return true;
            }
        }
        else{return false;  }
    }
    function verificaTelaio() {
        var valore= $("#telaio").val();
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificatargaAcquisizione() {
        var valore= $("#targaAcquisizione").val();
        var regtarga=/^[a-zA-Z]{2}[0-9]{3,4}[a-zA-Z]{2}$/;
        if(valore!='' && regtarga.test(valore)){ return true; }
        else {
            if(!regtarga.test(valore) && valore!=''){txttargaAcquisizione="il formato della targa è sbagliato";}
            else {txttargaCliente="compilare il campo targa";}
            return false;
        }
    }
    function verificaFileAcquisizione() {
        var valore= $("#input-idAcquisizione").val();

        if(valore!=''){ return true; }
        else  return false;
    }
    function verificaValore() {
        var valore= $("#valoreAssicurato").val();
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaDecorrenza() {
        var valore= $("#dataDecorrenza").val();
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaTelefono() {
        var valore= $("#telefonoDef").val();
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaModulo(){
        var valore= $("#input-idmodulo").val();
        if (valore!=''){
            return true;
        }
        else { return false; }
    }
    function verificaNuovaPolizza(){
        if($("#acquisizioneNuovaPolizza").is(":checked")){  return true;}
        else{return false;}
    }
    function verificaBersani(){
        if($("#acquisizioneBersani").is(":checked")){  return true;}
        else{return false;}
    }
    function verificaPassaggio(){
        if($("#acquisizionePassaggio").is(":checked")){  return true;}
        else{return false;}
    }

    $("#targa").on("change", function(event) {
        if(($.trim($("#targa").val()) != '' )&& verificaTarga() && ($.trim($("#emailDef").val()) != '' )  && verificaEmailContr() && verificaTelefono() && verificaDataImmatricolazione() && verificaTelaio()  && verificaValore() && verificaDecorrenza()){
            $( ".alert " ).remove();
            $("#avanti1").prop('disabled', false);
        }
        else{
            if(!verificaTarga()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txttargaCliente);
            }
            $("#avanti1").prop('disabled', true);
        }
    });
    $("#emailDef").on("change", function(event) {
        if(($.trim($("#emailDef").val()) != '' )&& verificaEmailContr() && verificaTelefono() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio()  && verificaValore() && verificaDecorrenza() ){
            $( ".alert " ).remove();
            $("#avanti1").prop('disabled', false);
        }
        else{
            if(!verificaEmailContr()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtemailCliente);
            }
            $("#avanti1").prop('disabled', true);
        }
    });
    $("#telaio").on("change", function(event) {
        if(($.trim($("#telaio").val()) != '' )  && verificaEmailContr() && verificaTelefono()  && verificaDataImmatricolazione() && verificaTarga()   && verificaDecorrenza()  && verificaValore()){
            $("#avanti1").prop('disabled', false);
        }else {
            $("#avanti1").prop('disabled', true);
        }
    });
    $("#telefonoDef").on("change", function(event) {
        if(($.trim($("#telefonoDef").val()) != '' )  && verificaEmailContr() && verificaTelaio()  && verificaDataImmatricolazione() && verificaTarga()   && verificaDecorrenza()  && verificaValore()){
            $("#avanti1").prop('disabled', false);
        }else {
            $("#avanti1").prop('disabled', true);
        }
    });
    $("#valoreAssicurato").on("change", function(event) {
        if(($.trim($("#valoreAssicurato").val()) != '' ) && verificaEmailContr() && verificaTelefono()&& verificaDataImmatricolazione() && verificaTarga() && verificaTelaio()  && verificaDecorrenza() ){
            $("#avanti1").prop('disabled', false);
        }else{
            $("#avanti1").prop('disabled', true);
        }
    });
    //autoveicolo
    $("#autoveicolo").on("change", function(event) {
        if(($.trim($("#autoveicolo").val()) != '' ) && verificaEmailContr() && verificaTelefono()&& verificaDataImmatricolazione() && verificaTarga() && verificaTelaio()  && verificaDecorrenza() ){
            $("#avanti1").prop('disabled', false);
        }else{
            $("#avanti1").prop('disabled', true);
        }
    });
    /*$("#input-idmodulo").on("change", function(event) {
     var valore= $("#input-idmodulo").val();
     if(valore!=''&& verificaValore()==true &&verificaDataImmatricolazione()==true && verificaTarga()==true && verificaTelaio()==true  && verificaDecorrenza()==true  &&( verificaNuovaPolizza()==true ||(verificatargaAcquisizione()==true && verificaFileAcquisizione()==true))){
     if($.trim($("#emailDef").val()) != ''){
     if(verificaEmailContr()==true){$("#bottonSalva").prop('disabled', false);}
     else{$("#bottonSalva").prop('disabled', true);}
     }else{
     $("#bottonSalva").prop('disabled', false);
     }
     }else {
     $( ".alert" ).remove();
     //$( ".datiPoli" ).before( "<div class=\"alert alert-warning\" role=\"alert\"> <span>Completare i dati del cliente: cellulare/email</span></div>" );
     $("#bottonSalva").prop('disabled', true);
     }
     });*/
    /*$('#input-idmodulo').on('fileclear', function(event) {
     $("#bottonSalva").prop('disabled', true);
     });*/

    $("#dataImmatricolazione").on("changeDate", function(event) {
        var data1 = $("#dataImmatricolazione").val().split('-');
        if(data1 != ''){
            if(verificaDataImmatricolazione()){
                if(verificaTelaio() && verificaEmailContr() && verificaTelefono() && verificaTarga()  && verificaValore()  && verificaDecorrenza()){$("#avanti1").prop('disabled', false);}
                else{$("#avanti1").prop('disabled', true);}
            }else{
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtdataImmatricolazione);
                $("#avanti1").prop('disabled', true);
            }

        }else{$("#bottonSalva").prop('disabled', true);}
    });

    $("#targaAcquisizione").on("change", function(event) {
        var valore= $("#targaAcquisizione").val();
        if(valore!=''){
            if((verificaBersani() || verificaPassaggio())  && verificatargaAcquisizione() && verificaFileAcquisizione() ){
                $( ".alert " ).remove();
                $("#avanti2").prop('disabled', false);
            }
            else{
                if(!verificatargaAcquisizione()){
                    $( ".alert " ).remove();
                    $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                    $( ".testoAlert").append(txttargaAcquisizione);
                }
                $("#avanti2").prop('disabled', true);
            }
        }
    });
    //input-idAcquisizone allegatoBersani
    $("#input-idAcquisizione").on("change", function(event) {
        var valore= $("#input-idAcquisizione").val();
        if(valore!='') {
            if((verificaBersani()==true || verificaPassaggio()==true) && verificatargaAcquisizione()==true){
                $("#avanti2").prop('disabled', false);
            }else{$("#avanti2").prop('disabled', true);}
        }
    });
    $('#input-idAcquisizione').on('fileclear', function(event) {
        if(verificaNuovaPolizza()!=true){
            $("#avanti2").prop('disabled', true);
        }
    });
    $("#cliente").on("change", function(event) {
        if($("#cliente").is(":checked")){
            var idPolizza=$("#idPolizza").val();
            $.post("${createLink(controller: "polizze", action: 'getPolizza')}",{ idPolizza: idPolizza,  _csrf: "${request._csrf.token}"}, function(response) {
                <sec:ifHasRole role="ADMIN">
                if(response.risposta==true){
                    $( ".alert" ).remove();
                    $("#cognomeInt").val(response.cognomeInt);
                    $("#nomeInt").val(response.nomeInt);
                    $("#tipoClienteInt").val(response.tipoClienteInt);
                    $("#noPratica").val(response.noPratica);
                    $("#partitaIvaInt").val(response.partitaIvaInt);
                    $("#telefonoDef").val(response.telefonoInt);
                    $("#indirizzoInt").val(response.indirizzoInt);
                    $("#capInt").val(response.capInt);
                    $("#localitaInt").val(response.localitaInt);
                    $("#provinciaInt").val(response.provinciaInt);
                    $("#emailDef").val(response.emailInt);
                    $("#targa").prop("disabled",false);
                    $("#telaio").prop("disabled",false);
                    $("#marca").val(response.marca);
                    $("#dataImmatricolazione").prop("disabled",false);
                    $("#dataDecorrenza").val(response.dataDecorrenza);
                    $("#modello").val(response.modello);
                    var autoveicolo=response.autoveicolo;
                    if(autoveicolo==true){
                        $("#autoV").prop("selected",true);
                    }else{
                        $("#autoC").prop("selected",true);
                    }
                    $("#ragionesociale").val(response.dealer);
                    $("#piva").val(response.piva);
                    $("#indirizzo").val(response.indirizzo);
                    $("#cap").val(response.cap);
                    $("#provincia").val(response.provincia);
                    $("#comune").val(response.comune);
                    $("#telefono").val(response.telefono);
                    var fax;
                    if(response.fax=="null"){
                        fax='';
                    }else{
                        fax=response.fax;
                    }
                    $("#fax").val(fax);
                    $("#email").val(response.email);
                    $("#idPolizza").val(idPolizza);
                    if($("#acquisizioneNuovaPolizza").is(":checked")){
                        $("#targaAcquisizione").val("").prop("disabled", true);
                        if($("#input-idAcquisizione").val()!=''){
                            $("#input-idAcquisizione").fileinput("clear").fileinput("disable");
                        }
                    }
                    if(verificaTarga() && verificaDataImmatricolazione() && verificaTelaio()  && verificaValore() && verificaDecorrenza()  && verificaTelefono() && verificaEmailContr() ){
                        $("#avanti1").prop('disabled', false);
                    }else{
                        $("#avanti1").prop('disabled', true);
                    }
                    $("#modalPolizza").modal({
                        keyboard: true
                    });
                }
                </sec:ifHasRole>
                <sec:ifHasRole role="DEALER">
                if(response.risposta==true && response.stato=="PREVENTIVO"){
                    $( ".alert" ).remove();
                    $("#cognomeInt").val(response.cognomeInt);
                    $("#nomeInt").val(response.nomeInt);
                    $("#tipoClienteInt").val(response.tipoClienteInt);
                    $("#noPratica").val(response.noPratica);
                    $("#partitaIvaInt").val(response.partitaIvaInt);
                    $("#telefonoDef").val(response.telefonoInt);
                    $("#indirizzoInt").val(response.indirizzoInt);
                    $("#capInt").val(response.capInt);
                    $("#localitaInt").val(response.localitaInt);
                    $("#provinciaInt").val(response.provinciaInt);
                    $("#emailDef").val(response.emailInt);
                    $("#targa").prop("disabled",false);
                    $("#telaio").prop("disabled",false);
                    $("#marca").val(response.marca);
                    $("#dataImmatricolazione").prop("disabled",false);
                    $("#dataDecorrenza").val(response.dataDecorrenza);
                    $("#modello").val(response.modello);
                    var autoveicolo=response.autoveicolo;
                    if(autoveicolo==true){
                        $("#autoV").prop("selected",true);
                    }else{
                        $("#autoC").prop("selected",true);
                    }
                    $("#ragionesociale").val(response.dealer);
                    $("#piva").val(response.piva);
                    $("#indirizzo").val(response.indirizzo);
                    $("#cap").val(response.cap);
                    $("#provincia").val(response.provincia);
                    $("#comune").val(response.comune);
                    $("#telefono").val(response.telefono);
                    var fax;
                    if(response.fax=="null"){
                        fax='';
                    }else{
                        fax=response.fax;
                    }
                    $("#fax").val(fax);
                    $("#email").val(response.email);
                    $("#idPolizza").val(idPolizza);
                    if($("#acquisizioneNuovaPolizza").is(":checked")){
                        $("#targaAcquisizione").val("").prop("disabled", true);
                        if($("#input-idAcquisizione").val()!=''){
                            $("#input-idAcquisizione").fileinput("clear").fileinput("disable");
                        }
                    }
                    if(verificaTarga() && verificaDataImmatricolazione() && verificaTelaio()  && verificaValore() && verificaDecorrenza()  && verificaTelefono() && verificaEmailContr() ){
                        $("#avanti1").prop('disabled', false);
                    }else{
                        $("#avanti1").prop('disabled', true);
                    }
                    $("#modalPolizza").modal({
                        keyboard: true
                    });
                }
                </sec:ifHasRole>

            }, "json");
        }
    });
    $("#garante").on("change", function(event) {
        if($("#garante").is(":checked")){
            var idPolizza=$("#idPolizza").val();
            $.post("${createLink(controller: "polizze", action: 'getPolizzaGarante')}",{ idPolizza: idPolizza,  _csrf: "${request._csrf.token}"}, function(response) {
            <sec:ifHasRole role="ADMIN">
                if(response.risposta==true){
                    $( ".alert" ).remove();
                    $("#cognomeInt").val(response.cognomeInt);
                    $("#nomeInt").val(response.nomeInt);
                    $("#tipoClienteInt").val(response.tipoClienteInt);
                    $("#noPratica").val(response.noPratica);
                    $("#partitaIvaInt").val(response.partitaIvaInt);
                    $("#telefonoDef").val(response.telefonoInt);
                    $("#indirizzoInt").val(response.indirizzoInt);
                    $("#capInt").val(response.capInt);
                    $("#localitaInt").val(response.localitaInt);
                    $("#provinciaInt").val(response.provinciaInt);
                    $("#emailDef").val(response.emailInt);
                    $("#targa").prop("disabled",false);
                    $("#telaio").prop("disabled",false);
                    $("#marca").val(response.marca);
                    $("#dataImmatricolazione").prop("disabled",false);
                    $("#dataDecorrenza").val(response.dataDecorrenza);
                    $("#modello").val(response.modello);
                    var autoveicolo=response.autoveicolo;
                    if(autoveicolo==true){
                        $("#autoV").prop("selected",true);
                    }else{
                        $("#autoC").prop("selected",true);
                    }
                    $("#ragionesociale").val(response.dealer);
                    $("#piva").val(response.piva);
                    $("#indirizzo").val(response.indirizzo);
                    $("#cap").val(response.cap);
                    $("#provincia").val(response.provincia);
                    $("#comune").val(response.comune);
                    $("#telefono").val(response.telefono);
                    var fax;
                    if(response.fax=="null"){
                        fax='';
                    }else{
                        fax=response.fax;
                    }
                    $("#fax").val(fax);
                    $("#email").val(response.email);
                    $("#idPolizza").val(idPolizza);
                    if($("#acquisizioneNuovaPolizza").is(":checked")){
                        $("#targaAcquisizione").val("").prop('disabled', true);
                        if($("#input-idAcquisizione").val()!=''){
                            $("#input-idAcquisizione").fileinput("clear").fileinput("disable");
                        }

                    }
                    if(verificaTarga() && verificaDataImmatricolazione() && verificaTelaio()  && verificaValore() && verificaDecorrenza()  && verificaTelefono() && verificaEmailContr() ){
                        $("#avanti1").prop('disabled', false);
                    }else{
                        $("#avanti1").prop('disabled', true);
                    }

                    $("#modalPolizza").modal({
                        keyboard: true
                    });
                }
            </sec:ifHasRole>
            <sec:ifHasRole role="DEALER">
                if(response.risposta==true && response.stato=="PREVENTIVO"){
                    $( ".alert" ).remove();
                    $("#cognomeInt").val(response.cognomeInt);
                    $("#nomeInt").val(response.nomeInt);
                    $("#tipoClienteInt").val(response.tipoClienteInt);
                    $("#noPratica").val(response.noPratica);
                    $("#partitaIvaInt").val(response.partitaIvaInt);
                    $("#telefonoDef").val(response.telefonoInt);
                    $("#indirizzoInt").val(response.indirizzoInt);
                    $("#capInt").val(response.capInt);
                    $("#localitaInt").val(response.localitaInt);
                    $("#provinciaInt").val(response.provinciaInt);
                    $("#emailDef").val(response.emailInt);
                    $("#targa").prop("disabled",false);
                    $("#telaio").prop("disabled",false);
                    $("#marca").val(response.marca);
                    $("#dataImmatricolazione").prop("disabled",false);
                    $("#dataDecorrenza").val(response.dataDecorrenza);
                    $("#modello").val(response.modello);
                    var autoveicolo=response.autoveicolo;
                    if(autoveicolo==true){
                        $("#autoV").prop("selected",true);
                    }else{
                        $("#autoC").prop("selected",true);
                    }
                    $("#ragionesociale").val(response.dealer);
                    $("#piva").val(response.piva);
                    $("#indirizzo").val(response.indirizzo);
                    $("#cap").val(response.cap);
                    $("#provincia").val(response.provincia);
                    $("#comune").val(response.comune);
                    $("#telefono").val(response.telefono);
                    var fax;
                    if(response.fax=="null"){
                        fax='';
                    }else{
                        fax=response.fax;
                    }
                    $("#fax").val(fax);
                    $("#email").val(response.email);
                    $("#idPolizza").val(idPolizza);
                    if($("#acquisizioneNuovaPolizza").is(":checked")){
                        $("#targaAcquisizione").val("").prop('disabled', true);
                        if($("#input-idAcquisizione").val()!=''){
                            $("#input-idAcquisizione").fileinput("clear").fileinput("disable");
                        }

                    }
                    if(verificaTarga() && verificaDataImmatricolazione() && verificaTelaio()  && verificaValore() && verificaDecorrenza()  && verificaTelefono() && verificaEmailContr() ){
                        $("#avanti1").prop('disabled', false);
                    }else{
                        $("#avanti1").prop('disabled', true);
                    }

                    $("#modalPolizza").modal({
                        keyboard: true
                    });
                }
            </sec:ifHasRole>

            }, "json");

        }
    });
    $("#acquisizioneNuovaPolizza").on("change", function(event){
        if($("#acquisizioneNuovaPolizza").is(":checked")){
            $("#targaAcquisizione").val("").prop('disabled', true);
            if($('#input-idAcquisizione').val()!=''){
                $('#input-idAcquisizione').fileinput('clear').fileinput('disable');
            }
            $("#avanti2").prop('disabled', false);
        }
    });
    $("#acquisizioneBersani").on("change", function(event){
        if($("#acquisizioneBersani").is(":checked")){
            $('#input-idAcquisizione').fileinput('enable');
            $("#targaAcquisizione").prop('disabled', false);
            if(verificatargaAcquisizione() && verificaFileAcquisizione() ){
                $("#avanti2").prop('disabled', false);
            }else {$("#avanti2").prop('disabled', true);}
        }
    });
    $("#acquisizionePassaggio").on("change", function(event){
        if($("#acquisizionePassaggio").is(":checked")){
            $('#input-idAcquisizione').fileinput('enable');
            $("#targaAcquisizione").prop('disabled', false);
            if(verificatargaAcquisizione()==true && verificaFileAcquisizione()==true ){
                $("#avanti2").prop('disabled', false);
            }else {
                $("#avanti2").prop('disabled', true);
            }
        }
    });

</script>
</body>
</html>