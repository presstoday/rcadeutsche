<g:applyLayout name="main">
    <html>
    <head>
        <title><g:layoutTitle/></title>
        <g:layoutHead/>
        <style>
        .page-header h1 { display: inline-block; }
        </style>
    </head>
    <body>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div>
                <div class="bs-component">
                    <nav role="navigation" class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <asset:image src="/loghi/deutsche_small.png" style="height:50px; margin-top: 10px; margin-left: 10px;" />
                            </div>
                            <div id="navbarCollapse" class="collapse navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <li><h3 class="testo-navBarTitolo">RCA BMW</h3></li>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"><sec:loggedInUser/><span class="caret"></span></a>
                                        <ul class="dropdown-menu navbar-right" role="menu" >
                                        %{--<li id="estrazione"><a  href="${createLink(controller: "aderenti", action: "creaListaAderenti")}" style="font-size:medium;">Invio ultimo resoconto</a></li>
                                        <li class="divider"></li>--}%
                                            <sec:ifHasRole role="DEALER">
                                                <li><a href="${createLink(controller: "user", action: "datiDealer")}">Profilo</a></li>
                                                <li class="divider"></li>
                                            </sec:ifHasRole>
                                        %{--<li id="allegati"><a href="${createLink(controller: "aderenti", action: "estrazioneAllegati")}" style="font-size:medium;">Scarica ultimo resoconto</a></li>
                                        <li class="divider"></li>--}%
                                            <li><g:link uri="/logout">Esci</g:link> </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <g:layoutBody/>
    </body>
    </html>
</g:applyLayout>