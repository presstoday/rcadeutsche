<html>
<head>
    <meta name="layout" content="navigazione"/>
    <title>Dati Dealer</title>
</head>
<body>
<div class="col-md-12 col-md-push-11"><asset:image src="/loghi/mansutti.png" height="30px" style="margin-bottom: 25px;" /></div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h2 class="text-center h1Login">Aggiornamento dealer </h2>
                %{--<fa:icon name="user-plus" large="5x"/>--}%
            </div>
            <div class="panel-body">
                <f:form>
                    <div class="col-md-12">
                    <sec:ifHasRole role="ADMIN">
                            <div class="col-md-6"><label class="testoParagraph control-label">CODICE DEALER</label></div>
                            <div id="dealer" class="col-md-12">
                                <input id="dealer-input" type="text" align="center" name="dealerCensiti.username"  class="col-md-12 form-control typeahead"  value="<g:if test="${dealerCensiti}">${dealerCensiti.username}</g:if><g:else></g:else>" required="true" placeholder="inserire il codice del dealer">
                                <input type="hidden" name="dealer.id" id="id-dealer" value="${dealerCensiti?.id}" >
                            </div>
                        %{--<div class="col-md-6">
                            <label class="testoParagraph control-label">DEALER</label>
                            <div class="input-group datiUtente">
                                <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                <select class="dealers form-control" name="dealer" id="dealer"  onchange="loadDati.call(this, event);" >
                                    <option value="" selected="selected">SELEZIONA DEALER...</option>
                                    <g:each var="dealer" in="${dealers}">
                                        <option value="${dealer.id}">${dealer.username.padLeft(11,"0")} - ${dealer.ragioneSociale.toUpperCase()}</option>
                                    </g:each>
                                </select>
                            </div>
                        </div>--}%
                       %{-- <div class="col-md-6">
                            <label class="testoParagraph control-label">CODICE</label>
                            <div class="input-group datiUtente">
                                <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="username"  value="" />
                            </div>
                        </div>--}%
                        <div class="col-md-6">
                            <label class="testoParagraph control-label">RAGIONE SOCIALE</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="ragioneSociale" id="ragioneSociale" style="text-transform:uppercase" value=""/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="testoParagraph control-label">P. IVA</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-list-alt"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="piva" id="piva" value=""/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="testoParagraph control-label">PROVINCIA</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-globe"></span></span>
                                <select class="form-control" name="provincia" id="provincia" >
                                    <g:each var="provincia" in="${province}">
                                        <option>${provincia}</option>
                                    </g:each>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="testoParagraph control-label">INDIRIZZO</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-home"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="indirizzo" id="indirizzo" style="text-transform:uppercase" value=""/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="testoParagraph control-label">CAP</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-inbox"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="cap" id="cap" value=""/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="testoParagraph control-label">LOCALITA'</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-globe"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="localita" id="localita" style="text-transform:uppercase" value=""/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="testoParagraph control-label">TELEFONO</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="telefono" id="telefono"  value=""/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="testoParagraph control-label">FAX</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-fax"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="fax" id="fax"  value=""/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="testoParagraph control-label">MAIL</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                <f:input control-class="col-sm-6" type="text" name="email" id="email" value=""/>
                            </div>
                        </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 25px;">
                            <g:link page="1" controller="polizze" action="lista" class="btn btn-annulla col-md-3">Annulla</g:link>
                            %{-- <g:link controller="polizze" action="lista"><div class="btn btn-yellow col-md-offset-2  col-md-6"><b>Ok</b></div></g:link>--}%
                            <button type="submit" class="btn btn-yellow col-md-3 col-md-offset-6">Ok</button>
                        </div>
                    </sec:ifHasRole>
                </f:form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    /*$( ".dealers" ).change(function() {
      var id=  console.log($(this).val())
        alert(id.value());
    });*/
    /*function loadDati(event){
        var idDealer=this.options[this.selectedIndex].value;
        $.post("${createLink(controller: "user", action: 'loadDealer')}",{ idDealer: idDealer, _csrf: "${request._csrf.token}"}, function(response) {
            if(response.risposta==true){
                $("#piva").val(response.piva);
                $("#indirizzo").val(response.indirizzo);
                $("#localita").val(response.localita);
                $("#cap").val(response.cap);
                $("#provincia").val(response.provincia);
                $("#email").val(response.email);
                $("#telefono").val(response.telefono);
                $("#fax").val(response.fax);
            }else{
                $("#piva").val("");
                $("#indirizzo").val("");
                $("#localita").val("");
                $("#cap").val("");
                $("#provincia").val("");
                $("#email").val("");
                $("#telefono").val("");
                $("#fax").val("");
            }
        }, "json");
    };*/

    $('#dealer .typeahead').typeahead({
                hint: true,
                highlight: true,
                minLength:7,
                classNames: {
                    menu: 'popover',
                    dataset: 'popover-content'
                }
            }, {
                name: "elencodealers",
                display: "username",
                limit: 20,
                source: new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: {
                        url: '${createLink(action: "dealerCensiti", params: [username: "nodealer"])}',
                        wildcard: 'nodealer'
                    }
                }),
                templates:{
                    header: Handlebars.compile( '<p class="testoTypeahead"><strong>Codici dealer:</strong></p>'),
                    suggestion: Handlebars.compile('<div>{{#if username}} {{username}} {{else}} <p class="testoTypeahead"><strong><i>il dealer inserito non si trova nell\'elenco di dealer disponibili</i></strong></p>{{/if}}</div>')
                }
            }
    ).on("typeahead:select", function(event, selected) {
        if(selected.id >0){
            //if(selected.username){
                $.post("${createLink(controller: "user", action: 'getDealer')}",{ nodealer: selected.id,  _csrf: "${request._csrf.token}"}, function(response) {
                    var risposta=response.risposta;
                    if(risposta==false){
                    }else{
                        $("#ragioneSociale").val(response.ragioneSociale);
                        $("#piva").val(response.piva);
                        $("#indirizzo").val(response.indirizzo);
                        $("#provincia").val(response.provincia);
                        $("#cap").val(response.cap);
                        $("#localita").val(response.localita);
                        $("#telefono").val(response.telefono);
                        $("#fax").val(response.fax);
                        $("#email").val(response.email);
                    }
                }, "json");
            //}
        }
        $("#id-dealer").val(selected.id);
/*        $.post("${createLink(action: 'datiDealer')}",{ id: selected.id, _csrf: "${request._csrf.token}"}, function(response) {
            if(response.id>0){
                $("#salva").prop('readonly', true);
            }

        });*/
    }).on("blur", function(event) {
        var input = $(this);
        if(input.val() == "") {
            input.typeahead("val", "");
            $("#id-dealer").val(null);
            $.post("${createLink(action: 'datiDealerT')}",{ id: null, _csrf: "${request._csrf.token}"}, function(response) {});
        }
    });
</script>
</body>
</html>