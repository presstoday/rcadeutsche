import grails.util.Environment
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.util.CellReference
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import rcadeutsche.utenti.*
import org.springframework.web.context.support.WebApplicationContextUtils

import java.text.DecimalFormat
import java.util.regex.Pattern

import static org.apache.poi.ss.usermodel.DateUtil.getJavaDate


class BootStrap {
    private def cellValue(cell) {
        switch(cell.cellType) {

            case Cell.CELL_TYPE_BOOLEAN: return cell.booleanCellValue
            case Cell.CELL_TYPE_ERROR: return cell.errorCellValue
            case Cell.CELL_TYPE_NUMERIC: return cell.numericCellValue
            case Cell.CELL_TYPE_STRING: return cell.stringCellValue
            case Cell.CELL_TYPE_FORMULA: return cell.getRichStringCellValue()
            default: return null
        }
    }
    private def cell(row, index, date = false) {
        def value = cellValue(row.getCell(index, Row.CREATE_NULL_AS_BLANK))
        if(date) return value ? getJavaDate(value) : null
        return value
    }
    void caricaExcelDealer() {
        def excel = this.class.getResource("excel/DEALER.xlsx")
        println "Loading DEALERS.xlsx from: ${excel.path}"
        def wb = new XSSFWorkbook(excel.openStream())
        def decimalFormat = new DecimalFormat("#")
        def format = { dato ->
            if(dato instanceof String) return dato
            try {
                return decimalFormat.format(dato)
            } catch(e) {
                println "$dato: ${dato.getClass()} non convertibile"
                return null
            }
        }
        String special = "!@#\$%^&*()_\\/"
        String pattern = ".*[" + Pattern.quote(special) + "].*"
        Dealer.withTransaction {
            wb.getSheetAt(0).with {
                def cellaCodice = new CellReference("A2208")
                def cellaRagioneSociale = new CellReference("B2208")
                def cellIndirizzo = new  CellReference("I2208")
                def cellLocalita = new  CellReference("J2208")
                def cellProvincia = new  CellReference("K2208")
                def cellCap = new  CellReference("L2208")
                def cellTelefono = new  CellReference("M2208")
                def cellPartitaIva = new  CellReference("F2208")
                def cellmail=new CellReference("N2208")
                def ultimaRiga = new CellReference("A2210")
                def firstRow = cellaCodice.row
                def lastRow = ultimaRiga.row
                (firstRow..lastRow).each { rowIndex ->
                    println "Analizzo riga: ${rowIndex}"
                    def row = getRow(rowIndex)
                    def codice=cell(row,cellaCodice.col)
                    codice=codice.toString()
                    //println "cod ingreso-> ${codice}"

                    if(codice.contains(".")){
                        /*def punto=codice.indexOf(".")
                        codice=codice.substring(0,punto).replaceAll("[^0-9]", "")*/
                        codice=Double.valueOf(codice).longValue().toString()
                        //println "cod formatato-> ${codice}"
                    }
                    if (codice.toString().matches("[0].*")) {
                        //codice=codice.toString().replaceFirst("[^1-9]", "")
                        codice=codice.toString().replaceFirst("^0+(?!\\\$)", "")
                        //println "cod formatatowww-> ${codice}"
                    }

                    //println "codi -> ${codice}"
                    //s = s.replaceFirst ("^0*", "");
                    def partitaIva = cell(row, cellPartitaIva.col)
                    partitaIva=partitaIva.toString()
                    if(partitaIva.trim()!='null'){
                        if(!partitaIva.toString().matches("^[a-z|A-Z]{6}[0-9]{2}[A-Z|a-z][0-9]{2}[A-Z|a-z][0-9]{3}[A-Z|a-z]\$") && !partitaIva.matches("^0+\\d*(?:\\.\\d+)?\$")){
                            partitaIva=Double.valueOf(partitaIva).longValue().toString()
                        }
                    }
                    if(!partitaIva.matches("[0-9]{11}")){
                        partitaIva=partitaIva.padLeft(11,"0")
                    }

                    def ragioneSociale = cell(row, cellaRagioneSociale.col)
                    def indirizzo = cell(row, cellIndirizzo.col)
                    def localita = cell(row, cellLocalita.col).toString()
                    def subProv = cell(row, cellProvincia.col).toString()
                    def cap = cell(row, cellCap.col)
                    cap=cap.toString()
                    if(cap.contains(".")){
                        def puntocap=cap.indexOf(".")
                        cap=cap.substring(0,puntocap).replaceAll("[^0-9]", "")
                    }
                    if (!cap.matches("[0-9]{5}")) {
                        cap=cap.padLeft(5,"0")
                    }
                    def telefono = cell(row, cellTelefono.col)
                    telefono=telefono.toString()
                    if(telefono.trim()!='null'){
                        telefono=telefono.trim().toString()
                        if(telefono.contains(".")){
                            telefono=Double.valueOf(telefono).longValue().toString()
                        }
                       /* if(!telefono.matches(pattern) && !telefono.contains("-")){
                            telefono=Double.valueOf(telefono).longValue().toString()
                        }else{
                            telefono=telefono.trim().toString()
                        }*/
                    }

                    def email=cell(row, cellmail.col)
                    def dealer = Dealer.findOrCreateWhere(
                            username: codice,
                            ragioneSociale:ragioneSociale.toString().toUpperCase(),
                            piva:  partitaIva,
                            indirizzo: indirizzo.toString().toUpperCase(),
                            localita: localita.toString().toUpperCase(),
                            cap: cap,
                            telefono: telefono,
                            email: email,
                            provincia: subProv.toString().toUpperCase(),
                            password:partitaIva,
                            enabled: true,
                            attivo: false,
                            passwordScaduta: false
                    )

                    if(dealer.save()) println "Dealer creato correttamente ${codice}"
                    else println "Errore creazione Dealer: ${dealer.errors}"
                }
            }
            wb.close()
        }
       /* def userAdmin= new Admin()
        userAdmin.username= "ITADMIN"
        userAdmin.password="adminMach1"
        userAdmin.nome="admin"
        userAdmin.cognome="admin"
        userAdmin.passwordScaduta=false
        if(userAdmin.save(flush: true)) println "L'utente admin è stato creato"
        else println "Errore creazione utente admin: ${userAdmin.errors}"
       /* def userAdminPost= new Admin()
        userAdminPost.username= "ITADMINPOST"
        userAdminPost.password="adminMach1"
        userAdminPost.nome="adminPosticipate"
        userAdminPost.cognome="adminPosticipate"
        userAdminPost.passwordScaduta=false

        if(userAdminPost.save(flush: true)) {
            userAdminPost.removeRuolo(Ruolo.ADMIN)
            userAdminPost.addRuolo(Ruolo.ADMINPOSTICIPATI)
            println "L'utente admin posticipate è stato creato"
        }
        else println "Errore creazione utente admin posticipate: ${userAdminPost.errors}"*/

    }
    def caricaAdmin(){
        def userAdmin= new Admin()
        userAdmin.username= "ITADMIN"
        userAdmin.password="adminMach1"
        userAdmin.nome="admin"
        userAdmin.cognome="admin"
        userAdmin.passwordScaduta=false
        if(userAdmin.save(flush: true)) println "L'utente admin è stato creato"
        else println "Errore creazione utente admin: ${userAdmin.errors}"
    }
    def caricaAdminDealer(){
        def userAdmin= new Admin()
        userAdmin.username= "adminDealer"
        userAdmin.password="adminDealer"
        userAdmin.nome="adminDealer"
        userAdmin.cognome="adminDealer"
        userAdmin.passwordScaduta=false
        if(userAdmin.save(flush: true)) {
            userAdmin.addRuolo(Ruolo.ADMINDEALER)
            println "L'utente admin dealer è stato creato"
        }
        else println "Errore creazione utente admin: ${userAdmin.errors}"
    }
    def init = { servletContext ->
        if(Environment.current == Environment.DEVELOPMENT) {
            //caricaExcelDealer()
            //caricaExcelTelai()
          // caricaAdminDealer()
        }
        println "RCADEUTSCHE started!"
    }
    def destroy = {
    }
}
