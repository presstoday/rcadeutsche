package rcadeutsche

import utils.*


class FormTagLib implements TagLibUtils {

    static namespace = "f"

    def form = { attrs, body ->
        def csrf = pageScope._csrf
        def content = body()
        if(!content) throwTagError("Il tag [form] di FormTagLib deve essere utilizzato con un body valido")
        if(!attrs.method) attrs.method = "post"
        if(!(attrs.class ==~ ".*form-horizontal.*")) addHtmlClass(attrs, "form-horizontal")
        def html = new HtmlBuilder(out)
        html.form(attrs) {
            if(csrf && attrs.method.toLowerCase() == "post") html.input(type: "hidden", name: csrf.parameterName, value: csrf.token)
            mkp.yieldUnescaped content
        }
    }

    def field = { attrs, body ->
        if(!attrs.type) throwTagError("L'attributo [type] del tag [field] di FormTagLib è obbligatorio")
        def display = attrs.remove("display")
        if(display == "false" || display == false) return
        def fieldAttrs = attrsForPrefix("field", attrs, [class: "form-group form-group-sm"])
        def required = attrs.remove("required") == "true" ? true : false
        if(required) addHtmlClass(fieldAttrs, "required")
        def labelAttrs = attrsForPrefix("label", attrs, [class: "control-label"])
        if(!(labelAttrs.class ==~ ".*col-.*")) addHtmlClass(labelAttrs, "col-sm-2")
        def labelText = getLabel(attrs)

        def controlAttrs = attrsForPrefix("control", attrs)
        if(!(controlAttrs.class ==~ ".*col-.*")) addHtmlClass(controlAttrs, "col-sm-10")
        log.debug "Creazione nuovo field di tipo: ${attrs.type}"
        def html = new HtmlBuilder(out)
        def errors = attrs.remove("errors")
        if(errors) html.div(class: "has-error") {
            html.input(attrs)
            renderErrorsHelpBlock(errors, html)
        }
        html.div(fieldAttrs) {
            if(labelText) label(labelText, labelAttrs)
            html.div(controlAttrs) {
                def type = attrs.type
                if(type == "text" || type == "password" || type == "hidden") mkp.yieldUnescaped input(attrs)
                else if(type == "checkbox") mkp.yieldUnescaped checkbox(attrs)
                else if(type == "textarea") mkp.yieldUnescaped textarea(attrs)
                else if(type == "date") mkp.yieldUnescaped date(attrs)
                else if(type == "select") mkp.yieldUnescaped select(attrs, body)
                else if(type == "integer" || type == "decimal") mkp.yieldUnescaped number(attrs)
                else if(type == "custom") mkp.yieldUnescaped body()
            }
        }
    }

    def input = { attrs ->
        def display = attrs.remove("display")
        if(display == "false" || display == false) return
        prepareAttributes(attrs)
        def errors = attrs.remove("errors")
        def info = attrs.remove("info")
        if(!(attrs.class ==~ "form-control")) addHtmlClass(attrs, "form-control")
        def html = new HtmlBuilder(out)
        if(errors) html.div(class: "has-error") {
            html.input(attrs)
            renderErrorsHelpBlock(errors, html)
        } else html.div() {
            html.input(attrs)
            renderInfoHelpBlock(info, html)
        }
    }

    def password = { attrs ->
        def display = attrs.remove("display")
        if(display == "false" || display == false) return
        attrs.type = "password"
        out << input(attrs)
    }

    def hidden = { attrs ->
        def display = attrs.remove("display")
        if(display == "false" || display == false) return
        attrs.type = "hidden"
        out << input(attrs)
    }

    def number = { attrs ->
        def display = attrs.remove("display")
        if(display == "false" || display == false) return
        prepareAttributes(attrs)
        if(attrs.type != "integer" && attrs.type != "decimal") attrs.type = "decimal"
        def format = attrs.type == "integer" ? "#,##0" : "#,##0.00"
        attrs.type = "text"
        if(attrs.value instanceof Number) {
            if(attrs.containsKey("min")) {
                def min = attrs.remove("min")
                if(min instanceof String) min = min.parseNumber()
                if(attrs.value < min) attrs.value = min
            }
            if(attrs.containsKey("max")) {
                def max = attrs.remove("max")
                if(max instanceof String) max = max.parseNumber()
                if(attrs.value > max) attrs.value = max
            }
            attrs.value = attrs.value.format(format)
        }
        out << input(attrs)
    }

    def textarea = { attrs ->
        def display = attrs.remove("display")
        if(display == "false" || display == false) return
        prepareAttributes(attrs)
        def value = attrs.remove("value") ?: ""
        def errors = attrs.remove("errors")
        attrs.remove("type")
        if(!(attrs.class ==~ "form-control")) addHtmlClass(attrs, "form-control")
        if(attrs.autosize != "false" || Boolean.valueOf(attrs.autosize) != false) addHtmlClass(attrs, "autosize")
        def info = attrs.remove("info")
        def html = new HtmlBuilder(out)
        if(errors) html.div(class: "has-error") {
            html.textarea(attrs) { mkp.yield value }
            renderErrorsHelpBlock(errors, html)
        } else html.div() {
            html.textarea(attrs) { mkp.yield value }
            renderInfoHelpBlock(info, html)
        }
    }

    def checkbox = { attrs ->
        def display = attrs.remove("display")
        if(display == "false" || display == false) return
        prepareAttributes(attrs)
        def errors = attrs.remove("errors")
        attrs.type = "checkbox"
        if(attrs.class ==~ "form-control") removeHtmlClass(attrs, "form-control")
        def checked = Boolean.valueOf(attrs.remove("checked"))
        if(checked) attrs.checked = "checked"
        def info = attrs.remove("info")
        def text = attrs.remove("text")
        def html = new HtmlBuilder(out)
        if(errors) html.div(class: "has-error checkbox") {
            label {
                html.input(attrs)
                if(text) mkp.yield text
                renderErrorsHelpBlock(errors, html)
            }
        } else html.div(class: "checkbox") {
            label {
                html.input(attrs)
                if(text) mkp.yield text
                renderInfoHelpBlock(info, html)
            }
        }
    }
    def radiobutton = { attrs ->
        def display = attrs.remove("display")
        if(display == "false" || display == false) return
        prepareAttributes(attrs)
        def errors = attrs.remove("errors")
        attrs.type = "radio"
        if(attrs.class ==~ "form-control") removeHtmlClass(attrs, "form-control")
        def checked = Boolean.valueOf(attrs.remove("checked"))
        if(checked) attrs.checked = "checked"
        def info = attrs.remove("info")
        def text = attrs.remove("text")
        def html = new HtmlBuilder(out)
        if(errors) html.div(class: "has-error radio") {
            label {
                html.input(attrs)
                if(text) mkp.yield text
                renderErrorsHelpBlock(errors, html)
            }
        } else html.div(class: "radio") {
            label {
                html.input(attrs)
                if(text) mkp.yield text
                renderInfoHelpBlock(info, html)
            }
        }
    }

    def date = {attrs ->
        def display = attrs.remove("display")
        if(display == "false" || display == false) return
        //prepareAttributes(attrs)
        attrs.type = "text"
        attrs["data-provide"] = "datepicker"
        def datepickerOptions = [
            language: "it",
            format: "dd-mm-yyyy",
            "today-btn": attrs.remove("todayBtn") == "false" ? null : "linked",
            autoclose: true,
            "today-highlight": true,
            "toggle-active": attrs.remove("toggleActive") == "false" ? false : true,
            "disable-touch-keyboard": true,
            "enable-on-readonly": false
        ]
        if(attrs.orientation && attrs.orientation != "auto") datepickerOptions["orientation"] = attrs.remove("orientation")
        if(attrs.remove("calendarWeeks") == "true") datepickerOptions["calendar-weeks"] = true
        if(attrs.startDate) {
            def startDate = attrs.remove("startDate")
            if(startDate instanceof Date) startDate = startDate.format("dd-MM-yyyy")
            datepickerOptions["start-date"] = startDate
        }
        if(attrs.endDate) {
            def endDate = attrs.remove("endDate")
            if(endDate instanceof Date) endDate = endDate.format("dd-MM-yyyy")
            datepickerOptions["end-date"] = endDate
        }
        datepickerOptions.each { option, value -> attrs["data-date-${option}"] = value }
        if(attrs.value instanceof Date) attrs.value = attrs.value.format("dd-MM-yyyy")
        out << input(attrs)
    }

    def select = { attrs, body ->
        def display = attrs.remove("display")
        if(display == "false" || display == false) return
        prepareAttributes(attrs)
        def value = attrs.remove("value")
        def errors = attrs.remove("errors")
        def info = attrs.remove("info")
        attrs.remove("type")
        Collection from = attrs.remove("from") ?: []
        //def multiple = attrs.multiple == "false" || Boolean.valueOf(attrs.multiple)
       // if(multiple) attrs.multiple = "multiple"
        def nullText = "Scegli..."
        if(attrs.nullText) {
            if(attrs.nullText == "false" || attrs.nullText == false) nullText = false
            else nullText = attrs.remove("nullText")
        }
        def groupBy = attrs.remove("groupBy")
        attrs.remove("nullText")
        def key = attrs.remove("key")
        def text = attrs.remove("option")
        addHtmlClass(attrs, "selectpicker show-tick")
        def optionDataAttrs = attrsForPrefix("option-data", attrs)
        def selectpickerOptions = [
            style: "form-control btnSelect",
            //mobile: request.currentDevice.isMobile() || request.currentDevice.isTablet(),
            "live-search": attrs.search == "true" || Boolean.valueOf(attrs.search) || from.size() > 10 ? true : false,
            container: attrs.remove("container") ?: "body",
            size: attrs.size == "false" || Boolean.valueOf(attrs.size) == false ? false : attrs.size ==~ "[0-9]+" ? attrs.remove("size") : "auto",
            width: attrs.remove("width") ?: "100%"
        ]
        if(attrs.header) selectpickerOptions["header"] = attrs.remove("header")
        if(attrs.showSubtext == "true") selectpickerOptions["show-subtext"] = attrs.remove("showSubtext")  //da accoppiare a option-data-subtext="..."
        selectpickerOptions.each { option, val -> attrs["data-${option}"] = val }
        def html = new HtmlBuilder(out)
        def renderSelect = {
            html.select(attrs) {
                if (nullText) option(value: "") { mkp.yield nullText }
                def processOption = { item ->
                    def itemValue = item
                    def compareValue = value
                    if (key) {
                        itemValue = item.beanProperty(key)
                        compareValue = value.beanProperty(key)
                    } else {
                        if (item instanceof Enum) {
                            itemValue = item.name()
                            compareValue = value.beanProperty("name()")
                        } else if (item instanceof Map && item.value) itemValue = item.value
                        else if(item instanceof Class) itemValue = item.name
                    }
                    def itemOption = item
                    if(text) itemOption = item.beanProperty(text)
                    else {
                        if(item instanceof Map && item.text) itemOption = item.text
                        else if(item instanceof Class) itemOption = item.simpleName.splitCamelCase()
                    }
                    def selected = compareValue ? (multiple ? itemValue in compareValue : itemValue == compareValue) : false
                    log.debug "selected option: ${compareValue} ? (${multiple} ? ${itemValue} in ${compareValue} : ${itemValue} == ${compareValue}) -> ${selected}"
                    def optionAttrs = [value: itemValue]
                    optionDataAttrs.each { dataKey, dataValue -> optionAttrs = mergeAttrs(optionAttrs, ["data-${dataKey}": evaluate(dataValue, item)]) }
                    if (selected) optionAttrs.selected = "selected"
                    option(optionAttrs) { mkp.yieldUnescaped itemOption.toString() }
                }
                if (groupBy) {
                    def options = groupBy instanceof Closure ? from.groupBy(groupBy) : from.groupBy { item -> item.beanProperty(groupBy) }
                    options.each { group, values -> optgroup(label: group) { values.each processOption } }
                } else from.each processOption
                mkp.yieldUnescaped body()?.trim()
            }
        }
        if(errors) html.div(class: "has-error") {
            renderSelect()
            renderErrorsHelpBlock(errors, html)
        } else html.div() {
            renderSelect()
            renderInfoHelpBlock(info, html)
        }
    }

    private String getLabel(Map attrs) {
        if(attrs.label) {
            def label = attrs.remove("label")
            if(label == "false") return ""
            else return label
        } else {
            String name = attrs.name
            String property = attrs.property ?: name
            if(property) return property.splitCamelCase(true)
            else return ""
        }
    }

    private void prepareAttributes(Map attrs) {
        String name = attrs.name
        String property = attrs.remove("property") ?: name
        def bean = attrs.remove("bean")
        // id
        if(!attrs.id) {
            def id = property ? property.hyphenate() : null
            if(id == null) id = randomId()
            attrs.id = id
        }
        log.debug "ID field ${property}: ${attrs.id}"
        // value
        def value = bean && property ? bean.beanProperty(property) : attrs.value
        if(value == null) {
            if(attrs.containsKey("defaultValue")) value = attrs.remove("defaultValue")
            else value = ""
        } else attrs.remove("defaultValue")
        attrs.value = value
        log.debug "Value field ${property}: ${attrs.value}"
        // errors
        attrs.errors = attrs.errors ?: !bean ? "" : property ? hasErrors(bean: bean, field: property) { renderErrors(bean: bean, field: property) } : hasErrors(bean: bean) { renderErrors(bean: bean) }
        if(attrs.renderErrors) {
            def renderErrors = attrs.remove("renderErrors")
            if(attrs.errors.size() > 0) {
                if(renderErrors == "tooltip") {
                    attrs.title = attrs.remove("errors")
                    attrs["data-toggle"] = "tooltip"
                    attrs.errors = true
                }
            }
        }
        log.debug "Errori field ${property}: ${attrs.errors}"
    }

    private void renderErrorsHelpBlock(errors, HtmlBuilder html) {
        if(errors && !(errors instanceof Boolean)) html.span(class: "help-block") {
            mkp.yieldUnescaped errors
        }
    }

    private void renderInfoHelpBlock(String info, HtmlBuilder html) {
        if(info) html.span(class: "help-block") {
            mkp.yieldUnescaped fa.icon(name: "info", class: "help-icon")
            mkp.yield info
        }
    }

}
