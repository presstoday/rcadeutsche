package rcadeutsche

import utils.HtmlBuilder

class UtilsTagLib {

    static namespace = "g"

    def renderErrors = { attrs, body ->
        if(attrs.html == "false" || attrs.html == false) {
            def errors = []
            eachError(attrs) { error -> errors << message(error: error) }
            out << errors.join("\n")
        } else {
            def html = new HtmlBuilder(out)
            html.ul(class: "error-list") {
                mkp.yieldUnescaped eachError(attrs) { error -> html.li() { mkp.yieldUnescaped message(error: error) } }
            }
        }
    }

    def renderFlash = { attrs, body ->
        def message = attrs.remove("message")
        def cssClass = attrs.remove("class")
        def messages = [
            "message": "alert-info",
            "important": "alert-primary",
            "success": "alert-success",
            "warning": "alert-warning",
            "errors": "alert-danger",
            "error": "alert-danger"
        ]
        if(message && message in messages.keySet() && flash[message]) {
            attrs.message = flash[message]
            attrs.class = cssClass ?: messages[message]
            out << bs.alert(attrs)
        }
        else messages.each { key, alertClass ->
            attrs.message = flash[key]
            attrs.class = cssClass ?: alertClass
            if(flash[key]) out << bs.alert(attrs)
        }
    }

}
