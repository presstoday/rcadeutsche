package rcadeutsche

import utils.*


class FontAwesomeTagLib implements TagLibUtils {

    static namespace = "fa"

    def icon = { attrs ->
        String icon = attrs.remove("name") ?: throwTagError("L'attributo [name] del tag [fa:icon] è obbligatorio")
        def large = attrs.remove("large").with {
            if(it == "true") return "fa-lg"
            else if(it ==~ "[1-5]x") return "fa-${it}"
            else if(it ==~ "fa-[1-5]x") return it
            else return false
        }
        def fixedWidth = attrs.remove("fixedWidth") == "true" ? "fa-fw" : false
        def border = attrs.remove("border") == "true" ? "fa-border" : false
        def animated = attrs.remove("animated") == "true" ? "fa-spin" : false
        def iconAttrs = attrs
        if(!(iconAttrs.class ==~ "fa")) addHtmlClass(iconAttrs, "fa")
        if(!icon.startsWith("fa-")) icon = "fa-" + icon
        addHtmlClass(iconAttrs, icon)
        if(large) addHtmlClass(iconAttrs, large)
        if(fixedWidth) addHtmlClass(iconAttrs, fixedWidth)
        if(border) addHtmlClass(iconAttrs, border)
        if(animated) addHtmlClass(iconAttrs, animated)
        new HtmlBuilder(out).i("", iconAttrs)
    }

}
