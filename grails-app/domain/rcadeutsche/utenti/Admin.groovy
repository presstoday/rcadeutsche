package rcadeutsche.utenti

class Admin  extends Utente {

    String nome
    String cognome


    static constraints = {
        nome blank: false
        cognome blank: false
    }

    static mapping = {
        table "admins"
    }

    String toString() { return "${username}" }

    Set<Ruolo> getDefaultRoles() { return [Ruolo.ADMIN] }
}
