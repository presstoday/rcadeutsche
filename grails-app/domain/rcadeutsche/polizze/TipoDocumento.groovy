package rcadeutsche.polizze

enum TipoDocumento {
    PASSAGGIO("passaggio da vecchio veicolo"),
    BERSANI("bersani"),
    CERTIFICATO("certificato_polizza"),
    MODULO_ADESIONE("modulo di adesione")


    final String value
    private TipoDocumento(String value) {
        this.value = value
    }
    String toString() { return value }
    static list() { return [PASSAGGIO, BERSANI,CERTIFICATO,MODULO_ADESIONE] }
}
