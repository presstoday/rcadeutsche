package rcadeutsche.polizze
import rcadeutsche.utenti.*
import org.grails.databinding.BindingFormat

class Polizza {
    Date dateCreated = new Date()
    String noPolizza, noPratica, indirizzoInt,capInt,localitaInt,telefonoInt,emailInt,indirizzoGarante,capGarante,localitaGarante,telefonoGarante,emailGarante
    BigDecimal valoreAssicurato
    @BindingFormat("uppercase")String partitaIvaInt
    @BindingFormat("uppercase")String nomeInt
    @BindingFormat("uppercase")String cognomeInt
    @BindingFormat("uppercase")String provinciaInt
    @BindingFormat("uppercase")String partitaIvaGarante
    @BindingFormat("uppercase")String nomeGarante
    @BindingFormat("uppercase")String cognomeGarante
    @BindingFormat("uppercase")String provinciaGarante
    @BindingFormat("uppercase")String marca
    @BindingFormat("uppercase")String modello
    @BindingFormat("uppercase")String targa
    @BindingFormat("uppercase")String targaAcquisizione
    @BindingFormat("uppercase")String telaio
    String stato = "PREVENTIVO"
    TipoCliente tipoClienteInt = TipoCliente.M
    TipoCliente tipoClienteGarante = TipoCliente.M
    Acquisizione tipoAcquisizione = Acquisizione.NUOVA_POLIZZA
    Contraente tipoContraente = Contraente.INTESTATARIO
    Date dataDecorrenza
    Date dataScadenza
    Date dataImmatricolazione
    boolean accettata =false
    boolean autoveicolo =false
    boolean adesione =false
    Tracciato tracciato


    static belongsTo = [dealer: Dealer]
    static hasMany = [documenti: Documenti]
    static hasOne = [tracciatoCompagnia: TracciatoCompagnia]

    static constraints = {
        noPratica nullable: false, unique: true
        noPolizza nullable: true
        telaio nullable: true, unique: true
        dataDecorrenza nullable: true
        dataScadenza nullable: true
        tracciato nullable: true
        tracciatoCompagnia nullable: true
        dealer nullable: false
        dataImmatricolazione nullable: true
        marca nullable: true
        indirizzoInt nullable: false
        telefonoInt nullable: true
        partitaIvaInt nullable: false
        nomeInt nullable: false
        cognomeInt nullable: false
        capInt nullable: false
        localitaInt nullable: false
        provinciaInt nullable: false
        emailInt nullable: true
        targa nullable: true, unique: true
        modello nullable: true
        valoreAssicurato nullable: true
        indirizzoGarante nullable: false
        telefonoGarante nullable: true
        partitaIvaGarante nullable: false
        nomeGarante nullable: false
        cognomeGarante nullable: false
        capGarante nullable: false
        localitaGarante nullable: false
        provinciaGarante nullable: false
        emailGarante nullable: true
        targaAcquisizione nullable: true

        /* dataImmatricolazione validator: { data ->
             /* if(data > dataMassima) return ["polizze.dataImmatricolazione.data.max", data, dataMassima]
             def today = new Date(), minimo = use(TC) { today - 4.years }, massimo = use(TC) { today - 8.years }
             if(data > minimo) return "polizza.dataImmatricolazione.min.notmet"
             if(data < massimo) return "polizza.dataImmatricolazione.max.exceeded"*/
        // }
    }
    static mapping = {

    }
    def afterInsert(){
        if(stato =="PREVENTIVO") noPolizza = "PREV" + id.toString().padLeft(6, "0")
        else noPolizza = "DE" + id.toString().padLeft(8, "0")
    }
    def afterUpdate() {
        if(stato =="POLIZZA_IN_ATTESA_ATTIVAZIONE") noPolizza = "DE" + id.toString().padLeft(8, "0")
    }

    void setDataDecorrenza(Date data) {
        if(data) {
            dataDecorrenza = data
            stato == "POLIZZA_IN_ATTESA_ATTIVAZIONE"
        }
    }
}
