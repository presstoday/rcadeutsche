package rcadeutsche.polizze

enum StatoPolizza {
    PREVENTIVO("preventivo"), PREVENTIVO_SCADUTO("preventivo scaduto"),  POLIZZA("polizza"), POLIZZA_SCADUTA("polizza scaduta"),ANNULLATA("annullata"), POLIZZA_IN_ATTESA_ATTIVAZIONE("polizza in attesa di attivazione")
    final String stato
    private StatoPolizza(String stato) { this.stato = stato }
    String toString() { return stato }
    static list() { return [PREVENTIVO,  POLIZZA, PREVENTIVO_SCADUTO,ANNULLATA,POLIZZA_IN_ATTESA_ATTIVAZIONE] }
}
enum Acquisizione {
    NUOVA_POLIZZA("nuova polizza"),PASSAGGIO("passaggio"), BERSANI("Bersani")
    final String acquisizione
    private Acquisizione(String acquisizione) { this.acquisizione = acquisizione }
    String toString() { return acquisizione }
    static list() { return [PASSAGGIO,  BERSANI] }
}
enum Contraente {
    INTESTATARIO("intestatario"),GARANTE("garante")
    final String contraente
    private Contraente(String contraente) { this.contraente = contraente }
    String toString() { return contraente }
    static list() { return [INTESTATARIO,  GARANTE] }
}

enum TipoCliente {
    M("uomo", "M"), F("donna", "F"), DITTA_INDIVIDUALE("ditta individuale", ""), SOCIETA("società", "")
    final String tipo, value
    private TipoCliente(String tipo, String value) {
        this.tipo = tipo
        this.value = value
    }
    String toString() { return tipo }
    static list() { return [M, F, SOCIETA, DITTA_INDIVIDUALE] }
}