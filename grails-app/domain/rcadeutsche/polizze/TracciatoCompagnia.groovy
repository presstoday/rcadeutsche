package rcadeutsche.polizze

class TracciatoCompagnia {
    /***contiene i dati per rigenerare il tracciato*/
    String dataCaricamento, dataErogazione,importoRichiesto,importoFinanziato,importoNettoErogato,codProdotto,categoriaBene,beneFinanziato
    static belongsTo = [polizza: Polizza]
    static constraints = {
        dataCaricamento nullable: true
        dataErogazione nullable: true
        importoRichiesto nullable: true
        importoFinanziato nullable: true
        importoNettoErogato nullable: true
        codProdotto nullable: true
        categoriaBene nullable: true
        beneFinanziato nullable: true
    }
    static mapping = {
        table "tracciati_compagnia"

    }


}
