package rcadeutsche.polizze

class DatiContabili {

    BigDecimal imposte= 0.0, provvigioniDealer= 0.0, imponibile= 0.0, totale= 0.0, pdu= 0.0, provvigioniMansutti= 0.0, provvigioniMach1= 0.0



    static constraints = {
        imposte  min: 0.0
        provvigioniDealer  min: 0.0
        imponibile  min: 0.0
        pdu  min: 0.0
        provvigioniMansutti  min: 0.0
        provvigioniMach1  min: 0.0
        totale  min: 0.0
    }
}
