package rcadeutsche

import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.PdfContentByte
import com.itextpdf.text.pdf.PdfReader
import com.itextpdf.text.pdf.PdfStamper
import com.presstoday.groovy.Stopwatch
import excel.builder.ExcelBuilder
import excel.reader.*
import grails.converters.JSON
import groovy.time.TimeCategory as TC
import rcadeutsche.polizze.Acquisizione
import rcadeutsche.polizze.Documenti
import rcadeutsche.polizze.Polizza
import rcadeutsche.polizze.StatoPolizza
import rcadeutsche.polizze.TipoCliente
import rcadeutsche.polizze.TipoDocumento
import rcadeutsche.polizze.TracciatoCompagnia
import rcadeutsche.utenti.Admin
import rcadeutsche.utenti.Dealer
import security.SecurityUtils
import utils.HtmlBuilder
import rcadeutsche.utenti.Log

import java.util.regex.Pattern

class PolizzeController {
    def holidayService
    def tracciatiService
    def index() {}
    def lista(){
       //CaricaPolizzeJob.triggerNow()
       //MailSeraleJob.triggerNow()
       //ScaricaPolizzeJob.triggerNow()
        def principal = SecurityUtils.principal
        def utente,dealer,admin
        if(SecurityUtils.hasRole("ADMIN")){
            admin=Admin.get(principal.id)
            utente=admin
        }

        else if(SecurityUtils.hasRole("DEALER")){
            dealer=Dealer.get(principal.id)
            utente=dealer
        }
        def max = 15, pagesPerPage = 7
        def page = (params.page ?: 1) as int
        def offset = (page - 1) * max
        def cerca = params.search
        def polizze = Polizza.createCriteria().list(max: max, offset: offset) {
            if(dealer) {
                eq "dealer", dealer
            }
            else {
                projections {
                    createAlias ('dealer', 'd')
                }
            }
            if(cerca) {
                or {
                    ilike "noPratica", "%${cerca}%"
                    ilike "cognomeInt", "%${cerca}%"
                    ilike "cognomeGarante", "%${cerca}%"
                    ilike "partitaIvaInt", "%${cerca}%"
                    ilike "partitaIvaGarante", "%${cerca}%"
                    //ilike "tipoCliente",TipoCliente.DITTA_INDIVIDUALE.tipo
                    if(!dealer){
                        ilike "d.ragioneSociale", "%${cerca}%"
                    }
                }
            }
            and {
                order('stato', 'desc')
                if(!dealer){
                    order('d.ragioneSociale', 'asc')
                }

            }
        }

        def pages = Math.ceil(polizze.totalCount / max) as int
        def totalPages=pages
        if(pages <= 1) pages = []
        else if(pages <= pagesPerPage) pages = 1..pages
        else if(page < (pagesPerPage / 2))  pages = 1..pagesPerPage
        else {
            def minPage = page - ((pagesPerPage / 2) as int)
            def maxPage = page + ((pagesPerPage / 2) as int)
            if(maxPage > pages) pages = (maxPage - pagesPerPage)..pages
            else pages = minPage..maxPage
        }
        if(!pages && page > 1) {
            redirect action: "lista", params: params.search ? [search: params.search] : null
        }
        else if(pages && page > pages.max()){
            redirect action: "lista", params: [page: 1, search: params.search]
        }
        [polizze: polizze, pages: pages, page: page,utente:utente, totalPages:totalPages]
    }
    def getPolizza(String idPolizza){
        def polizza=Polizza.get(idPolizza)
        def risposta
        if(polizza){
            def dataDecorrenza = holidayService.dataDecorr()

            risposta=[noPratica:polizza.noPratica,stato:polizza.stato.toString().toUpperCase(),cognomeInt:polizza.cognomeInt.toString().toUpperCase(),nomeInt:polizza.nomeInt.toString().toUpperCase(),tipoClienteInt:polizza.tipoClienteInt.value.toString().toUpperCase(),partitaIvaInt:polizza.partitaIvaInt,indirizzoInt:polizza.indirizzoInt?:"",capInt:polizza.capInt?:"",localitaInt:polizza.localitaInt?:"",provinciaInt:polizza.provinciaInt?:"",dealer:polizza.dealer.ragioneSociale,piva:polizza.dealer.piva,indirizzo:polizza.dealer.indirizzo,cap:polizza.dealer.cap,provincia:polizza.dealer.provincia,pref:polizza.dealer.pref?:"",telefono:polizza.dealer.telefono?:"",fax:polizza.dealer.fax?:"",comune:polizza.dealer.localita,email:polizza.dealer.email?:"",targa:polizza.targa?:"",telaio:polizza.telaio?:"",marca:polizza.marca?:"",dataImmatricolazione:polizza.dataImmatricolazione?.format("dd-MM-yyyy")?:"",dataDecorrenza:(polizza.dataDecorrenza)?polizza.dataDecorrenza.format("dd-MM-yyyy"):dataDecorrenza.format("dd-MM-yyyy"),modello:polizza.modello?:"",autoveicolo:polizza.autoveicolo?:"",telefonoInt:polizza.telefonoInt?:"",emailInt:polizza.emailInt?:"",valoreAssicurato:polizza.valoreAssicurato?:"",risposta:true]
        }
        else{risposta=[risposta:false]}
        render risposta as JSON
    }
    def getPolizzaAdmin(String idPolizza){
        def polizza=Polizza.get(idPolizza)
        def risposta
        if(polizza){
            def dataDecorrenza = holidayService.dataDecorr()
            risposta=[noPratica:polizza.noPratica,stato:polizza.stato.toString().toUpperCase(),cognomeInt:(polizza.tipoContraente.toString()=="intestatario")?polizza.cognomeInt.toString().toUpperCase():polizza.cognomeGarante.toString().toUpperCase(),nomeInt:(polizza.tipoContraente.toString()=="intestatario")?polizza.nomeInt.toString().toUpperCase():polizza.nomeGarante.toString().toUpperCase(),tipoClienteInt:(polizza.tipoContraente.toString()=="intestatario")?polizza.tipoClienteInt.value.toString().toUpperCase():polizza.tipoClienteGarante.value.toString().toUpperCase(),partitaIvaInt:(polizza.tipoContraente.toString()=="intestatario")?polizza.partitaIvaInt:polizza.partitaIvaGarante,indirizzoInt:polizza.indirizzoInt?:"",capInt:(polizza.tipoContraente.toString()=="intestatario")?polizza.capInt?:"":polizza.capGarante?:"",localitaInt:(polizza.tipoContraente.toString()=="intestatario")?polizza.localitaInt?:"":polizza.localitaGarante?:"",provinciaInt:(polizza.tipoContraente.toString()=="intestatario")?polizza.provinciaInt?:"":polizza.provinciaGarante?:"",dealer:polizza.dealer.ragioneSociale,piva:polizza.dealer.piva,indirizzo:polizza.dealer.indirizzo,cap:polizza.dealer.cap,provincia:polizza.dealer.provincia,pref:polizza.dealer.pref?:"",telefono:polizza.dealer.telefono?:"",fax:polizza.dealer.fax?:"",comune:polizza.dealer.localita,email:polizza.dealer.email?:"",targa:polizza.targa?:"",telaio:polizza.telaio?:"",marca:polizza.marca?:"",dataImmatricolazione:polizza.dataImmatricolazione?.format("dd-MM-yyyy")?:"",dataDecorrenza:(polizza.dataDecorrenza)?polizza.dataDecorrenza.format("dd-MM-yyyy"):dataDecorrenza.format("dd-MM-yyyy"),modello:polizza.modello?:"",autoveicolo:polizza.autoveicolo.toString()?:"",telefonoInt:(polizza.tipoContraente.toString()=="intestatario")?polizza.telefonoInt?:"":polizza.telefonoGarante?:"",emailInt:(polizza.tipoContraente.toString()=="intestatario")?polizza.emailInt?:"":polizza.emailGarante?:"",valoreAssicurato:polizza.valoreAssicurato?:"",tipoContraente:polizza.tipoContraente.toString()?:"",tipoAcquisizione:polizza.tipoAcquisizione.toString(),targaAcquisizione:(polizza.targaAcquisizione)?polizza.targaAcquisizione:"",risposta:true]
        }
        else{risposta=[risposta:false]}
        render risposta as JSON
    }
    def getPolizzaGarante(String idPolizza){
        def polizza=Polizza.get(idPolizza)
        def risposta
        if(polizza){
            def dataDecorrenza=holidayService.dataDecorr()

            risposta=[noPratica:polizza.noPratica,stato:polizza.stato.toString().toUpperCase(),cognomeInt:polizza.cognomeGarante.toString().toUpperCase(),nomeInt:polizza.nomeGarante.toString().toUpperCase(),tipoClienteInt:polizza.tipoClienteGarante.value.toString().toUpperCase(),partitaIvaInt:polizza.partitaIvaGarante,indirizzoInt:polizza.indirizzoGarante?:"",capInt:polizza.capGarante?:"",localitaInt:polizza.localitaGarante?:"",provinciaInt:polizza.provinciaGarante?:"",dealer:polizza.dealer.ragioneSociale,piva:polizza.dealer.piva,indirizzo:polizza.dealer.indirizzo,cap:polizza.dealer.cap,provincia:polizza.dealer.provincia,pref:polizza.dealer.pref?:"",telefono:polizza.dealer.telefono?:"",fax:polizza.dealer.fax?:"",comune:polizza.dealer.localita,email:polizza.dealer.email?:"",targa:polizza.targa?:"",telaio:polizza.telaio?:"",marca:polizza.marca?:"",dataImmatricolazione:polizza.dataImmatricolazione?.format("dd/MM/yyyy")?:"",dataDecorrenza:(polizza.dataDecorrenza)?polizza.dataDecorrenza.format("dd-MM-yyyy"):dataDecorrenza.format("dd-MM-yyyy"),modello:polizza.modello?:"",autoveicolo:polizza.autoveicolo?:"",telefonoInt:polizza.telefonoGarante?:"",emailInt:polizza.emailGarante?:"",valoreAssicurato:polizza.valoreAssicurato?:"",autoveicolo:polizza.autoveicolo,risposta:true]
        }
        else{risposta=[risposta:false]}
        render risposta as JSON
    }
    def aggiornaPolizza(long idPolizza){
        def logg
        if(Polizza.exists(idPolizza)) {
            def polizza=Polizza.get(idPolizza)
            /***data scadenza*/
            //Date.parse("dd-MM-yyyy", params.dataDecorrenza)
            //def dataScadenza= use(TC) { Date.parse("dd-MM-yyyy", params.dataDecorrenza) + 12.months}
            def dataScadenza=params.dataDecorrenza ? use(TC) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +12.months}:""
            if(dataScadenza!=""){
                GregorianCalendar calendar = new GregorianCalendar()
                def yearInt = Integer.parseInt(dataScadenza.format("yyyy"))
                def monthInt = Integer.parseInt(dataScadenza.format("MM"))
                monthInt = monthInt - 1
                calendar.set(yearInt, monthInt, 1)
                def dayInt = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH)
                dataScadenza=new GregorianCalendar(yearInt,monthInt,dayInt).format("dd-MM-yyyy")
                Date dataFormatata = Date.parse( "dd-MM-yyyy", dataScadenza )
                polizza.dataScadenza=dataFormatata
            }
                bindData(polizza, params, [exclude: ['indirizzoInt', 'capInt','localitaInt','telefonoInt','emailInt','partitaIvaInt','nomeInt','cognomeInt','provinciaInt','tipoClienteInt','indirizzoGarante','capGarante','localitaGarante','partitaIvaGarante','nomeGarante','cognomeGarante','provinciaGarante','tipoClienteGarante']])
                /*if(params.autoveicolo=="AUTOCARRO"){polizza.autoveicolo=false}
                else{polizza.autoveicolo=true}*/
                polizza.stato="POLIZZA_IN_ATTESA_ATTIVAZIONE"
                if(params.tipoContraente=="GARANTE"){
                    polizza.telefonoGarante=params.telefonoDef.toString().trim()?:null
                    polizza.emailGarante=params.emailDef.toString().trim()?:null
                }else if(params.tipoContraente=="INTESTATARIO"){
                    polizza.telefonoInt=params.telefonoDef.toString().trim()?:null
                    polizza.emailInt=params.emailDef.toString().trim()?:null
                }
                //aggiornamenti dati dealer
                if((params.telefono.toString().trim()!='' && params.telefono.toString().trim() != polizza.dealer.telefono ))polizza.dealer.telefono=params.telefono.toString().trim()
                if((params.fax.toString().trim()!='' && params.fax.toString()!= polizza.dealer.fax))polizza.dealer.fax=params.fax.toString().trim()
                if((params.email.toString().trim()!='' && params.email.toString()!= polizza.dealer.email)) polizza.dealer.email=params.email.toString().trim()
                if(params.tipoAcquisizione=="NUOVA_POLIZZA"){
                    polizza.targaAcquisizione=null
                }else if((params.tipoAcquisizione=="PASSAGGIO" || params.tipoAcquisizione=="BERSANI" )&& params.fileContentAcquisizione.size>0 ){
                    def documentiAcquisizione=new Documenti()
                    if(params.tipoAcquisizione=="PASSAGGIO"){
                        polizza.targaAcquisizione=params.targaAcquisizione?:null
                        documentiAcquisizione.tipo=TipoDocumento.PASSAGGIO
                        documentiAcquisizione.fileContent=params.fileContentAcquisizione.bytes?:null
                        documentiAcquisizione.fileName=params.fileContentAcquisizione?.originalFilename?:null
                    }else if (params.tipoAcquisizione=="BERSANI"){
                        polizza.targaAcquisizione=params.targaAcquisizione?:null
                        documentiAcquisizione.tipo=TipoDocumento.BERSANI
                        documentiAcquisizione.fileContent=params.fileContentAcquisizione.bytes?:null
                        documentiAcquisizione.fileName=params.fileContentAcquisizione?.originalFilename?:null
                    }
                    polizza.addToDocumenti(documentiAcquisizione)
                }

            if(params.fileContentModAdesione?.size>0){
                def documentiAdesione=new Documenti()
                documentiAdesione.tipo=TipoDocumento.MODULO_ADESIONE
                documentiAdesione.fileContent=params.fileContentModAdesione.bytes?:null
                documentiAdesione.fileName=params.fileContentModAdesione?.originalFilename?:null
                polizza.adesione=true
                polizza.addToDocumenti(documentiAdesione)
            }
            if(polizza.save(flush:true)){
                logg =new Log(parametri: "Polizza salvata", operazione: "aggiornamento polizza", pagina: "INSERISCE POLIZZA")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if(polizza.stato=="POLIZZA_IN_ATTESA_ATTIVAZIONE"){
                    logg =new Log(parametri: "POLIZZA_IN_ATTESA_ATTIVAZIONE", operazione: "aggiornamento polizza", pagina: "INSERISCE POLIZZA")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    if(polizza.tracciato!=null){
                        polizza.tracciato.delete()
                        def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                        if(!tracciatoPolizza.save(flush: true)){
                            logg =new Log(parametri: "errore generazione tracciato ${renderErrors(bean: tracciatoPolizza)}", operazione: "aggiornamento tracciato aggiornamento polizza", pagina: "INSERISCE POLIZZA")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            flash.error = renderErrors(bean: tracciatoPolizza)
                        } else{
                            logg =new Log(parametri: " generazione tracciato", operazione: "aggiornamento tracciato aggiornamento polizza", pagina: "INSERISCE POLIZZA")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            flash.message="POLIZZA"
                        }
                    }else{
                        def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                        if(!tracciatoPolizza.save(flush: true)){
                            logg =new Log(parametri: "errore generazione tracciato ${renderErrors(bean: tracciatoPolizza)}", operazione: " tracciato aggiornamento polizza", pagina: "INSERISCE POLIZZA")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            flash.error = renderErrors(bean: tracciatoPolizza)
                        } else{
                            logg =new Log(parametri: " generazione tracciato", operazione: "aggiornamento tracciato aggiornamento polizza", pagina: "INSERISCE POLIZZA")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            flash.message="POLIZZA"
                        }
                    }
                }
            }else {
                logg =new Log(parametri: "errore aggironamento polizza ${renderErrors(bean: polizza)}", operazione: " aggiornamento polizza", pagina: "INSERISCE POLIZZA")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "errori-->${renderErrors(bean: polizza)}"
                flash.error = renderErrors(bean: polizza)
            }

            redirect action: "lista", params: params.search ? [search: params.search] : [:]
        }else response.sendError(404)
    }
    def annullaPolizza(String idPolizza){
        def logg
        def polizza=Polizza.get(idPolizza)
        def risposta
        if(polizza){
            polizza.stato="ANNULLATA"
            if(polizza.save(flush:true)){
                logg =new Log(parametri: "polizza annullata", operazione: " annulla polizza", pagina: "POLIZZE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                risposta=[risposta:true]
                render risposta as JSON
            }
            else {
                logg =new Log(parametri: "Errore nel aggiornamento dello stato del preventivo ${renderErrors(bean: polizza)}", operazione: " annulla polizza", pagina: "POLIZZE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                risposta =[risposta: "Errore nel aggiornamento dello stato del preventivo ${renderErrors(bean: polizza)}"]
                println "Errore caricamento polizza: ${polizza.errors}"
                render risposta as JSON
            }
        }
    }
    def certificatoPolizza(String id){
        def polizza=Polizza.get(id)
        def nome = polizza.nomeInt
        /*def arr =[]
        arr= nome.split("\\W+")
        def nomearr=""
        def cognomearr=""
        if(arr.length==2){
            nomearr=arr[0]
            cognomearr=arr[1]
        }else if (arr.length>2){
            nomearr=arr[0]
            for(int i=1; i<arr.length;i++) {
                cognomearr += arr[i] + " "
            }
        }*/
        def nomeContr=(polizza.tipoContraente.toString().toUpperCase()=="INTESTATARIO")?polizza.nomeInt:polizza.nomeGarante
        def cognomeContr=(polizza.tipoContraente.toString().toUpperCase()=="INTESTATARIO")?polizza.cognomeInt:polizza.cognomeGarante
        def contraente=cognomeContr+" "+nomeContr
        def indContr=(polizza.tipoContraente.toString().toUpperCase()=="INTESTATARIO")?polizza.indirizzoInt:polizza.indirizzoGarante
        def capContr=(polizza.tipoContraente.toString().toUpperCase()=="INTESTATARIO")?polizza.capInt:polizza.capGarante
        def locContr=(polizza.tipoContraente.toString().toUpperCase()=="INTESTATARIO")?polizza.localitaInt:polizza.localitaGarante
        def localita=locContr+" "+capContr

        def targa_telaio= polizza.targa+" / "+polizza.telaio

        def src = this.class.getResource("/pdf/provvisorio.pdf").file
        src = src.replaceAll("%23", "#")
        if (src) {
            def stream = new FileInputStream(src)
            PdfReader reader = new PdfReader(stream)
            def (canvas, output, writer) = createCanvasAndStreams(reader, 1)
            canvas.saveState()
            writeText(canvas, 86, 746, (polizza.dataDecorrenza) ? polizza.dataDecorrenza.format("dd") :"", 10, "nero","normal")
            writeText(canvas, 98, 746, (polizza.dataDecorrenza) ? polizza.dataDecorrenza.format("MMMMM") :"", 10, "nero","normal")
            writeText(canvas, 128, 746, (polizza.dataDecorrenza) ? polizza.dataDecorrenza.format("yyyy") :"", 10, "nero","normal")
            writeText(canvas, 332, 746, contraente, 10, "nero","normal")
            writeText(canvas, 332, 734, indContr, 10, "nero","normal")
            writeText(canvas, 332, 722, localita, 10, "nero","normal")
            writeText(canvas, 44, 505,  targa_telaio, 10, "nero","normal")
            writeText(canvas, 44, 477, polizza.noPolizza, 10, "nero","normal")
            writeText(canvas, 44, 452, polizza.dataDecorrenza.format("dd/MM/yyyy"), 10, "nero","normal")
            writeText(canvas, 44, 425, "AUTOVETTURA", 10, "nero","normal")


            canvas = writer.getOverContent(2)
            writer.close()
            reader.close()
            response.contentType = "application/pdf"
            response.addHeader "Content-disposition", "inline; filename=certificatoPolizza${polizza.noPolizza}.pdf"
            response.outputStream << output.toByteArray()
        }
    }
    def generaFilePolizzeattivate(){

        def polizze=Polizza.createCriteria().list(){
           // eq "stato", StatoPolizza.POLIZZA
        }

        def wb = Stopwatch.log { ExcelBuilder.create {
            style("header") {
                background bisque
                font {
                    bold(true)
                }
            }
            sheet("Polizze attivate") {
                row(style: "header") {
                    cell("Dealer")
                    cell("Telefono Dealer")
                    cell("Mail Dealer")
                    cell("Numero Pratica")
                    cell("Nome Contraente")
                    cell("Cognome Contraente")
                    cell("PartitaIva Contraente")
                    cell("Marca")
                    cell("Modello")
                    cell("Targa")
                    cell("Telaio")
                    cell("Valore Assicurato")
                    cell("Classe appartenenza contraente")
                    cell("Data Immatricolazione")
                    cell("Data Decorrenza")
                    cell("Data Scadenza")
                    cell("Stato pratica")

                }
                if(polizze){
                    polizze.each { polizza ->
                        row {
                            cell(polizza.dealer.ragioneSociale)
                            cell(polizza.dealer.telefono)
                            cell(polizza.dealer.email?polizza.dealer.email.toString().trim():"")
                            cell(polizza.noPratica)
                            cell((polizza.tipoContraente.toString()=="garante")? polizza.nomeGarante : polizza.nomeInt)
                            cell((polizza.tipoContraente.toString()=="garante")? polizza.cognomeGarante : polizza.cognomeInt)
                            cell((polizza.tipoContraente.toString()=="garante")? polizza.partitaIvaGarante : polizza.partitaIvaInt)
                            cell(polizza.marca)
                            cell(polizza.modello)
                            cell(polizza.targa!=null?polizza.targa.toString().toUpperCase():"")
                            cell(polizza.telaio!=null?polizza.telaio.toString().toUpperCase():"")
                            cell(polizza.valoreAssicurato)
                            cell(polizza.tipoAcquisizione.toString()=="nuova polizza"? polizza.tipoAcquisizione.toString().toUpperCase():"VECCHIA POLIZZA")
                            cell(polizza.dataImmatricolazione)
                            cell(polizza.dataDecorrenza)
                            cell(polizza.dataScadenza)
                            cell(polizza.stato.toString().toUpperCase())
                        }
                        polizza.discard()
                    }
                    for (int i = 0; i < 17; i++) {
                        sheet.autoSizeColumn(i);
                    }
                }else{
                    row{
                        cell("non ci sono polizze attivate")
                    }
                }

            }

        } }

        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        response.addHeader "Content-disposition", "inline; filename=polizze_${new Date().format("ddMMyyyy")}.xlsx"
        response.outputStream << stream.toByteArray()
    }
    def writeText(canvas, x, y, String text, fontSize, color, font){
        def fontsB = [
                "normal": BaseFont.HELVETICA,
                "bold": BaseFont.HELVETICA_BOLD
        ]
        def carattere = BaseFont.createFont(fontsB[font], BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        canvas.beginText()
        canvas.moveText(x as float,y as float)
        canvas.setFontAndSize(carattere, fontSize as float)
        if(color == "rosso") canvas.setRGBColorFill(192, 0, 0)
        else if (color == "bianco") canvas.setRGBColorFill(255, 255, 255)
        else if (color == "grigio") canvas.setRGBColorFill(147, 147, 147)
        else canvas.setRGBColorFill(0, 0, 0)
        canvas.showText(text)
        canvas.endText()
    }
    def createCanvasAndStreams(document, page) {
        def output = new ByteArrayOutputStream()
        def writer = new PdfStamper(document, output)
        PdfContentByte canvas = writer.getOverContent(page)
        return [canvas, output, writer]
    }
    def scaricaDocumento(long id) {
        def polizza = Polizza.read(id)
        def documento=Documenti.createCriteria().list(max:1) {
            eq "polizza", polizza
            ne "tipo", TipoDocumento.MODULO_ADESIONE
            ne "tipo", TipoDocumento.CERTIFICATO
            order('dateCreated', 'desc')

        }
        if(documento) {
            documento.each { doc ->
                response.with {
                    contentType = "application/octet-stream"
                    setHeader("Content-disposition", "attachment; filename=${doc.fileName}")
                    outputStream << doc.fileContent
                }
            }

        } else response.sendError(404)
    }
    def scaricaModulo(long id) {
        def polizza = Polizza.read(id)
        def documento=Documenti.createCriteria().list(max:1) {
            eq "polizza", polizza
            eq "tipo", TipoDocumento.MODULO_ADESIONE
            order('dateCreated', 'desc')
        }
        if(documento) {
            documento.each { doc ->
                response.with {
                    contentType = "application/octet-stream"
                    setHeader("Content-disposition", "attachment; filename=${doc.fileName}")
                    outputStream << doc.fileContent
                }
            }

        } else response.sendError(404)
    }
    def scaricaFascicoloInformativo() {
        def src = this.class.getResource("/pdf/fascicolo_informativo.pdf").file
        src = src.replaceAll("%23", "#")
        if (src) {
            def stream = new FileInputStream(src)
            response.contentType = "application/octet-stream"
            response.addHeader "Content-disposition", "inline; filename=Fascicolo informativo.pdf"
            response.outputStream << stream.bytes
        }
    }
    def scaricaCertificato(long id){
        def polizza = Polizza.read(id)
        if(polizza) {
            def documento=Documenti.createCriteria().get() {
                eq "polizza", polizza
                eq "tipo", TipoDocumento.CERTIFICATO
            }
            if(documento) {
                response.with {
                    contentType = "application/octet-stream"
                    setHeader("Content-disposition", "attachment; filename=${documento.fileName}")
                    outputStream << documento.fileContent
                }
            } else response.sendError(404)
        }else response.sendError(404)

    }
    def caricaDocumenti(long  idPolizzaDoc){
        def polizza=Polizza.get(idPolizzaDoc)
        if(polizza) {
            if (params.fileContent.size > 0) {
                if (params.tipo == "PASSAGGIO") {
                    def documenti = new Documenti(tipo: TipoDocumento.PASSAGGIO, fileContent:params.fileContent.bytes ?: null,fileName: params.fileContent?.originalFilename ?: null, polizza: polizza )
                   if(documenti.save(flush:true)){
                       flash.message="documento acquisizione caricato"
                       polizza.tipoAcquisizione=Acquisizione.PASSAGGIO
                   }else{
                       println "errori-->${renderErrors(bean: documenti)}"
                       flash.error = renderErrors(bean: documenti)
                   }

                } else if (params.tipo == "BERSANI") {

                    def documenti = new Documenti(tipo: TipoDocumento.BERSANI, fileContent:params.fileContent.bytes ?: null,fileName: params.fileContent?.originalFilename ?: null, polizza: polizza )
                    if(documenti.save(flush:true)){
                        polizza.tipoAcquisizione=Acquisizione.BERSANI
                        flash.message="documento acquisizione caricato"
                    }else{
                        println "errori-->${renderErrors(bean: documenti)}"
                        flash.error = renderErrors(bean: documenti)
                    }

                } else if (params.tipo == "ADESIONE") {
                    def documenti = new Documenti(tipo: TipoDocumento.MODULO_ADESIONE, fileContent:params.fileContent.bytes ?: null,fileName: params.fileContent?.originalFilename ?: null, polizza: polizza )
                    if(documenti.save(flush:true)){
                        polizza.adesione = true
                        flash.message="modulo adesione caricato"
                    }else{
                        println "errori-->${renderErrors(bean: documenti)}"
                        flash.error = renderErrors(bean: documenti)
                    }


                }
            }
            if(!polizza.save(flush:true)){
                flash.error = renderErrors(bean: polizza)
            }
            redirect action: "lista", params: params.search ? [search: params.search] : [:]
        }
        else response.sendError(404)
    }
    def caricaDealer() {
        if(request.post) {
            def dealersPresenti=[], dealersPiva=[]
            def dealersNuovi=[]
            def file =params.excelDealer
            if(file.bytes) {
                def myInputStream = new ByteArrayInputStream(file.bytes)
                ExcelReader.readXlsx(myInputStream) {
                    sheet(0) {
                        rows(from: 1) {
                            if(cell("A") != null && cell("C").value != null){
                                def codiceDealer = cell("A").value
                                def cfPiva = cell("C").value
                                if((codiceDealer !=null && cfPiva !=null )){
                                    def dealer = new Dealer(attivo: false)
                                    if(codiceDealer.toString().contains(".")){
                                        codiceDealer=Double.valueOf(codiceDealer).longValue().toString()
                                    }
                                    if (codiceDealer.toString().matches("[0].*")) {
                                        codiceDealer=codiceDealer.toString().replaceFirst("^0+(?!\\\$)", "")
                                    }
                                    /*if(codiceDealer.startsWith("0")) codiceDealer = codiceDealer[1..-1]     //tolgo il primo zero, se presente
                                    if(codiceDealer.endsWith(".0")) codiceDealer = codiceDealer.substring(0,codiceDealer.indexOf("."))     //tolgo il primo zero, se presente*/
                                    def dealerPiva, dealerUser
                                    cfPiva = cell("C").valueAsString
                                    if (!cfPiva.toString().trim().matches("^[a-z|A-Z]{6}[0-9]{2}[A-Z|a-z][0-9]{2}[A-Z|a-z][0-9]{3}[A-Z|a-z]\$") && !cfPiva.toString().trim().matches("^0+\\d*(?:\\.\\d+)?\$")) {
                                        cfPiva = Double.valueOf(cfPiva).longValue().toString()
                                    }
                                    if (cfPiva.toString().size() < 11) {
                                        cfPiva = cfPiva.toString().trim().padLeft(11, "0")
                                    }
                                    dealerPiva = Dealer.findByUsernameAndPiva(codiceDealer, cfPiva)
                                    if(dealerPiva) {
                                        dealersPiva.add("codice: ${cell("A").value}  partita iva: ${cfPiva}")
                                    }else {
                                        dealerUser = Dealer.findByUsername(codiceDealer)
                                        if(dealerUser) {
                                            dealersPresenti.add("codice: ${cell("A").value}  partita iva: ${cfPiva}")
                                        }
                                        else{
                                            def ragioneSociale = cell("B").valueAsString
                                            def indirizzo = cell("D").valueAsString
                                            def localita = cell("E").valueAsString
                                            def provincia = cell("F").valueAsString
                                            def cap = cell("G").value
                                            if(cap.toString().contains(".")){
                                                cap=Double.valueOf(cap).longValue().toString()
                                                if (cap.toString().trim().length()<5){cap=cap.toString().trim().padLeft(5,"0")}
                                            }
                                            def telefono = cell("H") ? cell("H").valueAsString : ""
                                            def fax = cell("I") ? cell("I").valueAsString : ""
                                            def email = cell("J") ? cell("J").valueAsString : ""
                                            dealer.username=codiceDealer
                                            dealer.piva=cfPiva
                                            dealer.password=cfPiva
                                            dealer.ragioneSociale=ragioneSociale
                                            dealer.indirizzo=indirizzo
                                            dealer.localita=localita
                                            dealer.provincia=provincia
                                            dealer.cap=cap
                                            dealer.telefono=telefono
                                            dealer.fax=fax
                                            dealer.email=email
                                            if (!dealer.save(flush: true)) {
                                                flash.error = "<p><strong>Si sono verificati errori nel caricamento.</strong></p><p>${renderErrors(bean: dealer)}</p>"
                                                log.error renderErrors(bean: dealer)
                                            } else {
                                                println  "DEALER INSERITO ${dealer.username}"
                                                dealersNuovi.add("codice: ${cell("A").value} partita iva: ${dealer.piva}")
                                            }
                                        }
                                    }
                                }else{
                                    flash.error = "verificare il file excel perche'presenta delle righe vuote"
                                }
                            }
                        }
                    }
                }
                if(dealersPiva.size()>0){
                    flash.errorDealer=[]
                    flash.errorDealerPiva=dealersPiva
                }
                if(dealersPresenti.size()>0){
                    flash.errorDealer=[]
                    flash.errorDealer=dealersPresenti
                }
                if(dealersNuovi.size()>0){
                    flash.messageDealer=[]
                    flash.messageDealer=dealersNuovi
                }
            } else flash.error = "Specificare l'elenco excel"
            redirect action: "lista"
        } else response.sendError(404)
    }
    def caricaPratica() {
        def logg
        if(request.post) {
            def pratichePresente=[], praticheNuove=[], errorePratica=[]
            def dealerMancante=[]
            def file =params.excelPratica
            def filename=""
            if(file.bytes) {
                InputStream myInputStream = new ByteArrayInputStream(file.bytes)
                def response =[]
                Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$)/
                Pattern pattern = ~/(\w+)/
                String regex = "<(\\S+)[^>]+?mso-[^>]*>.*?</\\1>";
                def x_value=[]
                def dealer
                def dealers=[]
                BufferedReader reader = new BufferedReader(new InputStreamReader(myInputStream, "UTF-8"));
                String line;
                try {
                    while ((line = reader.readLine()) != null) {
                        //if(line.trim().isEmpty() ||  line.equals("<TITLE>") || line.equals("REPORT") || line.equals("</TITLE>") || line.equals("<HTML>") || line.equals("<HEAD>") || line.equals("</HEAD>") || line.contains("COL1") || line.equals("<BODY>")|| line.equals("<PRE>")|| line.equals("</PRE>")|| line.equals("</BODY>")|| line.equals("</HTML>") || line.contains("----------------------")){
                        if(line.trim().isEmpty() ||  !line.contains(";") ){
                            continue;
                        }
                        x_value.add(line.split(";"))
                    }
                    for(int i=0; i<x_value.size();i++){
                        //println x_value[i]
                        def valore=x_value[i]
                        def cellaDealer = valore[0].toString().trim()
                        def cellnoPratica = valore[4].toString().trim()
                        def celladataCaricamento = valore[5].toString().trim()
                        def celladataErogazione = valore[6].toString().trim()
                        def cellaImportoRichiesto = valore[7].toString().trim()
                        def cellaImportoFinanziato = valore[8].toString().trim()
                        def cellaImportoNetto = valore[9].toString().trim()
                        def cellaNoRate = valore[10].toString().trim()
                        def cellaCodProd = valore[11].toString().trim()
                        def cellaCognomeInt  =  valore[12].toString().trim()
                        def cellaNomeInt  =  valore[13].toString().trim()
                        def cellaPIvaInt =  valore[14].toString().trim()
                        def cellaSessoInt =  valore[15].toString().trim()
                        def cellaIndirizzoInt =  valore[16].toString().trim()
                        def cellaLocalitaInt =  valore[17].toString().trim()
                        def cellaProvinciaInt =  valore[18].toString().trim()
                        def cellaCapInt =  valore[19].toString().trim()
                        def cellaTelInt =  valore[20].toString().trim()
                        def cellaEmailInt = valore[21].toString().trim()
                        def cellaCognomeGarante=  valore[22].toString().trim()
                        def cellaNomeGarante=  valore[23].toString().trim()
                        def cellaCodFiscGarante=  valore[24].toString().trim()
                        def cellaSessoGarante=  valore[25].toString().trim()
                        def cellaIndirizzoGarante=  valore[26].toString().trim()
                        def cellaLocalitaGarante=  valore[27].toString().trim()
                        def cellaProvGarante=  valore[28].toString().trim()
                        def cellaCapGarante=  valore[29].toString().trim()
                        def cellaTelGarante=  valore[30].toString().trim()
                        def cellaEmailGarante=  valore[31].toString().trim()
                        def cellaMarcaAuto=  valore[32].toString().trim()
                        def cellaModelloAuto=  valore[33].toString().trim()
                        def cellaCategoriabene=  valore[34].toString().trim()
                        def cellaBeneFinanzia=  valore[35].toString().trim()
                        def cellaAnnoImm=  valore[36].toString().trim()
                        def cellaNuovo=  valore[38].toString().trim()
                        def cellaTipo=  valore[39].toString().trim()
                        if(cellaNomeInt.toString().equals("null")){
                            cellaNomeInt="";
                        }else{
                            cellaNomeInt=cellaNomeInt.toString().trim()
                        }
                        if(cellaCognomeInt.toString().equals("null")){
                            cellaCognomeInt="";
                        }else{
                            cellaCognomeInt=cellaCognomeInt.toString().trim()
                        }

                        if(cellnoPratica.toString().equals("null")){
                            cellnoPratica="";
                        }else{
                            cellnoPratica=cellnoPratica.toString().replaceFirst ("^0*", "")
                            if(cellnoPratica.toString().contains(".")){
                                def punto=cellnoPratica.toString().indexOf(".")
                                cellnoPratica=cellnoPratica.toString().substring(0,punto).replaceAll("[^0-9]", "")
                            }
                        }
                        if(cellaCapInt.toString().equals("null")){
                            cellaCapInt="";
                        }else{
                            if(cellaCapInt.toString().contains(".")){
                                def punto=cellaCapInt.toString().indexOf(".")
                                cellaCapInt=cellaCapInt.toString().substring(0,punto).replaceAll("[^0-9]", "")
                            }
                        }
                        if(cellaDealer.toString().equals("null")){
                            cellaDealer="";
                        }else{
                            cellaDealer=cellaDealer.toString().replaceFirst ("^0*", "")
                            if(cellaDealer.toString().contains(".")){
                                cellaDealer=Double.valueOf(cellaDealer).longValue().toString()
                                //cellaDealer=cellaDealer.toString().substring(0,punto).replaceAll("[^0-9]", "")
                            }
                            //println cellaDealer
                            dealer=Dealer.findByUsername(cellaDealer.toString())
                        }
                        cellaPIvaInt=cellaPIvaInt.toString().replaceAll("[^a-z,A-Z,0-9]","")
                        def sessoInt
                        if(cellaSessoInt.toString().trim().equals("null")){
                            sessoInt="";
                        }else if (cellaSessoInt.toString().trim().equalsIgnoreCase("M")) sessoInt=TipoCliente.M
                        else if (cellaSessoInt.toString().trim().equalsIgnoreCase("F")) sessoInt=TipoCliente.F
                        else sessoInt=TipoCliente.DITTA_INDIVIDUALE
                        def sessoGrnt
                        if(cellaSessoGarante.toString().trim().equals("null")){
                            sessoGrnt="";
                        }else if (cellaSessoGarante.toString().trim().equalsIgnoreCase("M")) sessoGrnt=TipoCliente.M
                        else if (cellaSessoGarante.toString().trim().equalsIgnoreCase("F")) sessoGrnt=TipoCliente.F
                        else sessoGrnt=TipoCliente.DITTA_INDIVIDUALE

                        if(cellaNomeGarante.toString().equals("null")){cellaNomeGarante="";}
                        else{cellaNomeGarante=cellaNomeGarante.toString().trim()}

                        if(cellaCognomeGarante.toString().equals("null")){cellaCognomeGarante="";}
                        else{cellaCognomeGarante=cellaCognomeGarante.toString().trim()}

                        if(cellaCapGarante.toString().equals("null")){
                            cellaCapGarante="";
                        }else{
                            if(cellaCapGarante.toString().contains(".")){
                                def punto=cellaCapGarante.toString().indexOf(".")
                                cellaCapGarante=cellaCapGarante.toString().substring(0,punto).replaceAll("[^0-9]", "")
                            }
                        }
                        cellaCodFiscGarante=cellaCodFiscGarante.toString().replaceAll("[^a-z,A-Z,0-9]","")

                        if(cellaMarcaAuto.toString().equals("null")){cellaMarcaAuto="";}
                        else{cellaMarcaAuto=cellaMarcaAuto.toString().trim()}

                        if(cellaModelloAuto.toString().equals("null")){cellaModelloAuto="";}
                        else{cellaModelloAuto=cellaModelloAuto.toString().trim()}

                        if(celladataCaricamento.toString().equals("null")){ celladataCaricamento=""; }
                        else{  celladataCaricamento=celladataCaricamento.toString().trim() }

                        if(celladataErogazione.toString().equals("null")){  celladataErogazione="";  }
                        else{  celladataErogazione=celladataErogazione.toString().trim() }

                        if(cellaImportoRichiesto.toString().equals("null")){  cellaImportoRichiesto=""; }
                        else{  cellaImportoRichiesto=cellaImportoRichiesto.toString().trim()  }

                        if(cellaImportoFinanziato.toString().equals("null")){ cellaImportoFinanziato=""; }
                        else{  cellaImportoFinanziato=cellaImportoFinanziato.toString().trim() }

                        if(cellaImportoNetto.toString().equals("null")){ cellaImportoNetto="";  }
                        else{ cellaImportoNetto=cellaImportoNetto.toString().trim() }

                        if(cellaNoRate.toString().equals("null")){cellaNoRate="";}
                        else{cellaNoRate=cellaNoRate.toString().trim() }

                        if(cellaCodProd.toString().equals("null")){cellaCodProd="";}
                        else{cellaCodProd=cellaCodProd.toString().trim()}

                        if(cellaCategoriabene.toString().equals("null")){cellaCategoriabene="";}
                        else{cellaCategoriabene=cellaCategoriabene.toString().trim()}

                        if(cellaBeneFinanzia.toString().equals("null")){cellaBeneFinanzia="";}
                        else{cellaBeneFinanzia=cellaBeneFinanzia.toString().trim()}

                        if(cellaAnnoImm.toString().equals("null")){cellaAnnoImm="";}
                        else{cellaAnnoImm=cellaAnnoImm.toString().trim()}

                        if(cellaNuovo.toString().equals("null")){cellaNuovo="";}
                        else{cellaNuovo=cellaNuovo.toString().trim()}

                        if(cellaTipo.toString().equals("null")){cellaTipo="";}
                        else{cellaTipo=cellaTipo.toString().trim()}
                        if(dealer && cellnoPratica){
                            def polizzaesistente= Polizza.findByNoPraticaIlike(cellnoPratica)
                            if (polizzaesistente){
                                logg =new Log(parametri: "la pratica-->${cellnoPratica} \u00E8 gia' stata caricata nel portale", operazione: " caricamento pratiche", pagina: "POLIZZE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                errorePratica.add("la pratica-->${cellnoPratica} \u00E8 gia' stata caricata nel portale")
                            }else{
                                def polizza = Polizza.findOrCreateWhere(
                                        noPratica: cellnoPratica,
                                        dealer: dealer,
                                        nomeInt: cellaNomeInt.toString().trim(),
                                        cognomeInt:  cellaCognomeInt.toString().trim(),
                                        partitaIvaInt: cellaPIvaInt.toString().trim(),
                                        tipoClienteInt: sessoInt,
                                        indirizzoInt: cellaIndirizzoInt.toString().trim(),
                                        localitaInt: cellaLocalitaInt.toString().trim(),
                                        capInt: cellaCapInt.toString().trim(),
                                        provinciaInt: cellaProvinciaInt.toString().trim(),
                                        nomeGarante:  cellaNomeGarante.toString().trim(),
                                        cognomeGarante:  cellaCognomeGarante.toString().trim(),
                                        partitaIvaGarante: cellaCodFiscGarante.toString().trim(),
                                        tipoClienteGarante: sessoGrnt,
                                        indirizzoGarante: cellaIndirizzoGarante.toString().trim(),
                                        localitaGarante: cellaLocalitaGarante.toString().trim(),
                                        capGarante: cellaCapGarante.toString().trim(),
                                        provinciaGarante: cellaProvGarante.toString().trim(),
                                        marca: cellaMarcaAuto,
                                        modello: cellaModelloAuto,
                                        autoveicolo:!(cellaTipo=='A')?false:true,
                                        stato: "PREVENTIVO"
                                )
                                if(!polizza.save(flush:true)) {
                                    println polizza.errors
                                    errorePratica.add(" pratica-->${cellnoPratica} Errore creazione pratica: ${polizza.errors}")
                                    logg =new Log(parametri: "pratica-->${cellnoPratica} Errore creazione pratica: ${polizza.errors}", operazione: " caricamento pratiche", pagina: "POLIZZE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                }
                                else{
                                    def tracciatoCompagnia= TracciatoCompagnia.findOrCreateWhere(
                                            dataCaricamento: celladataCaricamento.toString().trim(),
                                            dataErogazione: celladataErogazione.toString().trim(),
                                            importoRichiesto: cellaImportoRichiesto.toString().trim(),
                                            importoFinanziato: cellaImportoFinanziato.toString().trim(),
                                            importoNettoErogato: cellaImportoNetto.toString().trim(),
                                            codProdotto: cellaCodProd.toString().trim(),
                                            categoriaBene: cellaCategoriabene.toString().trim(),
                                            beneFinanziato: cellaBeneFinanzia.toString().trim(),
                                            polizza: polizza
                                    )
                                    if(!tracciatoCompagnia.save(flush:true)){
                                        println "${tracciatoCompagnia.errors}"
                                        errorePratica.add("pratica-->${cellnoPratica} non \u00E8  stato possibile inserire i dati per il tracciato della compagnia")
                                        logg =new Log(parametri: "pratica-->${cellnoPratica} non \u00E8  stato possibile inserire i dati per il tracciato della compagnia ${tracciatoCompagnia.errors}", operazione: " caricamento pratiche", pagina: "POLIZZE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }else{
                                        praticheNuove.add("pratica : ${polizza.noPratica}")
                                        logg =new Log(parametri: "pratica caricata -->${polizza.noPratica}", operazione: " caricamento pratiche", pagina: "POLIZZE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }
                                }
                            }

                        }else if(!dealer){
                            println "il dealer non \u00E8  presente"
                            errorePratica.add("Per la pratica  numero: ${cellnoPratica} il dealer non \u00E8 presente")
                            logg =new Log(parametri: "Per la pratica  numero: ${cellnoPratica} il dealer non \u00E8 presente", operazione: " caricamento pratiche", pagina: "POLIZZE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }else if(!cellnoPratica){
                            logg =new Log(parametri: "Errore creazione polizza: non è presente un numero di pratica", operazione: " caricamento pratiche", pagina: "POLIZZE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            println "Errore creazione polizza: non \u00E8 presente un numero di pratica"
                            errorePratica.add("non \u00E8 presente un numero di pratica")
                        }
                    }
                    reader.close();

                } catch (e){
                    logg =new Log(parametri: "Errore ${e.toString()}", operazione: " caricamento pratiche", pagina: "POLIZZE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    println "verificare il formato del file"
                    errorePratica.add("verificare il formato del file")
                }
                if(errorePratica.size()>0){
                    flash.errorePratica=[]
                    flash.errorePratica=errorePratica
                }
                if(dealerMancante.size()>0){
                    flash.dealerMancante=[]
                    flash.dealerMancante=dealerMancante
                }
                if(praticheNuove.size()>0){
                    flash.praticheNuove=[]
                    flash.praticheNuove=praticheNuove
                }
            } else flash.error = "Specificare l'elenco txt"
            redirect action: "lista"
        } else response.sendError(404)
    }

}
