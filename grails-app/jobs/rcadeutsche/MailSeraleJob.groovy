package rcadeutsche

import com.jcraft.jsch.ChannelSftp
import com.jcraft.jsch.JSch
import grails.transaction.Transactional
import grails.util.Environment
import rcadeutsche.polizze.Polizza
import rcadeutsche.polizze.StatoPolizza
import rcadeutsche.utenti.Log

@Transactional
class MailSeraleJob {
    def mailService
    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
            cron name: "MailSerale", cronExpression: "0 30 22 * * ?"
        }else{
          //  cron name: "MailSerale", cronExpression: "0 30 16 * * ?"
        }

    }
    def group = "Invio mail"
    def description = "job per mandare mail tutti i giorni"
    def execute() {
        try {
            invioMail()

        } catch (Exception e) {
            log.error e.message
        }
    }
    def invioMail() {
        def logg
        try {
            def polizze=Polizza.createCriteria().list(){
                ne "stato", "PREVENTIVO"
                ne "stato", "POLIZZA"
            }
            if(polizze){
                polizze.each { polizza ->
                    def statusInvio=mailService.invioMail(polizza)
                    println statusInvio
                    logg =new Log(parametri: "status invio mail serale, ${statusInvio}", operazione: " mail serale pratiche", pagina: "MAIL SERALE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    /*if(statusInvio){
                        println "la mail e' stata inviata"
                    }else{
                        println "la mail non e' stata inviata "
                    }*/
                    polizza.discard()
                }
            }
        } catch(e) { e.printStackTrace() }
    }
}
