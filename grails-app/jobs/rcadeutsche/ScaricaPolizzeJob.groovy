package rcadeutsche

import com.jcraft.jsch.ChannelSftp
import com.jcraft.jsch.JSch
import com.presstoday.groovy.Stopwatch
import excel.builder.ExcelBuilder
import grails.transaction.Transactional
import grails.util.Environment
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.Font
import rcadeutsche.polizze.Polizza
import rcadeutsche.utenti.Log

import java.util.regex.Pattern

@Transactional
class ScaricaPolizzeJob {
    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
            cron name: "ScaricaPolizze", cronExpression: "0 00 22 * * ?"
        }else{
           // cron name: "ScaricaPolizze", cronExpression: "0 15 16 * * ?"
        }

    }
    def group = "DownloadPolizze"
    def description = "job per scaricare le polizze"
    def execute() {
        try {
            downloadFile()

        } catch (Exception e) {
            log.error e.message
        }
    }
    def downloadFile() {
        def logg
        def host,port,username,password

        host = "5.249.141.51"
        port = 22
        username = "flussi-nais"
        password = "XYZrrr655"


        try {
            def creazione
            creazione=createPolizzeXLS()
            def jsch = new JSch()
            def session = jsch.getSession(username, host, port)
            def properties = new Properties()
            properties.put("StrictHostKeyChecking", "no")
            session.config = properties
            session.userName = username
            session.password = password
            session.connect()
            def channel = session.openChannel("sftp") as ChannelSftp
            channel.connect()
            println "Connected to $host:$port"
            logg =new Log(parametri: "Connected to $host:$port", operazione: " scarica polizze serale", pagina: "SCARICA POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            //channel.cd("flussi-bmw")
            println "Current sftp directory: ${channel.pwd()}"
            logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: " scarica polizze serale", pagina: "SCARICA POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            channel.cd("..")
            if(Environment.current == Environment.PRODUCTION) {
                channel.cd("/home/flussi-brokeronline/to_Bol")
            }else{
                channel.cd("/home/flussi-brokeronline/to_Bol/TEST")
            }

            println "Current sftp directory: ${channel.pwd()}"
            logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: " scarica polizze serale", pagina: "SCARICA POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            if(creazione.size()>0 && !creazione.toString().contains("problema con la generazione del file")){
                println "è stato creato un file:"
                logg =new Log(parametri: "\u00E8 stato creato un file:", operazione: " scarica polizze serale", pagina: "SCARICA POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                channel.put(new ByteArrayInputStream(creazione.toByteArray()), "bol_${new Date().format("yyyyMMdd")}.xls")
                logg =new Log(parametri: "creazione estrazioni bol_${new Date().format("yyyyMMdd")}", operazione: " scarica polizze serale", pagina: "SCARICA POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "creazione estrazioni bol_${new Date().format("yyyyMMdd")}"
                println "Current sftp directory: ${channel.pwd()}"
                logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: " scarica polizze serale", pagina: "SCARICA POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
               /* mailService.sendMail {
                    to "priscila@presstoday.com"
                    subject "riassunto trasferimento polizze portale DEUTSCHE"
                    msg "<p style=\"font-family:sans-serif; font-size:14px;\">In allegato troverete l'elenco dei file delle polizze caricate il ${new Date().format("dd-MMM-yyyy")}.<p>\n"
                    attach streamRiassunti.toByteArray(), "riassunto_${new Date().format("dd-MM-yyyy")}.zip", "application/x-compressed-zip"
                }*/
            channel.disconnect()
            session.disconnect()


        } catch(e) { e.printStackTrace() }

    }
    def createPolizzeXLSX(){
        def response =[]
        Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{5}$)/
        Pattern pattern = ~/(\w+)/
        def polizze=Polizza.createCriteria().list(){
            // eq "stato", StatoPolizza.POLIZZA
        }
        def stream = new ByteArrayOutputStream()
        try {
            def wb = Stopwatch.log { ExcelBuilder.create {
                style("header") {
                    background bisque
                    font {
                        bold(true)
                    }
                }
                sheet("Polizze") {
                    row(style: "header") {
                        cell("Codice Dealer")
                        cell("Ragione Sociale Dealer")
                        cell("Partita Iva/Codice Fiscale Dealer")
                        cell("Codice campagna")
                        cell("Numero Pratica")
                        cell("Data caricamento/proposta (verificare)")
                        cell("Data erogazione")
                        cell("Importo richiesto")
                        cell("Importo finanziato")
                        cell("Importo Netto Erogato")
                        cell("Cod. Prodotto")
                        cell("Cognome Intestatario pt")
                        cell("Nome Intestatario pt")
                        cell("Codice Fiscale Intestatario")
                        cell("Sesso Intestatario")
                        cell("Indirizzo Intestatario pt")
                        cell("Località Intestatario pt")
                        cell("PV Intestatario pt")
                        cell("CAP Intestatario pt")
                        cell("Telefono Intestatario")
                        cell("E-mail Intestatario")
                        cell("Cognome Garante pt")
                        cell("Nome Garante pt")
                        cell("Codice Fiscale Garante")
                        cell("Sesso Garante")
                        cell("Indirizzo Garante pt")
                        cell("Località Garante pt")
                        cell("PV Garante pt")
                        cell("CAP Garante pt")
                        cell("Telefono Garante")
                        cell("E-mail Garante")
                        cell("Marca auto")
                        cell("Modello")
                        cell("Categoria bene")
                        cell("Bene finanziato")
                        cell("Numero polizza")
                        cell("Data partenza copertura")
                        cell("Contraente effettivo")
                        cell("Cognome Nome Intestatario pt")
                        cell("Data Immatricolazione")
                        cell("Marca bene")
                        cell("Modello")
                        cell("Targa")
                        cell("Telaio")
                        cell("Valore Assicurato")
                        cell("Classe appartenenza contraente")
                        cell("TELEFONO DEALER")
                        cell("MAIL DEALER")
                        cell("STATO PRATICA")

                    }
                    if(polizze){
                        polizze.each { polizza ->
                            row {
                                cell(polizza.dealer.username)
                                cell(polizza.dealer.ragioneSociale)
                                cell(polizza.dealer.piva)
                                cell("")
                                cell(polizza.noPratica)
                                cell(polizza.tracciatoCompagnia.dataCaricamento)
                                cell(polizza.tracciatoCompagnia.dataErogazione)
                                cell(polizza.tracciatoCompagnia.importoRichiesto)
                                cell(polizza.tracciatoCompagnia.importoFinanziato)
                                cell(polizza.tracciatoCompagnia.importoNettoErogato)
                                cell(polizza.tracciatoCompagnia.codProdotto)
                                cell(polizza.cognomeInt)
                                cell(polizza.nomeInt)
                                cell(polizza.partitaIvaInt)
                                cell(polizza.tipoClienteInt.value.toString().toUpperCase())
                                cell(polizza.indirizzoInt)
                                cell(polizza.localitaInt)
                                cell(polizza.provinciaInt)
                                cell(polizza.capInt)
                                cell(polizza.telefonoInt )
                                cell(polizza.emailInt )
                                cell(polizza.cognomeGarante )
                                cell(polizza.nomeGarante )
                                cell(polizza.partitaIvaGarante )
                                cell(polizza.tipoClienteGarante.value.toString().toUpperCase())
                                cell(polizza.indirizzoGarante )
                                cell(polizza.localitaGarante)
                                cell(polizza.provinciaGarante )
                                cell(polizza.capGarante )
                                cell(polizza.telefonoGarante)
                                cell(polizza.emailGarante)
                                cell("")
                                cell("")
                                cell(polizza.tracciatoCompagnia.categoriaBene)
                                cell(polizza.tracciatoCompagnia.beneFinanziato)
                                cell(polizza.noPolizza)
                                cell(polizza.dataDecorrenza)
                                cell((polizza.tipoContraente.toString().toUpperCase()))
                                cell((polizza.tipoContraente.toString().toUpperCase()=="garante")?  "${polizza.cognomeGarante} ${polizza.nomeGarante}" : "${polizza.cognomeInt} ${polizza.nomeInt}")
                                cell(polizza.dataImmatricolazione)
                                cell(polizza.marca.toString().toUpperCase())
                                cell(polizza.modello.toString().toUpperCase())
                                cell(polizza.targa!=null?polizza.targa.toString().toUpperCase():"")
                                cell(polizza.telaio!=null?polizza.telaio.toString().toUpperCase():"")
                                cell(polizza.valoreAssicurato)
                                cell(polizza.tipoAcquisizione.toString()=="nuova polizza"? polizza.tipoAcquisizione.toString().toUpperCase():"VECCHIA POLIZZA")
                                cell(polizza.dealer.telefono)
                                cell(polizza.dealer.email)
                                cell(polizza.stato.toString().toUpperCase())
                            }
                            polizza.discard()
                        }
                        for (int i = 0; i < 50; i++) {
                            sheet.autoSizeColumn(i);
                        }
                    }else{
                            row{
                                cell("non ci sono polizze attivate")
                            }
                    }

                    }

                }
            }

        stream.write(wb)
        stream.close()
        return stream
    }catch (e){
             println "c'\u00E8 un problema con la generazione del file"
             response.add(codice:null, errore:"c'\u00E8 un problema con la generazione del file")
    }


    }
    def createPolizzeXLS(){
        def logg
        def response =[]
        Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{5}$)/
        Pattern pattern = ~/(\w+)/
        def polizze=Polizza.createCriteria().list(){
            // eq "stato", StatoPolizza.POLIZZA
        }
        def stream = new ByteArrayOutputStream()
        try {
            def wb =  new HSSFWorkbook()
        CellStyle style = wb.createCellStyle();//Create style
        Font font = wb.createFont();//Create font
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);//Make font bold
        style.setFont(font);//set it to bold
        //style.setFillBackgroundColor(IndexedColors.LIGHT_BLUE.getIndex());
            def sheet1 = wb.createSheet("elencoPolizze")
            def row=sheet1.createRow(0)
            row.createCell(0).setCellValue("Codice Dealer")
            row.createCell(1).setCellValue("Ragione Sociale Dealer")
            row.createCell(2).setCellValue("Partita Iva/Codice Fiscale Dealer")
            row.createCell(3).setCellValue("Codice campagna")
            row.createCell(4).setCellValue("Numero Pratica")
            row.createCell(5).setCellValue("Data caricamento/proposta (verificare)")
            row.createCell(6).setCellValue("Data erogazione")
            row.createCell(7).setCellValue("Importo richiesto")
            row.createCell(8).setCellValue("Importo finanziato")
            row.createCell(9).setCellValue("Importo Netto Erogato")
            row.createCell(10).setCellValue("Cod. Prodotto")
            row.createCell(11).setCellValue("Cognome Intestatario pt")
            row.createCell(12).setCellValue("Nome Intestatario pt")
            row.createCell(13).setCellValue("Codice Fiscale Intestatario")
            row.createCell(14).setCellValue("Sesso Intestatario")
            row.createCell(15).setCellValue("Indirizzo Intestatario pt")
            row.createCell(16).setCellValue("Localit\u00E1 Intestatario pt")
            row.createCell(17).setCellValue("PV Intestatario pt")
            row.createCell(18).setCellValue("CAP Intestatario pt")
            row.createCell(19).setCellValue("Telefono Intestatario")
            row.createCell(20).setCellValue("E-mail Intestatario")
            row.createCell(21).setCellValue("Cognome Garante pt")
            row.createCell(22).setCellValue("Nome Garante pt")
            row.createCell(23).setCellValue("Codice Fiscale Garante")
            row.createCell(24).setCellValue("Sesso Garante")
            row.createCell(25).setCellValue("Indirizzo Garante pt")
            row.createCell(26).setCellValue("Localit\u00E1 Garante pt")
            row.createCell(27).setCellValue("PV Garante pt")
            row.createCell(28).setCellValue("CAP Garante pt")
            row.createCell(29).setCellValue("Telefono Garante")
            row.createCell(30).setCellValue("E-mail Garante")
            row.createCell(31).setCellValue("Marca auto")
            row.createCell(32).setCellValue("Modello")
            row.createCell(33).setCellValue("Categoria bene")
            row.createCell(34).setCellValue("Bene finanziato")
            row.createCell(35).setCellValue("Numero polizza")
            row.createCell(36).setCellValue("Data partenza copertura")
            row.createCell(37).setCellValue("Contraente effettivo")
            row.createCell(38).setCellValue("Cognome Nome Intestatario pt")
            row.createCell(39).setCellValue("Data Immatricolazione")
            row.createCell(40).setCellValue("Marca bene")
            row.createCell(41).setCellValue("Modello")
            row.createCell(42).setCellValue("Targa")
            row.createCell(43).setCellValue("Telaio")
            row.createCell(44).setCellValue("Valore Assicurato")
            row.createCell(45).setCellValue("Classe appartenenza contraente")
            row.createCell(46).setCellValue("TELEFONO DEALER")
            row.createCell(47).setCellValue("MAIL DEALER")
            row.createCell(48).setCellValue("STATO PRATICA")
            for(int i = 0; i < row.getLastCellNum(); i++){//For each cell in the row
                row.getCell(i).setCellStyle(style);//Set the style
            }

            if(polizze){
                int i=1
                polizze.each { polizza ->
                    def rowc=sheet1.createRow(i)
                    rowc.createCell(0).setCellValue(polizza.dealer.username.toString())
                    rowc.createCell(1).setCellValue(polizza.dealer.ragioneSociale.toString())
                    rowc.createCell(2).setCellValue(polizza.dealer.piva.toString())
                    rowc.createCell(3).setCellValue("")
                    rowc.createCell(4).setCellValue(polizza.noPratica)
                    rowc.createCell(5).setCellValue(polizza.tracciatoCompagnia.dataCaricamento)
                    rowc.createCell(6).setCellValue(polizza.tracciatoCompagnia.dataErogazione)
                    rowc.createCell(7).setCellValue(polizza.tracciatoCompagnia.importoRichiesto.toString())
                    rowc.createCell(8).setCellValue(polizza.tracciatoCompagnia.importoFinanziato.toString())
                    rowc.createCell(9).setCellValue(polizza.tracciatoCompagnia.importoNettoErogato.toString())
                    rowc.createCell(10).setCellValue(polizza.tracciatoCompagnia.codProdotto.toString())
                    rowc.createCell(11).setCellValue(polizza.cognomeInt.toString().toUpperCase())
                    rowc.createCell(12).setCellValue(polizza.nomeInt.toString().toUpperCase())
                    rowc.createCell(13).setCellValue(polizza.partitaIvaInt.toString().toUpperCase())
                    rowc.createCell(14).setCellValue(polizza.tipoClienteInt.value.toString().toUpperCase())
                    rowc.createCell(15).setCellValue(polizza.indirizzoInt.toString())
                    rowc.createCell(16).setCellValue(polizza.localitaInt.toString().toUpperCase())
                    rowc.createCell(17).setCellValue(polizza.provinciaInt.toString().toUpperCase())
                    rowc.createCell(18).setCellValue(polizza.capInt.toString())
                    rowc.createCell(19).setCellValue(polizza.telefonoInt)
                    rowc.createCell(20).setCellValue(polizza.emailInt)
                    rowc.createCell(21).setCellValue(polizza.cognomeGarante.toString().toUpperCase())
                    rowc.createCell(22).setCellValue(polizza.nomeGarante.toString().toUpperCase())
                    rowc.createCell(23).setCellValue(polizza.partitaIvaGarante.toString().toUpperCase())
                    rowc.createCell(24).setCellValue(polizza.tipoClienteGarante.value.toString().toUpperCase())
                    rowc.createCell(25).setCellValue(polizza.indirizzoGarante.toString())
                    rowc.createCell(26).setCellValue(polizza.localitaGarante.toString().toUpperCase())
                    rowc.createCell(27).setCellValue(polizza.provinciaGarante.toString().toUpperCase())
                    rowc.createCell(28).setCellValue(polizza.capGarante.toString())
                    rowc.createCell(29).setCellValue(polizza.telefonoGarante)
                    rowc.createCell(30).setCellValue(polizza.emailGarante)
                    rowc.createCell(31).setCellValue("")
                    rowc.createCell(32).setCellValue("")
                    rowc.createCell(33).setCellValue(polizza.tracciatoCompagnia.categoriaBene.toString())
                    rowc.createCell(34).setCellValue(polizza.tracciatoCompagnia.beneFinanziato.toString())
                    rowc.createCell(35).setCellValue(polizza.noPolizza.toString())
                    if(polizza.dataDecorrenza!=null){
                        rowc.createCell(36).setCellValue(polizza.dataDecorrenza.format("ddMMyyyy"))
                    }else{
                        rowc.createCell(36).setCellValue("")
                    }
                    rowc.createCell(37).setCellValue(polizza.tipoContraente.toString().toUpperCase())
                    rowc.createCell(38).setCellValue((polizza.tipoContraente.toString().toUpperCase()=="garante")?  "${polizza.cognomeGarante} ${polizza.nomeGarante}" : "${polizza.cognomeInt} ${polizza.nomeInt}")
                    if(polizza.dataImmatricolazione!=null){
                        rowc.createCell(39).setCellValue(polizza.dataImmatricolazione.format("ddMMyyyy"))
                    }else{
                        rowc.createCell(39).setCellValue("")
                    }

                    rowc.createCell(40).setCellValue(polizza.marca.toString().toUpperCase())
                    rowc.createCell(41).setCellValue(polizza.modello.toString().toUpperCase())
                    rowc.createCell(42).setCellValue(polizza.targa!=null?polizza.targa.toString().toUpperCase():"")
                    rowc.createCell(43).setCellValue(polizza.telaio!=null?polizza.telaio.toString().toUpperCase():"")
                    rowc.createCell(44).setCellValue(polizza.valoreAssicurato)
                    rowc.createCell(45).setCellValue(polizza.tipoAcquisizione.toString()=="nuova polizza"? polizza.tipoAcquisizione.toString().toUpperCase():"VECCHIA POLIZZA")
                    rowc.createCell(46).setCellValue(polizza.dealer.telefono)
                    rowc.createCell(47).setCellValue(polizza.dealer.email)
                    rowc.createCell(48).setCellValue(polizza.stato.toString().toUpperCase())
                    i++
                    polizza.discard()
                    }

                }

                else{
                    def rowc=sheet1.createRow(1)
                        rowc.createCell(0).setCellValue("non ci sono polizze attivate")
                }
        for (int i = 0; i < 50; i++) {
            sheet1.autoSizeColumn(i);
        }

        wb.write(stream)
        stream.close()
        return stream
        }catch (e){
            logg =new Log(parametri: "c'è un problema con la generazione del file ${e.toString()}", operazione: " scarica polizze serale", pagina: "SCARICA POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "c'\u00E8 un problema con la generazione del file"
            response.add(codice:null, errore:"c'\u00E8 un problema con la generazione del file")
        }


    }

    def createPolizzeXLT(){
        def response=[]
        def polizze=Polizza.createCriteria().list(){
            // eq "stato", StatoPolizza.POLIZZA
        }
        def ragioneSociale,telInt,emailInt,telGarante,emailGarante,dataDecorrenza,dataImmatricolazione, tipoContraente,targa,telaio,tipoAcquisizione,valoreAssicurato,emailDealer,telDealer
        def stream = new ByteArrayOutputStream()
        try{
            def header="Codice Dealer\tRagione Sociale Dealer\tPartita Iva/Codice Fiscale Dealer\tCodice campagna\tNumero Pratica\tData caricamento/proposta (verificare)\tData erogazione\tImporto richiesto\tImporto finanziato\tImporto Netto Erogato\tCod. Prodotto\tCognome Intestatario pt\tNome Intestatario pt\tCodice Fiscale Intestatario\tSesso Intestatario\tIndirizzo Intestatario pt\tLocalit\u00E1 Intestatario pt\tPV Intestatario pt\tCAP Intestatario pt\tTelefono Intestatario\tE-mail Intestatario\tCognome Garante pt\tNome Garante pt\tCodice Fiscale Garante\tSesso Garante\tIndirizzo Garante pt\tLocalit\u00E1 Garante pt\tPV Garante pt\tCAP Garante pt\tTelefono Garante\tE-mail Garante\tMarca auto\tModello\tCategoria bene\tBene finanziato\tNumero polizza\tData partenza copertura\tContraente effettivo\tCognome Nome Intestatario pt\tData Immatricolazione\tMarca bene\tModello\tTarga\tTelaio\tValore Assicurato\tClasse appartenenza contraente\tTELEFONO DEALER\tMAIL DEALER\tSTATO PRATICA\n"

            stream.write(header.getBytes())
            if(polizze){
                int i=1
                polizze.each { polizza ->
                    ragioneSociale=polizza.dealer.ragioneSociale.toString().replaceAll("\"", "")
                    if(polizza.dataDecorrenza!=null){
                        dataDecorrenza=polizza.dataDecorrenza.format("ddMMyyyy")
                    }else{
                        dataDecorrenza=""
                    }
                    tipoContraente=(polizza.tipoContraente.toString().toUpperCase()=="garante")? "${polizza.cognomeGarante} ${polizza.nomeGarante}" : "${polizza.cognomeInt} ${polizza.nomeInt}"
                    if(polizza.dataImmatricolazione!=null){
                        dataImmatricolazione=polizza.dataImmatricolazione.format("ddMMyyyy")
                    }else{
                        dataImmatricolazione=""
                    }
                    telInt=(polizza.telefonoInt!=null)?polizza.telefonoInt.toString().toUpperCase():""
                    emailInt=(polizza.emailInt!=null)?polizza.emailInt.toString().toUpperCase():""
                    telGarante=(polizza.telefonoGarante!=null)?polizza.telefonoGarante.toString().toUpperCase():""
                    emailGarante=(polizza.emailGarante!=null)?polizza.emailGarante.toString().toUpperCase():""
                    emailDealer=(polizza.dealer.email!=null)?polizza.dealer.email.toString().toUpperCase():""
                    telDealer=(polizza.dealer.telefono!=null)?polizza.dealer.telefono.toString().toUpperCase():""
                    targa=(polizza.targa!=null)?polizza.targa.toString().toUpperCase():""
                    telaio=(polizza.telaio!=null)?polizza.telaio.toString().toUpperCase():""
                    valoreAssicurato=(polizza.valoreAssicurato!=null)?polizza.valoreAssicurato.toString().toUpperCase():""
                    tipoAcquisizione=(polizza.tipoAcquisizione.toString()=="nuova polizza")? polizza.tipoAcquisizione.toString().toUpperCase():"VECCHIA POLIZZA"
                    def dati="${polizza.dealer.username.toString()}\t${ragioneSociale}\t${polizza.dealer.piva.toString()}\t \t${polizza.noPratica.toString()}\t${polizza.tracciatoCompagnia.dataCaricamento}\t${polizza.tracciatoCompagnia.dataErogazione}\t${polizza.tracciatoCompagnia.importoRichiesto.toString()}\t${polizza.tracciatoCompagnia.importoFinanziato.toString()}\t${polizza.tracciatoCompagnia.importoNettoErogato.toString()}\t${polizza.tracciatoCompagnia.codProdotto.toString()}\t${polizza.cognomeInt.toString().toUpperCase()}\t${polizza.nomeInt.toString().toUpperCase()}\t${polizza.partitaIvaInt.toString().toUpperCase()}\t${polizza.tipoClienteInt.value.toString().toUpperCase()}\t${polizza.indirizzoInt.toString()}\t${polizza.localitaInt.toString().toUpperCase()}\t${polizza.provinciaInt.toString().toUpperCase()}\t${polizza.capInt.toString()}\t${telInt}\t${emailInt}\t${polizza.cognomeGarante.toString().toUpperCase()}\t${polizza.nomeGarante.toString().toUpperCase()}\t${polizza.partitaIvaGarante.toString().toUpperCase()}\t${polizza.tipoClienteGarante.value.toString().toUpperCase()}\t${polizza.indirizzoGarante.toString()}\t${polizza.localitaGarante.toString().toUpperCase()}\t${polizza.provinciaGarante.toString().toUpperCase()}\t${polizza.capGarante.toString()}\t${telGarante}\t${emailGarante}\t \t \t${polizza.tracciatoCompagnia.categoriaBene.toString()}\t${polizza.tracciatoCompagnia.beneFinanziato.toString()}\t${polizza.noPolizza.toString()}\t${dataDecorrenza}\t${polizza.tipoContraente.toString().toUpperCase()}\t${tipoContraente}\t${dataImmatricolazione}\t${polizza.marca.toString().toUpperCase()}\t${polizza.modello.toString().toUpperCase()}\t${targa}\t${telaio}\t${valoreAssicurato}\t${tipoAcquisizione}\t${telDealer}\t${emailDealer}\t${polizza.stato.toString().toUpperCase()}\n"
                    stream.write(dati.getBytes())
                    i++
                    polizza.discard()
                }
            }
            stream.close()
            return stream
        }   catch(e){
            println "c'\u00E8 un problema con la generazione del file"
            response.add(codice:null, errore:"c'\u00E8 un problema con la generazione del file")
        }

    }
}
