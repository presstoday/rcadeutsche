package rcadeutsche

import com.jcraft.jsch.ChannelSftp
import com.jcraft.jsch.JSch

import grails.transaction.Transactional
import grails.util.Environment
import groovy.time.TimeCategory
import excel.reader.*
import rcadeutsche.polizze.Polizza
import rcadeutsche.polizze.TipoCliente
import rcadeutsche.polizze.Tracciato
import rcadeutsche.polizze.TracciatoCompagnia

import java.text.DecimalFormat
import java.util.regex.Pattern
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import rcadeutsche.utenti.*


@Transactional
class CaricaPolizzeJob {
    def mailService
    static triggers = {

        if(Environment.current == Environment.PRODUCTION) {
            cron name: "CaricaPolizze", cronExpression: "0 05 13 * * ?"
        }else{
           // cron name: "CaricaPolizze", cronExpression: "0 00 16 * * ?"
        }

    }

    def group = "CaricamentoPolizze"
    def description = "job per caricare le polizze"
    def execute() {
        try {
            downloadFile()

        } catch (Exception e) {
            log.error e.message
        }
    }

    def downloadFile() {
        def host,port,username,password
        def logg
            host = "5.249.141.51"
            port = 22
            username = "flussi-nais"
            password = "XYZrrr655"
        try {
            def jsch = new JSch()
            def session = jsch.getSession(username, host, port)
            def properties = new Properties()
            properties.put("StrictHostKeyChecking", "no")
            session.config = properties
            session.userName = username
            session.password = password
            session.connect()
            def channel = session.openChannel("sftp") as ChannelSftp
            channel.connect()
            println "Connected to $host:$port"
            logg =new Log(parametri: "Connected to $host:$port", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            //channel.cd("flussi-bmw")
            println "Current sftp directory: ${channel.pwd()}"
            logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                channel.cd("..")
            if(Environment.current == Environment.PRODUCTION) {
                channel.cd("/home/flussi-brokeronline/to_Mach")
            }else{
                channel.cd("/home/flussi-brokeronline/to_Mach/TEST")
            }

            println "Current sftp directory: ${channel.pwd()}"
            logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def streamRiassunti = new ByteArrayOutputStream()
            def zipRiassunti = new ZipOutputStream(streamRiassunti)
            Vector<ChannelSftp.LsEntry> list = channel.ls("*.txt")
            def inserimento, fileNameRisposta, risposta, rispostaFinale, rispostaMail
            def i=0
            if(list.size()>0){
                println "sono stati trovati: ${list.size()}"
                logg =new Log(parametri: "sono stati trovati: ${list.size()}", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                for(ChannelSftp.LsEntry entry : list) {
                    boolean errori=false
                    println "downloaded: ${entry.filename}"
                    logg =new Log(parametri: "downloaded: ${entry.filename}", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    InputStream stream =  channel.get(entry.filename)
                    //createPolizzeCSV(stream)
                    def filename="${entry.filename}"
                    filename = filename.substring(0, filename.lastIndexOf("."));
                    inserimento =createPolizzeTXT(stream, filename)
                    fileNameRisposta = "risposta_${filename}_${i}.txt"
                    risposta="Riassunto inserimento:\r\n"
                    rispostaMail="Riassunto errori:\r\n"
                    inserimento.collect{ commento ->
                        if (commento?.messaggio){
                            risposta =risposta +" "+ commento.noPratica +" "+ commento.messaggio+"\r\n"
                        }else if (commento?.errore){
                            errori=true
                            rispostaMail =rispostaMail+" "+ commento.errore+"\r\n"
                        }
                    }.grep().join("\n")
                    rispostaFinale=risposta
                    i++
                    zipRiassunti.putNextEntry(new ZipEntry(fileNameRisposta))
                    zipRiassunti.write(rispostaFinale.bytes)
                    if(errori){
                        zipRiassunti.write(rispostaMail.bytes)
                    }
                    if(Environment.current == Environment.PRODUCTION) {
                        channel.rename("/home/flussi-brokeronline/to_Mach/"+entry.filename,"/home/flussi-brokeronline/to_Mach/"+entry.filename+".old")
                        println "rename: ${entry.filename}"
                    }

                    logg =new Log(parametri: "rename: ${entry.filename}", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    if(errori){
                        def inviaMailCaricamento=mailService.invioMailCaricamento(rispostaMail, filename)
                        if(inviaMailCaricamento==true){
                            println "la mail di carimenti falliti \u00E8 stata inviata"
                            logg =new Log(parametri: "la mail di carimenti falliti \u00E8 stata inviata", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            //response.add(noPratica:'', messaggio:"la mail di carimenti falliti \u00E8 stata inviata")
                        }else{
                            logg =new Log(parametri: "Non \u00E8 stato possibile inviare la mail di carimenti falliti", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            println "Non \u00E8 stato possibile inviare la mail di carimenti falliti"
                            // response.add(noPratica:'', errore:"Non \u00E8 stato possibile inviare la mail di carimenti falliti")
                        }
                    }
                }
                zipRiassunti.close()
                streamRiassunti.close()
                channel.put(new ByteArrayInputStream(streamRiassunti.toByteArray()), "riassunto_${new Date().format("yyyyMMdd")}.zip")
                logg =new Log(parametri: "riassunto_${new Date().format("yyyyMMdd")}", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "riassunto_${new Date().format("yyyyMMdd")}"
                println "Current sftp directory: ${channel.pwd()}"
                logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
            channel.disconnect()
            session.disconnect()


        } catch(e) { e.printStackTrace() }

    }

    def createPolizzeXLSX(InputStream file){
        def response =[]
        Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{5}$)/
        Pattern pattern = ~/(\w+)/

        def dealer
        try {
            def wb = ExcelReader.readXlsx(file) {
                sheet (0) {
                    rows(from: 1) {
                        def cellaDealer = cell("A")?.value?:""
                        def cellnoPratica = cell("E")?.value?:""
                        //def cellCodiceCampagana  = cell("B").value
                        def cellaCognomeInt  = cell("L")?.value?:""
                        def cellaNomeInt  = cell("M")?.value?:""
                        def cellaPIvaInt = cell("N")?.value?:""
                        def cellaSessoInt = cell("O")?.value?:""
                        def cellaIndirizzoInt = cell("P")?.value?:""
                        def cellaProvinciaInt = cell("R")?.value?:""
                        def cellaLocalitaInt = cell("Q")?.value?:""
                        def cellaCapInt = cell("S")?.value?:""
                        def cellaTelInt = cell("T")?.value ?:""
                        def cellaEmailInt = cell("U")?.value?:""
                        def cellaCognomeGarante= cell("V")?.value?:""
                        def cellaNomeGarante= cell("W")?.value?:""
                        def cellaCodFiscGarante= cell("X")?.value?:""
                        def cellaSessoGarante= cell("Y")?.value?:""
                        def cellaIndirizzoGarante= cell("Z")?.value?:""
                        def cellaLocalitaGarante= cell("AA")?.value?:""
                        def cellaProvGarante= cell("AB")?.value?:""
                        def cellaCapGarante= cell("AC")?.value?:""
                        def cellaTelGarante= cell("AD")?.value?:""
                        def cellaEmailGarante= cell("AE")?.value?:""
                        def cellaMarcaAuto= cell("AF")?.value?:""
                        def cellaModelloAuto= cell("AG")?.value?:""
                        def cellaCategoriabene= cell("AH")?.value?:""
                        def cellaBeneFinanzia= cell("AI")?.value?:""

                        /*int numParole=pattern.matcher(cellCognome).count
                        //println numParole
                        def nome=""
                        def cognome=" "
                        def parole=[]
                        parole= cellCognome.split(" ")

                        if(parole.size()==2){
                            nome=parole[0]
                            cognome=parole[1]
                        }else if(parole.size()>2){
                            nome=parole[0]
                            for ( int ind=1; ind<parole.size();ind++) {

                                cognome+=parole[ind]+" "
                            }

                        }*/

                        if(cellaNomeInt.toString().equals("null")){
                            cellaNomeInt="";
                        }else{
                            cellaNomeInt=cellaNomeInt.toString().trim()
                        }
                        if(cellaCognomeInt.toString().equals("null")){
                            cellaCognomeInt="";
                        }else{
                            cellaCognomeInt=cellaCognomeInt.toString().trim()
                        }

                        if(cellnoPratica.toString().equals("null")){
                            cellnoPratica="";
                        }else{
                            cellnoPratica=cellnoPratica.toString().replaceFirst ("^0*", "")
                            if(cellnoPratica.toString().contains(".")){
                                def punto=cellnoPratica.toString().indexOf(".")
                                cellnoPratica=cellnoPratica.toString().substring(0,punto).replaceAll("[^0-9]", "")
                            }
                        }
                        if(cellaCapInt.toString().equals("null")){
                            cellaCapInt="";
                        }else{
                            if(cellaCapInt.toString().contains(".")){
                                def punto=cellaCapInt.toString().indexOf(".")
                                cellaCapInt=cellaCapInt.toString().substring(0,punto).replaceAll("[^0-9]", "")
                            }
                        }
                        if(cellaDealer.toString().equals("null")){
                            cellaDealer="";
                        }else{
                            cellaDealer=cellaDealer.toString().replaceFirst ("^0*", "")
                            if(cellaDealer.toString().contains(".")){
                                cellaDealer=Double.valueOf(cellaDealer).longValue().toString()
                                //cellaDealer=cellaDealer.toString().substring(0,punto).replaceAll("[^0-9]", "")
                            }
                            dealer=Dealer.findByUsername(cellaDealer.toString())
                        }
                        cellaPIvaInt=cellaPIvaInt.toString().replaceAll("[^a-z,A-Z,0-9]","")
                        def sessoInt
                        if(cellaSessoInt.toString().trim().equals("null")){
                            sessoInt="";
                        }else if (cellaSessoInt.toString().trim().equalsIgnoreCase("M")) sessoInt=TipoCliente.M
                        else if (cellaSessoInt.toString().trim().equalsIgnoreCase("F")) sessoInt=TipoCliente.F
                        else sessoInt=TipoCliente.DITTA_INDIVIDUALE
                        def sessoGrnt
                        if(cellaSessoGarante.toString().trim().equals("null")){
                            sessoGrnt="";
                        }else if (cellaSessoGarante.toString().trim().equalsIgnoreCase("M")) sessoGrnt=TipoCliente.M
                        else if (cellaSessoGarante.toString().trim().equalsIgnoreCase("F")) sessoGrnt=TipoCliente.F
                        else sessoGrnt=TipoCliente.DITTA_INDIVIDUALE

                        if(cellaNomeGarante.toString().equals("null")){
                            cellaNomeGarante="";
                        }else{
                            cellaNomeGarante=cellaNomeGarante.toString().trim()
                        }
                        if(cellaCognomeGarante.toString().equals("null")){
                            cellaCognomeGarante="";
                        }else{
                            cellaCognomeGarante=cellaCognomeGarante.toString().trim()
                        }
                        if(cellaCapGarante.toString().equals("null")){
                            cellaCapGarante="";
                        }else{
                            if(cellaCapGarante.toString().contains(".")){
                                def punto=cellaCapGarante.toString().indexOf(".")
                                cellaCapGarante=cellaCapGarante.toString().substring(0,punto).replaceAll("[^0-9]", "")
                            }
                        }
                        cellaCodFiscGarante=cellaCodFiscGarante.toString().replaceAll("[^a-z,A-Z,0-9]","")
                        if(cellaMarcaAuto.toString().equals("null")){
                            cellaMarcaAuto="";
                        }else{
                            cellaMarcaAuto=cellaMarcaAuto.toString().trim()
                        }
                        if(cellaModelloAuto.toString().equals("null")){
                            cellaModelloAuto="";
                        }else{
                            cellaModelloAuto=cellaModelloAuto.toString().trim()
                        }
                        if(dealer && cellnoPratica){
                            def polizza = Polizza.findOrCreateWhere(
                                    noPratica: cellnoPratica,
                                    dealer: dealer,
                                    //codiceCampagna:  cellCodiceCampagana.toString().trim(),
                                    nomeInt: cellaNomeInt.toString().trim(),
                                    cognomeInt:  cellaCognomeInt.toString().trim(),
                                    partitaIvaInt: cellaPIvaInt.toString().trim(),
                                    tipoClienteInt: sessoInt,
                                    indirizzoInt: cellaIndirizzoInt.toString().trim(),
                                    localitaInt: cellaLocalitaInt.toString().trim(),
                                    capInt: cellaCapInt.toString().trim(),
                                    provinciaInt: cellaProvinciaInt.toString().trim(),
                                    nomeGarante:  cellaNomeGarante.toString().trim(),
                                    cognomeGarante:  cellaCognomeGarante.toString().trim(),
                                    partitaIvaGarante: cellaCodFiscGarante.toString().trim(),
                                    tipoClienteGarante: sessoGrnt,
                                    indirizzoGarante: cellaIndirizzoGarante.toString().trim(),
                                    localitaGarante: cellaLocalitaGarante.toString().trim(),
                                    capGarante: cellaCapGarante.toString().trim(),
                                    provinciaGarante: cellaProvGarante.toString().trim(),
                                    marca: cellaMarcaAuto,
                                    modello: cellaModelloAuto
                            )
                            if(polizza.save(flush:true)) {
                                response.add(noPratica:cellnoPratica, messaggio:"polizza creata correttamente ${polizza.noPolizza}")
                            }
                            else{
                                println polizza.errors
                                response.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: ${polizza.errors}")
                            }
                        }else if(!dealer){
                            println "il dealer non è presente"
                            response.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: il dealer non \u00E8 presente")
                        }else if(!cellnoPratica){
                            println "Errore creazione polizza: non è presente un numero di pratica"
                            response.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: non \u00E8 presente un numero di pratica")
                        }
                    }
                }
            }
        }catch (e){
            println "c'\u00E8 un problema con il file, ${file} verificare che non ci siano righe con valori nulli"
            response.add(codice:null, errore:"c'\u00E8 un problema con il file, ${file} verificare che non ci siano righe con valori nulli")
        }

        return response
    }
    def createPolizzeCSV(InputStream file){
        def response =[]
        Pattern pattern = ~/(\w+)/
        def fileReader = null
        def reader = null
        //Delimiter used in CSV file
        def delimiter = ";"
        def dealer

        try {
            def line = ""
            //Create the file reader
            reader = new InputStreamReader(file)
            fileReader = new BufferedReader(reader)
            fileReader.readLine()
            while ((line = fileReader.readLine()) != null)
            {

                //Get all tokens available in line
                def tokens = []
                tokens = line.split(delimiter)
                def cellnoPratica = tokens[0].toString().trim()
                def cellCodiceCampagana  = tokens[1].toString().trim()
                def cellCognome  = tokens[2].toString().trim()
                def cellPIva = tokens[3].toString().trim()
                def cellSesso = tokens[4].toString().trim()
                def cellIndirizzo = tokens[5].toString().trim()
                def cellProvincia = tokens[6].toString().trim()
                def cellLocalita = tokens[7].toString().trim()
                def cellCap = tokens[8].toString().trim().replace("'","")
                def cellaDealer = tokens[9].toString().trim()
                //int numParole=pattern.matcher(cellCognome).count
                //println numParole
                def nome=""
                def cognome=" "
                def parole=[]
                parole= cellCognome.split(" ")
                if(parole.size()==2){
                    nome=parole[1]
                    cognome=parole[0]
                }else if(parole.size()>2){
                    cognome=parole[0]
                    for ( int ind=1; ind<parole.size();ind++) {
                        nome+=parole[ind]+" "
                    }
                }
                if(cellnoPratica.equals("null")){
                    cellnoPratica="";
                }else{
                    cellnoPratica=cellnoPratica.replaceFirst ("^0*", "")
                    if(cellnoPratica.contains(".")){
                        def punto=cellnoPratica.indexOf(".")
                        cellnoPratica=cellnoPratica.substring(0,punto).replaceAll("[^0-9]", "")
                    }
                }
                if(cellCap.equals("null")){
                    cellCap="";
                }else{
                    if(cellCap.toString().contains(".")){
                        def punto=cellCap.indexOf(".")
                        cellCap=cellCap.substring(0,punto).replaceAll("[^0-9]", "")
                    }
                }
                if(cellaDealer.equals("null")){
                    cellaDealer="";
                }else{
                    cellaDealer=cellaDealer.replaceFirst ("^0*", "")
                    if(cellaDealer.contains(".")){
                        def punto=cellaDealer.indexOf(".")
                        cellaDealer=cellaDealer.substring(0,punto).replaceAll("[^0-9]", "")
                    }
                    dealer=Dealer.findByUsername(cellaDealer)
                }
                cellPIva=cellPIva.replaceAll("[^a-z,A-Z,0-9]","")
                def sesso
                if(cellSesso.equals("null")){
                    sesso="";
                }else if (cellSesso.equalsIgnoreCase("M")) sesso=TipoCliente.M
                else if (cellSesso.equalsIgnoreCase("F")) sesso=TipoCliente.F
                else sesso=TipoCliente.DITTA_INDIVIDUALE

                if(dealer && cellnoPratica){
                    def polizza = Polizza.findOrCreateWhere(
                            noPratica: cellnoPratica,
                            dealer: dealer,
                            codiceCampagna:  cellCodiceCampagana.toString().trim(),
                            nomeInt: nome.toString().trim(),
                            cognome:  cognome.toString().trim(),
                            partitaIva: cellPIva.toString().trim(),
                            tipoCliente: sesso,
                            indirizzo: cellIndirizzo.toString().trim(),
                            comune: cellLocalita.toString().trim(),
                            cap: cellCap.toString().trim(),
                            provincia: cellProvincia.toString().trim()
                    )
                    if(polizza.save(flush:true)) {
                        response.add(noPratica:"${polizza.noPratica}", messaggio:"polizza creata correttamente ${polizza.noPolizza}")
                    }
                    else response.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: ${polizza.errors}")
                }else if(!dealer){
                    response.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: il dealer non \u00E8 presente")
                }else if(!cellnoPratica){
                    response.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: non \u00E8 presente un numero di pratica")
                }
            }

       }catch (e){
            println "c'\u00E8 un problema con il file, ${file} verificare che non ci siano righe con valori nulli sino nel csv"
            response.add(codice:null, errore:"c'\u00E8 un problema con il file, ${file} verificare che non ci siano righe con valori nulli sono nel csv")
        }

        return response
    }
    def createPolizzeTXT(InputStream file, String filename){
        def logg
        def response =[]
        Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$)/
        Pattern pattern = ~/(\w+)/
        String regex = "<(\\S+)[^>]+?mso-[^>]*>.*?</\\1>";
        def x_value=[]
        def dealer
        def dealers=[]
        BufferedReader reader = new BufferedReader(new InputStreamReader(file, "UTF-8"));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                //if(line.trim().isEmpty() ||  line.equals("<TITLE>") || line.equals("REPORT") || line.equals("</TITLE>") || line.equals("<HTML>") || line.equals("<HEAD>") || line.equals("</HEAD>") || line.contains("COL1") || line.equals("<BODY>")|| line.equals("<PRE>")|| line.equals("</PRE>")|| line.equals("</BODY>")|| line.equals("</HTML>") || line.contains("----------------------")){
                if(line.trim().isEmpty() ||  !line.contains(";") ){

                    continue;
                }
                x_value.add(line.split(";"))
            }
            for(int i=0; i<x_value.size();i++){
                //println x_value[i]
                def valore=x_value[i]
                def cellaDealer = valore[0].toString().trim()
                def cellnoPratica = valore[4].toString().trim()
                def celladataCaricamento = valore[5].toString().trim()
                def celladataErogazione = valore[6].toString().trim()
                def cellaImportoRichiesto = valore[7].toString().trim()
                def cellaImportoFinanziato = valore[8].toString().trim()
                def cellaImportoNetto = valore[9].toString().trim()
                def cellaNoRate = valore[10].toString().trim()
                def cellaCodProd = valore[11].toString().trim()
                def cellaCognomeInt  =  valore[12].toString().trim()
                def cellaNomeInt  =  valore[13].toString().trim()
                def cellaPIvaInt =  valore[14].toString().trim()
                def cellaSessoInt =  valore[15].toString().trim()
                def cellaIndirizzoInt =  valore[16].toString().trim()
                def cellaLocalitaInt =  valore[17].toString().trim()
                def cellaProvinciaInt =  valore[18].toString().trim()
                def cellaCapInt =  valore[19].toString().trim()
                def cellaTelInt =  valore[20].toString().trim()
                def cellaEmailInt = valore[21].toString().trim()
                def cellaCognomeGarante=  valore[22].toString().trim()
                def cellaNomeGarante=  valore[23].toString().trim()
                def cellaCodFiscGarante=  valore[24].toString().trim()
                def cellaSessoGarante=  valore[25].toString().trim()
                def cellaIndirizzoGarante=  valore[26].toString().trim()
                def cellaLocalitaGarante=  valore[27].toString().trim()
                def cellaProvGarante=  valore[28].toString().trim()
                def cellaCapGarante=  valore[29].toString().trim()
                def cellaTelGarante=  valore[30].toString().trim()
                def cellaEmailGarante=  valore[31].toString().trim()
                def cellaMarcaAuto=  valore[32].toString().trim()
                def cellaModelloAuto=  valore[33].toString().trim()
                def cellaCategoriabene=  valore[34].toString().trim()
                def cellaBeneFinanzia=  valore[35].toString().trim()
                def cellaAnnoImm=  valore[36].toString().trim()
                def cellaNuovo=  valore[38].toString().trim()
                def cellaTipo=  valore[39].toString().trim()
                if(cellaNomeInt.toString().equals("null")){
                    cellaNomeInt="";
                }else{
                    cellaNomeInt=cellaNomeInt.toString().trim()
                }
                if(cellaCognomeInt.toString().equals("null")){
                    cellaCognomeInt="";
                }else{
                    cellaCognomeInt=cellaCognomeInt.toString().trim()
                }

                if(cellnoPratica.toString().equals("null")){
                    cellnoPratica="";
                }else{
                    cellnoPratica=cellnoPratica.toString().replaceFirst ("^0*", "")
                    if(cellnoPratica.toString().contains(".")){
                        def punto=cellnoPratica.toString().indexOf(".")
                        cellnoPratica=cellnoPratica.toString().substring(0,punto).replaceAll("[^0-9]", "")
                    }
                }
                if(cellaCapInt.toString().equals("null")){
                    cellaCapInt="";
                }else{
                    if(cellaCapInt.toString().contains(".")){
                        def punto=cellaCapInt.toString().indexOf(".")
                        cellaCapInt=cellaCapInt.toString().substring(0,punto).replaceAll("[^0-9]", "")
                    }
                }
                if(cellaDealer.toString().equals("null")){
                    cellaDealer=""
                }else{
                    cellaDealer=cellaDealer.toString().replaceFirst ("^0*", "")
                    if(cellaDealer.toString().contains(".")){
                        cellaDealer=Double.valueOf(cellaDealer).longValue().toString()
                        //cellaDealer=cellaDealer.toString().substring(0,punto).replaceAll("[^0-9]", "")
                    }
                    //println cellaDealer
                    dealer=Dealer.findByUsername(cellaDealer.toString())
                }
                cellaPIvaInt=cellaPIvaInt.toString().replaceAll("[^a-z,A-Z,0-9]","")
                def sessoInt
                if(cellaSessoInt.toString().trim().equals("null")){
                    sessoInt=""
                }else if (cellaSessoInt.toString().trim().equalsIgnoreCase("M")) sessoInt=TipoCliente.M
                else if (cellaSessoInt.toString().trim().equalsIgnoreCase("F")) sessoInt=TipoCliente.F
                else sessoInt=TipoCliente.DITTA_INDIVIDUALE
                def sessoGrnt
                if(cellaSessoGarante.toString().trim().equals("null")){
                    sessoGrnt=""
                }else if (cellaSessoGarante.toString().trim().equalsIgnoreCase("M")) sessoGrnt=TipoCliente.M
                else if (cellaSessoGarante.toString().trim().equalsIgnoreCase("F")) sessoGrnt=TipoCliente.F
                else sessoGrnt=TipoCliente.DITTA_INDIVIDUALE

                if(cellaNomeGarante.toString().equals("null")){cellaNomeGarante="";}
                else{cellaNomeGarante=cellaNomeGarante.toString().trim()}

                if(cellaCognomeGarante.toString().equals("null")){cellaCognomeGarante="";}
                else{cellaCognomeGarante=cellaCognomeGarante.toString().trim()}

                if(cellaCapGarante.toString().equals("null")){
                    cellaCapGarante=""
                }else{
                    if(cellaCapGarante.toString().contains(".")){
                        def punto=cellaCapGarante.toString().indexOf(".")
                        cellaCapGarante=cellaCapGarante.toString().substring(0,punto).replaceAll("[^0-9]", "")
                    }
                }
                cellaCodFiscGarante=cellaCodFiscGarante.toString().replaceAll("[^a-z,A-Z,0-9]","")

                if(cellaMarcaAuto.toString().equals("null")){cellaMarcaAuto=""}
                else{cellaMarcaAuto=cellaMarcaAuto.toString().trim()}

                if(cellaModelloAuto.toString().equals("null")){cellaModelloAuto=""}
                else{cellaModelloAuto=cellaModelloAuto.toString().trim()}

                if(celladataCaricamento.toString().equals("null")){ celladataCaricamento="" }
                else{  celladataCaricamento=celladataCaricamento.toString().trim() }

                if(celladataErogazione.toString().equals("null")){  celladataErogazione=""  }
                else{  celladataErogazione=celladataErogazione.toString().trim() }

                if(cellaImportoRichiesto.toString().equals("null")){  cellaImportoRichiesto="" }
                else{  cellaImportoRichiesto=cellaImportoRichiesto.toString().trim()  }

                if(cellaImportoFinanziato.toString().equals("null")){ cellaImportoFinanziato="" }
                else{  cellaImportoFinanziato=cellaImportoFinanziato.toString().trim() }

                if(cellaImportoNetto.toString().equals("null")){ cellaImportoNetto=""  }
                else{ cellaImportoNetto=cellaImportoNetto.toString().trim() }

                if(cellaNoRate.toString().equals("null")){cellaNoRate=""}
                else{cellaNoRate=cellaNoRate.toString().trim() }

                if(cellaCodProd.toString().equals("null")){cellaCodProd=""}
                else{cellaCodProd=cellaCodProd.toString().trim()}

                if(cellaCategoriabene.toString().equals("null")){cellaCategoriabene=""}
                else{cellaCategoriabene=cellaCategoriabene.toString().trim()}

                if(cellaBeneFinanzia.toString().equals("null")){cellaBeneFinanzia=""}
                else{cellaBeneFinanzia=cellaBeneFinanzia.toString().trim()}

                if(cellaAnnoImm.toString().equals("null")){cellaAnnoImm=""}
                else{cellaAnnoImm=cellaAnnoImm.toString().trim()}

                if(cellaNuovo.toString().equals("null")){cellaNuovo=""}
                else{cellaNuovo=cellaNuovo.toString().trim()}

                if(cellaTipo.toString().equals("null")){cellaTipo=""}
                else{cellaTipo=cellaTipo.toString().trim()}
                if(dealer && cellnoPratica){
                    def polizzaesistente= Polizza.findByNoPraticaIlike(cellnoPratica)
                    if (polizzaesistente){
                        logg =new Log(parametri: "errore:la pratica-->${cellnoPratica} \u00E8 gia' stata caricata nel portale", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        response.add(errore:"la pratica-->${cellnoPratica} \u00E8 gia' stata caricata nel portale")
                        dealers.add(errore:"la pratica-->${cellnoPratica} \u00E8 gia' stata caricata nel portale")
                    }else{
                        def polizza = Polizza.findOrCreateWhere(
                                noPratica: cellnoPratica,
                                dealer: dealer,
                                nomeInt: cellaNomeInt.toString().trim(),
                                cognomeInt:  cellaCognomeInt.toString().trim(),
                                partitaIvaInt: cellaPIvaInt.toString().trim(),
                                tipoClienteInt: sessoInt,
                                indirizzoInt: cellaIndirizzoInt.toString().trim(),
                                localitaInt: cellaLocalitaInt.toString().trim(),
                                capInt: cellaCapInt.toString().trim(),
                                provinciaInt: cellaProvinciaInt.toString().trim(),
                                nomeGarante:  cellaNomeGarante.toString().trim(),
                                cognomeGarante:  cellaCognomeGarante.toString().trim(),
                                partitaIvaGarante: cellaCodFiscGarante.toString().trim(),
                                tipoClienteGarante: sessoGrnt,
                                indirizzoGarante: cellaIndirizzoGarante.toString().trim(),
                                localitaGarante: cellaLocalitaGarante.toString().trim(),
                                capGarante: cellaCapGarante.toString().trim(),
                                provinciaGarante: cellaProvGarante.toString().trim(),
                                marca: cellaMarcaAuto,
                                modello: cellaModelloAuto,
                                autoveicolo:!(cellaTipo=='A')?false:true
                        )
                        if(!polizza.save(flush:true)) {
                            println polizza.errors
                            response.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: ${polizza.errors}")
                            dealers.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: ${polizza.errors}")
                            logg =new Log(parametri: "noPratica:${cellnoPratica} errore:Errore creazione polizza: ${polizza.errors}", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }
                        else{
                            def tracciatoCompagnia= TracciatoCompagnia.findOrCreateWhere(
                                    dataCaricamento: celladataCaricamento.toString().trim(),
                                    dataErogazione: celladataErogazione.toString().trim(),
                                    importoRichiesto: cellaImportoRichiesto.toString().trim(),
                                    importoFinanziato: cellaImportoFinanziato.toString().trim(),
                                    importoNettoErogato: cellaImportoNetto.toString().trim(),
                                    codProdotto: cellaCodProd.toString().trim(),
                                    categoriaBene: cellaCategoriabene.toString().trim(),
                                    beneFinanziato: cellaBeneFinanzia.toString().trim(),
                                    polizza: polizza
                            )
                            if(!tracciatoCompagnia.save(flush:true)){
                                println "${tracciatoCompagnia.errors}"
                                response.add(noPratica:cellnoPratica, messaggio:"non \u00E8  stato possibile inserire i dati per il tracciato della compagnia")
                                logg =new Log(parametri: "noPratica:${cellnoPratica} errore:non \u00E8  stato possibile inserire i dati per il tracciato della compagnia ${tracciatoCompagnia.errors}", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }else{
                                response.add(noPratica:cellnoPratica, messaggio:"dati del tracciato inseriti correttamente no. id tracciato: ${tracciatoCompagnia.id}")
                                response.add(noPratica:cellnoPratica, messaggio:"polizza creata correttamente ${polizza.noPolizza}")
                                logg =new Log(parametri: "noPratica:${cellnoPratica} dati del tracciato inseriti correttamente no. id tracciato: ${tracciatoCompagnia.id}", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                logg =new Log(parametri: "noPratica:${cellnoPratica} polizza creata correttamente ${polizza.noPolizza}", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }
                        }
                    }
                }else if(!dealer){
                    println "il dealer non è presente"
                    dealers.add(noPratica:"Per la pratica  numero: ${cellnoPratica}", errore:" il dealer non \u00E8 presente")
                    response.add(noPratica:"Per la pratica  numero: ${cellnoPratica}", errore:" il dealer non \u00E8 presente")
                    logg =new Log(parametri: "noPratica:${cellnoPratica} il dealer non \u00E8 presente", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }else if(!cellnoPratica){
                    logg =new Log(parametri: "Errore creazione polizza: non \u00E8 presente un numero di pratica", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    println "Errore creazione polizza: non è presente un numero di pratica"
                    response.add(noPratica:"non è presente un numero di pratica", errore:"")
                    dealers.add(noPratica:"non è presente un numero di pratica", errore:"")
                }

            }
            reader.close()

        } catch (e){
            logg =new Log(parametri: "c'\u00E8 un problema con il file, ${filename}--> ${e.toString()}", operazione: " caricamento pratiche", pagina: "CARICA POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "c'\u00E8 un problema con il file, ${filename} verificare che non ci siano righe con valori nulli"
            response.add(noPratica:'', errore:"c'\u00E8 un problema con il file ${filename} verificare che non ci siano righe con valori nulli")
            dealers.add(noPratica:'', errore:"c'\u00E8 un problema con il file ${filename} verificare che non ci siano righe con valori nulli")
        }
        /*if(dealers.size()>0){
            def inviaMailCaricamento=mailService.invioMailCaricamento(dealers, filename)
            if(inviaMailCaricamento==true){
                println "la mail di carimenti falliti è stata inviata"
                response.add(noPratica:'', messaggio:"la mail di carimenti falliti \u00E8 stata inviata")
            }else{
                println "Non è stato possibile inviare la mail di carimenti falliti"
                response.add(noPratica:'', errore:"Non \u00E8 stato possibile inviare la mail di carimenti falliti")
            }
        }*/
        return response
    }
}
