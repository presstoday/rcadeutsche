package rcadeutsche

import groovy.time.TimeCategory


class HolidayService {
    static transactional = false
    def dataDecorr() {
        def dataDecorrenza=use(TimeCategory) { new Date() }
        def dateFestive=['01-01','06-01','25-04','01-05','02-06','15-08','01-11','08-12','25-12','26-12']

        if(dataDecorrenza.getDay()==6){
            dataDecorrenza=use(TimeCategory) { dataDecorrenza + 2.days }
        }
        else if(dataDecorrenza.getDay()==7){
            dataDecorrenza=use(TimeCategory) { dataDecorrenza + 1.days }
        }else if(dataDecorrenza.getDay()==5){
            dataDecorrenza=use(TimeCategory) { dataDecorrenza + 3.days }
        }else {dataDecorrenza=use(TimeCategory) { dataDecorrenza + 1.days }}

        def annoCorrente=dataDecorrenza.format('y').toInteger()
        def pasqua=dataPasqua(annoCorrente)

        if(pasqua.equals(dataDecorrenza.format('dd-MM-yyyy'))){
            dataDecorrenza=use(TimeCategory) { dataDecorrenza + 1.days}
            }

        if(dateFestive.contains(dataDecorrenza.format('dd-MM')) ){
            println dataDecorrenza.format('dd-MM')
            dataDecorrenza=use(TimeCategory) {  dataDecorrenza + 1.days}
        }

        return dataDecorrenza
    }

    def dataPasqua(def anno){
        if ( (anno < 1573) || (anno > 2499) ) {
            println "anno fuori range"
        }
        def a = anno % 19
        def b = anno % 4
        def c = anno % 7

        def m = 0;
        def n = 0;

        if ( (anno >= 1583) && (anno <= 1699) ) { m = 22; n = 2; }
        if ( (anno >= 1700) && (anno <= 1799) ) { m = 23; n = 3; }
        if ( (anno >= 1800) && (anno <= 1899) ) { m = 23; n = 4; }
        if ( (anno >= 1900) && (anno <= 2099) ) { m = 24; n = 5; }
        if ( (anno >= 2100) && (anno <= 2199) ) { m = 24; n = 6; }
        if ( (anno >= 2200) && (anno <= 2299) ) { m = 25; n = 0; }
        if ( (anno >= 2300) && (anno <= 2399) ) { m = 26; n = 1; }
        if ( (anno >= 2400) && (anno <= 2499) ) { m = 25; n = 1; }

        def d = (19 * a + m) % 30
        def e = (2 * b + 4 * c + 6 * d + n) % 7

        Calendar calendar = new GregorianCalendar()
        calendar.set(Calendar.YEAR , anno)

        if ( d+e < 10 ) {
            calendar.set(Calendar.MONTH , Calendar.MARCH);
            calendar.set(Calendar.DAY_OF_MONTH, d + e + 23);
        } else {
            calendar.set(Calendar.MONTH , Calendar.APRIL);
            int day = d+e-8;
            if ( 26 == day ) {day = 19}
            if ( ( 25 == day ) && ( 28 == d) && ( e == 6 ) && ( a > 10 ) ) { day = 18 }
            calendar.set(Calendar.DAY_OF_MONTH, day);
        }
       // println calendar.getTime()
        return calendar.getTime().format('dd-MM-yyyy')
    }




}
