package tracciati

import rcadeutsche.polizze.*

class TracciatiService {
    static transactional = false

    def generaTracciatoIAssicur(Polizza polizza) {
        def datiContabili=DatiContabili.getAll().first()
        def provincia=(polizza.tipoContraente.toString()=="garante")? polizza.provinciaGarante.toString().trim():polizza.provinciaInt.toString().trim()
        def tasse=Tasse.findByProvincia(provincia)
        def marca
        if(Marche.findByMarca(polizza.marca.toString().trim())){
            marca=Marche.findByMarca(polizza.marca.toString().trim()).codice
        }else{marca=""}

        def cognome=(polizza.tipoContraente.toString()=="garante")? polizza.cognomeGarante.toString().trim() : polizza.cognomeInt.toString().trim()
        def nome=(polizza.tipoContraente.toString()=="garante")? polizza.nomeGarante.toString().trim() : polizza.nomeInt.toString().trim()
        def provinciaContr=(polizza.tipoContraente.toString()=="garante")? polizza.provinciaGarante.toString().trim() : polizza.provinciaInt.toString().trim()
        def indirizzo=(polizza.tipoContraente.toString()=="garante")? polizza.indirizzoGarante.toString().trim() : polizza.indirizzoInt.toString().trim()
        def cap=(polizza.tipoContraente.toString()=="garante")? polizza.capGarante.toString().trim() : polizza.capInt.toString().trim()
        def citta=(polizza.tipoContraente.toString()=="garante")? polizza.localitaGarante.toString().trim() : polizza.localitaInt.toString().trim()
        def sesso=(polizza.tipoContraente.toString()=="garante")? polizza.tipoClienteGarante.value.toString().trim() : polizza.tipoClienteInt.value.toString().trim()
        def partitaIva=(polizza.tipoContraente.toString()=="garante")? polizza.partitaIvaGarante.toString().trim() : polizza.partitaIvaInt.toString().trim()
        def telefono=(polizza.tipoContraente.toString()=="garante")? polizza.telefonoGarante?.toString().trim()?:"" : polizza.telefonoInt?.toString().trim()?:""
        def email=(polizza.tipoContraente.toString()=="garante")? polizza.emailGarante?.toString().trim()?:"" : polizza.emailInt?.toString().trim()?:""
        def autoveicolo=(polizza.autoveicolo==true)? "N" : "S"
        def tracciato = [
                "Kasko":"I".padLeft(1),
                "codiceModello":"".padLeft(5),
                "descrizioneVeicolo":"${polizza.modello.toString().trim()}".padLeft(20),
                "Targa":"${polizza.targa.trim()}".padLeft(15),
                "Cognome":"${cognome +" "+nome}".padLeft(40),
                "Nome":"".padLeft(40),
                "Provincia":"${provinciaContr}".padLeft(2),
                "Valuta":"E".padLeft(1),
                "Valore assicurato":(polizza.valoreAssicurato)?"${(polizza.valoreAssicurato*100).round()}".padLeft(9):"".padLeft(9),
                "Data immatricolazione":(polizza.dataImmatricolazione)?"${polizza.dataImmatricolazione.format('yyyyMMdd')}".padLeft(8):"".padLeft(8),
                "durata furto incendio":"".padLeft(3),
                "Indirizzo":"${indirizzo}".padLeft(30),
                "Cap":"${cap}".padLeft(5),
                "Città":"${citta}".padLeft(30),
                "Data di nascita":"".padLeft(8),
                "PROVINCIA NASCITA ASSICURATO":"".padLeft(2),
                "Sesso":"${sesso}".padLeft(1),
                "Numero polizza":"${polizza.noPratica}".padLeft(10),
                "codice operazione":"0".padLeft(3),
                "DATA APERTURA CONTRATTO":"".padLeft(8),
                "IMPORTO ASSICURATO":"".padLeft(9),
                "Codice fiscale / Partita IVA":"${partitaIva}".padLeft(16),
                "Marca":"${marca}".padLeft(5),
                "Autocarro":"${autoveicolo.toString().trim()}".padLeft(1),
                "USO":"".padLeft(1),
                "scadenza vincolo":"".padLeft(8),
                "codice dealer":"".padLeft(5),
                "Numero telaio":"${polizza.telaio.trim()}".padLeft(17),
                "NUMERO RINNOVO":"".padLeft(2),
                "vincolo":"".padLeft(1),
                "Pcl":"".padLeft(9),
                "OLD-CONTRA  ":"".padLeft(10),
                "Cv fisc":"".padLeft(4),
                "QUINTALI  ":"".padLeft(4),
                "Data decorrenza":(polizza.dataDecorrenza)?"${polizza.dataDecorrenza.format("yyyyMMdd")}".padLeft(8):"".padLeft(8),
                "POLIZZA BUY BACK": "".padLeft(1),
                "ANTIFURTO SATELLITARE": "".padLeft(1),
                "Data scadenza":(polizza.dataDecorrenza)?"${polizza.dataScadenza.format("yyyyMMdd")}".padLeft(8):"".padLeft(8),
                "Rc":"S".padLeft(1),
                "Telefono":"".padLeft(15),
                "TELEFONO LAVORO":"".padLeft(15),
                "TELEFONO 2":"".padLeft(15),
                "CELLULARE":"${telefono}".padLeft(15)?:"".padLeft(15),
                "provvigioni dealer":"${(datiContabili.provvigioniDealer* 100).round()}".padLeft(9),
                "provvigioni Mach1":"${(datiContabili.provvigioniMach1* 100).round()}".padLeft(9),
                "provvigioni società commerciale":"${(datiContabili.provvigioniMansutti* 100).round()}".padLeft(9),
                "totale caricamenti":"${((datiContabili.provvigioniMach1+datiContabili.provvigioniDealer+datiContabili.provvigioniMansutti)* 100).round()}".padLeft(9),
                "imponibile":"${(datiContabili.imponibile* 100).round()}".padLeft(9),
                "Imposte":"${(datiContabili.imponibile*tasse.tasse).round()}".padLeft(9),
                "Pdu":"".padLeft(9),
                "IBAN":"".padLeft(34),
                "Collisione":"".padLeft(1),
                "data operazione":"".padLeft(8),
                "codice prodotto":"DE".padLeft(2),
                "tipologia veicolo":"".padLeft(1),
                "rischio":"".padLeft(1),
                "zona":"".padLeft(1),
                "email":"${email}".padLeft(50),
                "mini collisione":"".padLeft(2),
                "garanzia2":"".padLeft(2),
                "durata garanzia2":"".padLeft(2),
                "durata kasko":"".padLeft(2),
                "durata collisione":"".padLeft(2),
                "durata valore a nuovo":"".padLeft(2),
                "codice società commerciale":"".padLeft(3),
                "codice venditore":"".padLeft(3),
                "provvigioni venditore":"".padLeft(9),
                "codice segnalatore":"".padLeft(3),
                "provvigioni segnalatore":"".padLeft(9),
                "integra":"".padLeft(1),
                "numero polizza integrata":"".padLeft(10),
                "durata integra":"".padLeft(2),
                "codice iban":"".padLeft(27),
                "intestatario iban":"".padLeft(30),
                "token":"".padLeft(16),
                "cognome nome assicurato":"".padLeft(40),
                "partita iva assicurato":"".padLeft(11),
                "indirizzo assicurato":"".padLeft(30),
                "citta assicurato":"".padLeft(30),
                "cap assicurato":"".padLeft(5),
                "provincia assicurato":"".padLeft(2),
                "codice fiscale assicurato":"".padLeft(16),
                "data nascita assicurato":"".padLeft(8),
                "sesso assicurato":"".padLeft(1),
                "Codice Lojack":"".padLeft(7)
        ]
        def dataInvioTracciato=new Date()

        return new Tracciato(polizza: polizza, tracciato: tracciato.values().join(), dataInvio: dataInvioTracciato)
    }

    private Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.ITALY);
        cal.setTime(date);
        return cal;
    }
}
