package mail

import grails.core.GrailsApplication
import grails.transaction.Transactional
import grails.util.Environment
import mandrill.MandrillAttachment
import mandrill.MandrillMessage
import mandrill.MandrillRecipient
import mandrill.MergeVar
import org.apache.commons.mail.EmailException
import org.apache.commons.mail.HtmlEmail
import rcadeutsche.polizze.Documenti
import rcadeutsche.polizze.TipoDocumento

import javax.mail.util.ByteArrayDataSource as ByteArrayDS
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

@Transactional
class MailService {
    def mandrillService
    GrailsApplication grailsApplication

    def invioMail(polizza) {
        def recpts = []
        def attachs=[]
        def contents = []
        def fileEncode = null
        def documentiPolizza=Documenti.findByPolizza(polizza)

        def posizioneAss= polizza.tipoAcquisizione.toString()=="nuova polizza"? polizza.tipoAcquisizione.toString().toUpperCase():"VECCHIA POLIZZA"
        def vecchiaTarga
                if(polizza.tipoAcquisizione.toString()!="nuova polizza"){
                    vecchiaTarga="Ti informiamo che la targa di acquisizione è: ${polizza.targaAcquisizione.toString().toUpperCase()}, inoltre troverai in allegato il documento corrispondente alla posizione assicurativa della polizza."
                }else{
                    vecchiaTarga=""
                }
        def nomeInt=(polizza.tipoContraente.toString()=="garante")? polizza.nomeGarante : polizza.nomeInt
        def cognomeInt=(polizza.tipoContraente.toString()=="garante")? polizza.cognomeGarante : polizza.cognomeInt
        def telefonoInt=(polizza.tipoContraente.toString()=="garante")? polizza.telefonoGarante : polizza.telefonoInt
        def mailInt=(polizza.tipoContraente.toString()=="garante")? polizza.emailGarante : polizza.emailInt
        def datiCliente="${cognomeInt}  ${nomeInt}  <br> Telefono: ${telefonoInt}  <br> email:  ${mailInt}"
        def vars = [new MergeVar(name: "VECCHIATARGA", content: "${vecchiaTarga}"),new MergeVar(name: "CLIENTE", content: "${datiCliente}"), new MergeVar(name: "POSIZIONE", content: "${polizza.tipoAcquisizione.toString().toUpperCase()}")]
        if(documentiPolizza){
            if((documentiPolizza.tipo==TipoDocumento.BERSANI || documentiPolizza.tipo==TipoDocumento.PASSAGGIO) && documentiPolizza.fileContent.size()>0){
                fileEncode = new sun.misc.BASE64Encoder().encode(documentiPolizza.fileContent)
                attachs.add(new MandrillAttachment(type: "application/pdf", name: "${documentiPolizza.fileName}", content: fileEncode))
            }
        }
        if(Environment.current == Environment.PRODUCTION) {
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "documenti@mach-1.it"))
        }else{
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }

        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Portale DEUTSCHE Targa ${polizza.targa.toString().toUpperCase()}, posizione assicurativa ${posizioneAss}",
                from_email:"documenti@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "resoconto-polizze-deutsche", contents )
        def inviatoDealer = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }


    private class EmailCategory {
        static void to(HtmlEmail email, to = "") {
            def addresses = []
            if(to instanceof String) addresses = to.split(",").collect { address -> address.trim() }
            else if(to instanceof List) addresses = to
            else if(to instanceof String[]) addresses = to as List
            addresses.each { address -> email.addTo(address) }
        }
        static void cc(HtmlEmail email, cc = "") {
            def addresses = []
            if(cc instanceof String) addresses = cc.split(",").collect { address -> address.trim() }
            else if(cc instanceof List) addresses = cc
            else if(cc instanceof String[]) addresses = cc as List
            addresses.each { address -> email.addTo(address) }
        }
        static void attach(HtmlEmail email, file, fileName, type = "text/plain", description = "") {
            def data = null
            if(file instanceof byte[] || file instanceof InputStream) data = file
            else if(file instanceof File) data = file.bytes
            if(data) {
                def source = new ByteArrayDS(data, type)
                email.attach(source, fileName, description)
            }
        }
        static void subject(HtmlEmail email, subject = "") { email.subject = subject }
        static void msg(HtmlEmail email, msg = "") { email.msg = msg }
    }

    def sendMail(Closure closure) throws EmailException {
        def emailConfig = grailsApplication.config.mail
        def email = new HtmlEmail()
        use(EmailCategory) {
            email.hostName = emailConfig.host
            email.smtpPort = emailConfig.port
            email.from = emailConfig.from
            closure.resolveStrategy = Closure.DELEGATE_ONLY
            closure.delegate = email
            closure.call()
            return email.send()
        }
    }

    def invioMailCaricamento(risposta, filename) {
        def streamRiassunti = new ByteArrayOutputStream()
        def zipRiassunti = new ZipOutputStream(streamRiassunti)
        def rispostaFinale, fileNameRisposta
        rispostaFinale=risposta
        fileNameRisposta = "risposta_${filename}.txt"
        zipRiassunti.putNextEntry(new ZipEntry(fileNameRisposta))
        zipRiassunti.write(rispostaFinale.bytes)
        zipRiassunti.close()
        streamRiassunti.close()
        def recpts = []
        def attachs=[]
        def contents = []
        def fileEncode = null
        def vars = [new MergeVar(name: "DATAC", content: "${new Date().format("dd-MMM-yyyy")}")]
        fileEncode = new sun.misc.BASE64Encoder().encode(streamRiassunti.toByteArray())
        attachs.add(new MandrillAttachment(type: "application/octet-stream", name: "riassunto_${filename}.zip", content: fileEncode))
        if(Environment.current == Environment.PRODUCTION) {
            recpts.add(new MandrillRecipient(name:"Cristina Sivelli", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Silvia Carino", email: "s.carino@mach-1.it"))
        }else{
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }

        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Elenco pratiche non caricate Portale RCA DEUTSCHE",
                from_email:"documenti@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "elenco-pratiche-noncaricatercadeutsche", contents )
        def inviatoD = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return inviatoD
    }

}
