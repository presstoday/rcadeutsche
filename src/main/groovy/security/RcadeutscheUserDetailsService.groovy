package security

import grails.transaction.Transactional
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import rcadeutsche.utenti.Utente

class RcadeutscheUserDetailsService implements UserDetailsService {

    static final Logger log = LoggerFactory.getLogger(RcadeutscheUserDetailsService)

    @Transactional(readOnly = true, noRollbackFor = [IllegalArgumentException, UsernameNotFoundException])
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        username=username.trim().replaceAll("\\s+","").replaceAll("-","")
        username=username.replaceAll("^0*","")
        def utente = Utente.findWhere(username: username, deleted: false)
        log.info "Authenticate user ${username}: ${utente}"
        if (!utente) {
            log.warn "User not found: $username"
            throw new UsernameNotFoundException('User not found')
        }
        return new RcadeutscheUser(utente)
    }

}
