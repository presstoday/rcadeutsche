package security

import grails.util.Holders
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.web.authentication.switchuser.SwitchUserFilter
import org.springframework.security.web.authentication.switchuser.SwitchUserGrantedAuthority

class SecurityUtils {

    static final Logger log = LoggerFactory.getLogger(SecurityUtils)

    static boolean hasRole(String role) {
        log.debug "Check if has role: ${role}"
        return authorities*.authority.contains(role)
    }

    static boolean hasAllRoles(String... roles) { return hasAllRoles(Arrays.asList(roles)) }

    static boolean hasAllRoles(Collection<String> roles) {
        log.debug "Check if has all roles: ${roles}"
        return authorities*.authority.containsAll(roles)
    }

    static boolean hasAnyRoles(String... roles) { return hasAnyRoles(Arrays.asList(roles)) }

    static boolean hasAnyRoles(Collection<String> roles) {
        log.debug "Check if has any roles: ${roles}"
        return !authorities*.authority.intersect(roles).isEmpty()
    }

    static boolean hasNotRole(String role) {
        log.debug "Check if has not role: ${role}"
        return !authorities*.authority.contains(role)
    }

    static boolean hasNotRoles(String... roles) { return hasNotRoles(Arrays.asList(roles)) }

    static boolean hasNotRoles(Collection<String> roles) {
        log.debug "Check if has not roles: ${roles}"
        return authorities*.authority.intersect(roles).isEmpty()
    }

    static boolean isSwitched() {
        def switched = false
        for(def authority : authorities) {
            if(authority instanceof SwitchUserGrantedAuthority || authority.authority == SwitchUserFilter.ROLE_PREVIOUS_ADMINISTRATOR) {
                switched = true
                break
            }
        }
        log.debug "Check if is switched: ${switched ? "user is switched" : "user is not switched"} (${switched})"
        return switched
    }

    static RcadeutscheUser getSwitchedUser() {
        if(isSwitched()) {
            def switchedUser = null
            for(def authority : authorities) {
                if(authority instanceof SwitchUserGrantedAuthority) {
                    switchedUser = authority.source.principal
                    break
                }
            }
            log.debug "Get switched user: ${switchedUser}"
            return switchedUser
        } else return null
    }

    static RcadeutscheUser getPrincipal() {
        def principal = authentication.principal
        log.debug "Get principal: ${principal}"
        return principal
    }

    static boolean isAnonymous() {
        def authentication = authentication
        return authentication instanceof AnonymousAuthenticationToken
    }

    static boolean isLoggedIn() {
        def authentication = authentication
        return authentication && authentication.isAuthenticated() && !isAnonymous()
    }

    static void reauthenticate(String username, String password) {
        def userDetails = userDetailsService.loadUserByUsername(username)
        def token = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.authorities)
        context.setAuthentication(token)
    }

    static Collection<GrantedAuthority> getAuthorities() {
        def authorities = authentication.authorities
        log.debug "Get authorities: ${authorities}"
        if(authorities) return authorities
        else return []
    }

    static Authentication getAuthentication() {
        def authentication = context.authentication
        log.debug "Get authentication: ${authentication}"
        return authentication
    }

    static SecurityContext getContext() {
        def context = SecurityContextHolder.context
        log.debug "Get security context: ${context}"
        return context
    }

    static UserDetailsService getUserDetailsService() { return getBean("userDetailsService") }

    private static <T> T getBean(final String beanName) { return Holders.applicationContext.getBean(beanName) }

}
