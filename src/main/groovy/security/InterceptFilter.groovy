package security

import groovy.util.logging.Slf4j

import javax.servlet.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Slf4j
class InterceptFilter implements Filter {

    void init(FilterConfig filterConfig) throws ServletException {}

    void destroy() {}

    void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        def url = ((HttpServletRequest)request).requestURI
        log.debug "Intercepted url: ${url}"
        if(url == "/RCADEUTSCHE") {
            log.debug "Url match /RCADEUTSCHE, performing redirect to /RCADEUTSCHE/"
            ((HttpServletResponse)response).sendRedirect("/RCADEUTSCHE/")
        } else chain.doFilter(request, response)
    }
}