package extensions

import groovy.transform.CompileStatic

@CompileStatic
class MapExtension {

    static List<Map> collate(Map target, int size, boolean keepReminder = true) { return collate(target, size, size, keepReminder) }

    static List<Map> collate(Map target, int size, int step, boolean keepReminder = true) {
        if(step == 0) step = size
        def result = []
        Collection<Map.Entry> list = target.collect()
        if(size <= 0 || list.size() == 0) result << target
        else {
            for(int i = 0; i <= list.size() && i > -1; i += step) {
                if(!keepReminder && i > list.size() - size) break
                def element = [:]
                for(int j = i; j < i + size && j < list.size(); j++) element.put(list[j].key, list[j].value)
                if(element) result << element
            }
        }
        return result
    }

    static <A, B> Map<B, A> swap(Map<A, B> target) {
        def swapped = [:]
        target.each { k, v -> swapped[v] = k }
        return swapped
    }

}

