package extensions

class ObjectExtension {

    static def beanProperty(def target, def property) {
        if(target && property) {
            if(target instanceof Collection) {                                                                                                // se l'oggetto è una collezione
                if(property instanceof Closure) return target.collect(property)                                                               // se la proprietà è una closure faccio la collect con quella closure come parametro
                else if(property instanceof String && property.endsWith("()")) return target*."${property[0..-3]}"()                          // se la proprietà è un metodo senza parametri chiamo quel metodo su ogni elemento della collezione e restituisco i risultati (come una collect)
                else if(property instanceof String || property instanceof Number) return target.collect { obj -> obj.getPath(property) }      // se la proprietà è una stringa o un numero vado a recuperarla mediante la navigazione attraverso i vari campi dell'oggetto
            } else if(property instanceof Closure) return property(target)                                                                    // se l'oggetto non è una collezione e la proprietà è una closure chiamo la closure passandogli come parametro l'oggetto
            else if(target instanceof Date) {                                                                                                 // se l'oggetto è una data
                if(property instanceof String) return target.format(property)                                                                 // se la proprietà è una stringa formatto la data usando la stringa come formato
            } else if(target instanceof Number) {                                                                                             // se l'oggetto è un numero
                if(property instanceof String) return target.format(property)                                                                 // se la proprietà è una stringa formatto il numero usando la stringa come formato
            } else {                                                                                                                          // se l'oggetto è altro
                if(property instanceof String && property.endsWith("()")) return target."${property[0..-3]}"()                                // se la proprietà è un metodo senza parametri chiamo quel metodo sull'oggetto
                else if(property instanceof String || property instanceof Number) return target.getPath(property)                             // se la proprietà è una stringa o un numero vado a recuperarla mediante la navigazione attraverso i vari campi dell'oggetto
            }
        } else return null
    }

    static def getPath(def target, String property) {
        if(target && property) {
            def path = property.tokenize(".[]")
            def value = target
            for(sub in path) {
                if(value != null) {
                    if(sub.isNumber()) value = value[sub as int]
                    else value = value."${sub}"
                } else break
            }
            return value
        } else return null
    }

    static def getPath(def target, Number property) {
        if(target && property) return target[property.intValue()]
        else return null
    }

}
