package mandrill


class SendResponse {
    String status
    String message
    String email
    String id
    String rejectReason
    boolean success

    SendResponse(data) {
        this.status = data.getAt('status')
        this.message = data.getAt('message')
        this.rejectReason = data.getAt('reject_reason')
        this.email =  data.getAt('email')
        this.id =  data.getAt('_id')
        success = ['sent', 'queued', 'scheduled'].contains(status)
    }
}
